/*
 * A sample of code that uses the Quack runtime. 
 * We'll write the kind of C we would expect to 
 * produce by code generation. 
 */

#include <stdio.h>
#include <stdlib.h>

#include "Builtins.h"

void quackmain(); 

/* Boilerplate --- 
 * let C build us a main program. 
 */
int main(int argc, char** argv) {
  quackmain();
  printf("--- Terminated successfully (woot!) ---\n");
  exit(0);
}

/* ---- User-written class -----
 * -  Hand-generated, but what we
 * -  would want to generate for 
 * 
 class Pt(x: Int, y: Int) {
    this.x = x;
    this.y = y; 

    def PRINT( ):Pt {
       "( ".PRINT(); 
       this.x.PRINT(); 
       ", ".PRINT();
       ")".PRINT(); 
     }

     def PLUS(other: Pt) {
         return Pt(this.x+other.x, this.y+other.y); 
     }
  }
  *-----------------------------
  */

/* ================
 * Pt
 * Fields: 
 *    x, y: Int
 * Methods: 
 *    STRING  (inherit, since we don't have concatenation yet)
 *    PRINT   (override)
 *    EQUALS  (inherit, since our EQUALS is busted) 
 *    and introducing
 *    PLUS    (add x and y values pointwise)
 * =================
 */

/* Declarations based on pattern in Builtins.h */ 
struct class_Pt_struct;
typedef struct class_Pt_struct* class_Pt;
typedef struct obj_Pt_struct {
  class_Pt  clazz;
  obj_Int x;
obj_Int y;
} * obj_Pt;
struct  class_Pt_struct  the_class_Pt_struct;
struct class_Pt_struct {
  /* Method table */

obj_Pt (*constructor) (obj_Int,obj_Int);
obj_Boolean (*EQUALS) (obj_Obj,obj_Obj);
obj_Pt (*PLUS) (obj_Pt,obj_Pt);
obj_String (*PRINT) (obj_Pt);
obj_String (*STRING) (obj_Pt);

};
extern class_Pt the_class_Pt;

obj_Pt new_Pt(obj_Int x,obj_Int y){
  obj_Pt new = (obj_Pt) malloc(sizeof(struct obj_Pt_struct));
  new->clazz = the_class_Pt;
  obj_Pt this;
  this = new;

//StatementExp UntypedAssignExp

obj_Int t;
obj_Int tmp2;
tmp2 = (t = int_literal(5));

//StatementExp UntypedAssignExp

//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp4;
tmp4 = this;
;

obj_Int tmp5;
tmp5 = tmp4->x;


//LEFT EXPRESSION IdRef
obj_Int tmp3;
tmp3 = x;
;

this->x = tmp3;

//StatementExp UntypedAssignExp

//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp9;
tmp9 = this;
;

obj_Int tmp10;
tmp10 = tmp9->y;


//LEFT EXPRESSION IdRef
obj_Int tmp8;
tmp8 = y;
;

this->y = tmp8;


  return new;
}

obj_Pt Pt_method_PLUS(obj_Pt this,obj_Pt other){
//StatementExp UntypedAssignExp


//CONSTRUCTOR CALL EXP


//BINARY EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp0;
tmp0 = this;
;

obj_Int tmp1;
tmp1 = tmp0->x;
;


obj_Int tmp2;
tmp2 = tmp1->clazz->PLUS(tmp1,int_literal(1));



//BINARY EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp3;
tmp3 = this;
;

obj_Int tmp4;
tmp4 = tmp3->y;
;


obj_Int tmp5;
tmp5 = tmp4->clazz->PLUS(tmp4,int_literal(1));
obj_Pt tmp6;
tmp6 = the_class_Pt->constructor(tmp2, tmp5);
;

return tmp6;


}

obj_String Pt_method_STRING(obj_Pt this){
//StatementExp UntypedAssignExp


//FUNC CALL EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp0;
tmp0 = this;
;

obj_Int tmp1;
tmp1 = tmp0->y;
;


obj_String tmp2;
tmp2 = tmp1->clazz->STRING(tmp1);
;

return tmp2;


}

obj_String Pt_method_PRINT(obj_Pt this) {
fprintf(stdout, "%s",(Pt_method_STRING(this))->text);
return this;
}

struct class_Pt_struct the_class_Pt_struct = {

new_Pt,
Obj_method_EQUALS,
Pt_method_PLUS,
Pt_method_PRINT,
Pt_method_STRING
};

class_Pt the_class_Pt = &the_class_Pt_struct;




void quackmain() {
//StatementExp UntypedAssignExp


//CONSTRUCTOR CALL EXP

obj_Pt tmp0;
tmp0 = the_class_Pt->constructor(int_literal(42), int_literal(69));
;

obj_Pt x;
obj_Pt tmp3;
tmp3 = (x = tmp0);

//StatementExp UntypedAssignExp


//FUNC CALL EXPRESSION

//LEFT EXPRESSION IdRef
obj_Pt tmp5;
tmp5 = x;
;


//LEFT EXPRESSION IdRef
obj_Pt tmp4;
tmp4 = x;
;

obj_Pt tmp6;
tmp6 = tmp5->clazz->PLUS(tmp5,tmp4);
;

obj_Pt y;
obj_Pt tmp9;
tmp9 = (y = tmp6);


//FUNC CALL EXPRESSION

//LEFT EXPRESSION IdRef
obj_Pt tmp10;
tmp10 = y;
;


obj_String tmp11;
tmp11 = tmp10->clazz->PRINT(tmp10);
;

}


