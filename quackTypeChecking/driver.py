

import os

import sys, json

from subprocess import  Popen, PIPE, STDOUT




if __name__ == "__main__":

  if len(sys.argv) == 0:
    print "Error: no input file"
    sys.exit(1)

  infile =  sys.argv[1]

  with open(infile,"r") as inf:
    contents = inf.read()
    p = Popen(["./quackParser", infile],stdin=PIPE,stdout=PIPE,stderr=STDOUT)
    out = p.communicate(input=contents)[0]

  if out.startswith("Errors:"):
    print out
    sys.exit()
  
  with open("runtime/output.c", "w") as ofile:
    ofile.write(out)

 # os.chdir("runtime")
  p = Popen(["gcc", "runtime/output.c", "runtime/Builtins.c", "runtime/Builtins.h", "-o", infile[:-3]],stdout=PIPE,stderr=STDOUT)
  compile_out = p.communicate()[0]
  if compile_out.find("error:") != -1:
    print compile_out
