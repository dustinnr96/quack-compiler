{-# OPTIONS_GHC -w #-}
module Main where
import Data.List
import qualified Data.Map.Strict as Map
--import qualified Data.Map.Internal.Debug as DebugMap
import Text.Printf (printf)

import QuackLexer
import QuackAST
import Imc
import ImcUtil
import Control.Applicative(Applicative(..))
import Control.Monad (ap)

-- parser produced by Happy Version 1.19.5

data HappyAbsSyn t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16 t17 t18 t19 t20 t21 t22 t23 t24 t25
	= HappyTerminal (Token)
	| HappyErrorToken Int
	| HappyAbsSyn4 t4
	| HappyAbsSyn5 t5
	| HappyAbsSyn6 t6
	| HappyAbsSyn7 t7
	| HappyAbsSyn8 t8
	| HappyAbsSyn9 t9
	| HappyAbsSyn10 t10
	| HappyAbsSyn11 t11
	| HappyAbsSyn12 t12
	| HappyAbsSyn13 t13
	| HappyAbsSyn14 t14
	| HappyAbsSyn15 t15
	| HappyAbsSyn16 t16
	| HappyAbsSyn17 t17
	| HappyAbsSyn18 t18
	| HappyAbsSyn19 t19
	| HappyAbsSyn20 t20
	| HappyAbsSyn21 t21
	| HappyAbsSyn22 t22
	| HappyAbsSyn23 t23
	| HappyAbsSyn24 t24
	| HappyAbsSyn25 t25

action_0 (26) = happyShift action_5
action_0 (29) = happyShift action_17
action_0 (32) = happyShift action_18
action_0 (33) = happyShift action_19
action_0 (46) = happyShift action_20
action_0 (49) = happyShift action_21
action_0 (55) = happyShift action_22
action_0 (56) = happyShift action_23
action_0 (57) = happyShift action_24
action_0 (58) = happyShift action_25
action_0 (59) = happyShift action_26
action_0 (4) = happyGoto action_6
action_0 (5) = happyGoto action_2
action_0 (6) = happyGoto action_7
action_0 (7) = happyGoto action_4
action_0 (12) = happyGoto action_8
action_0 (13) = happyGoto action_9
action_0 (15) = happyGoto action_10
action_0 (16) = happyGoto action_11
action_0 (20) = happyGoto action_12
action_0 (21) = happyGoto action_13
action_0 (22) = happyGoto action_14
action_0 (23) = happyGoto action_15
action_0 (24) = happyGoto action_16
action_0 _ = happyReduce_4

action_1 (26) = happyShift action_5
action_1 (5) = happyGoto action_2
action_1 (6) = happyGoto action_3
action_1 (7) = happyGoto action_4
action_1 _ = happyFail

action_2 _ = happyReduce_6

action_3 (26) = happyShift action_5
action_3 (29) = happyShift action_17
action_3 (32) = happyShift action_18
action_3 (33) = happyShift action_19
action_3 (46) = happyShift action_20
action_3 (49) = happyShift action_21
action_3 (55) = happyShift action_22
action_3 (56) = happyShift action_23
action_3 (57) = happyShift action_24
action_3 (58) = happyShift action_25
action_3 (59) = happyShift action_26
action_3 (5) = happyGoto action_51
action_3 (7) = happyGoto action_4
action_3 (12) = happyGoto action_8
action_3 (13) = happyGoto action_52
action_3 (15) = happyGoto action_10
action_3 (16) = happyGoto action_11
action_3 (20) = happyGoto action_12
action_3 (21) = happyGoto action_13
action_3 (22) = happyGoto action_14
action_3 (23) = happyGoto action_15
action_3 (24) = happyGoto action_16
action_3 _ = happyFail

action_4 (47) = happyShift action_55
action_4 (9) = happyGoto action_54
action_4 _ = happyFail

action_5 (55) = happyShift action_53
action_5 _ = happyFail

action_6 (60) = happyAccept
action_6 _ = happyFail

action_7 (26) = happyShift action_5
action_7 (29) = happyShift action_17
action_7 (32) = happyShift action_18
action_7 (33) = happyShift action_19
action_7 (46) = happyShift action_20
action_7 (49) = happyShift action_21
action_7 (55) = happyShift action_22
action_7 (56) = happyShift action_23
action_7 (57) = happyShift action_24
action_7 (58) = happyShift action_25
action_7 (59) = happyShift action_26
action_7 (5) = happyGoto action_51
action_7 (7) = happyGoto action_4
action_7 (12) = happyGoto action_8
action_7 (13) = happyGoto action_52
action_7 (15) = happyGoto action_10
action_7 (16) = happyGoto action_11
action_7 (20) = happyGoto action_12
action_7 (21) = happyGoto action_13
action_7 (22) = happyGoto action_14
action_7 (23) = happyGoto action_15
action_7 (24) = happyGoto action_16
action_7 _ = happyReduce_2

action_8 _ = happyReduce_26

action_9 (29) = happyShift action_17
action_9 (32) = happyShift action_18
action_9 (33) = happyShift action_19
action_9 (46) = happyShift action_20
action_9 (49) = happyShift action_21
action_9 (55) = happyShift action_22
action_9 (56) = happyShift action_23
action_9 (57) = happyShift action_24
action_9 (58) = happyShift action_25
action_9 (59) = happyShift action_26
action_9 (12) = happyGoto action_50
action_9 (15) = happyGoto action_10
action_9 (16) = happyGoto action_11
action_9 (20) = happyGoto action_12
action_9 (21) = happyGoto action_13
action_9 (22) = happyGoto action_14
action_9 (23) = happyGoto action_15
action_9 (24) = happyGoto action_16
action_9 _ = happyReduce_3

action_10 _ = happyReduce_22

action_11 _ = happyReduce_21

action_12 _ = happyReduce_23

action_13 _ = happyReduce_24

action_14 (38) = happyShift action_48
action_14 (54) = happyShift action_49
action_14 _ = happyReduce_49

action_15 _ = happyReduce_25

action_16 (34) = happyShift action_35
action_16 (35) = happyShift action_36
action_16 (36) = happyShift action_37
action_16 (37) = happyShift action_38
action_16 (39) = happyShift action_39
action_16 (40) = happyShift action_40
action_16 (41) = happyShift action_41
action_16 (42) = happyShift action_42
action_16 (43) = happyShift action_43
action_16 (44) = happyShift action_44
action_16 (45) = happyShift action_45
action_16 (52) = happyShift action_46
action_16 (53) = happyShift action_47
action_16 _ = happyFail

action_17 (46) = happyShift action_20
action_17 (49) = happyShift action_21
action_17 (55) = happyShift action_22
action_17 (56) = happyShift action_23
action_17 (57) = happyShift action_24
action_17 (58) = happyShift action_25
action_17 (59) = happyShift action_26
action_17 (22) = happyGoto action_28
action_17 (24) = happyGoto action_34
action_17 _ = happyFail

action_18 (46) = happyShift action_20
action_18 (49) = happyShift action_21
action_18 (55) = happyShift action_22
action_18 (56) = happyShift action_23
action_18 (57) = happyShift action_24
action_18 (58) = happyShift action_25
action_18 (59) = happyShift action_26
action_18 (22) = happyGoto action_28
action_18 (24) = happyGoto action_33
action_18 _ = happyFail

action_19 (46) = happyShift action_20
action_19 (49) = happyShift action_21
action_19 (52) = happyShift action_32
action_19 (55) = happyShift action_22
action_19 (56) = happyShift action_23
action_19 (57) = happyShift action_24
action_19 (58) = happyShift action_25
action_19 (59) = happyShift action_26
action_19 (22) = happyGoto action_28
action_19 (24) = happyGoto action_31
action_19 _ = happyFail

action_20 (46) = happyShift action_20
action_20 (49) = happyShift action_21
action_20 (55) = happyShift action_22
action_20 (56) = happyShift action_23
action_20 (57) = happyShift action_24
action_20 (58) = happyShift action_25
action_20 (59) = happyShift action_26
action_20 (22) = happyGoto action_28
action_20 (24) = happyGoto action_30
action_20 _ = happyFail

action_21 (46) = happyShift action_20
action_21 (49) = happyShift action_21
action_21 (55) = happyShift action_22
action_21 (56) = happyShift action_23
action_21 (57) = happyShift action_24
action_21 (58) = happyShift action_25
action_21 (59) = happyShift action_26
action_21 (22) = happyGoto action_28
action_21 (24) = happyGoto action_29
action_21 _ = happyFail

action_22 (49) = happyShift action_27
action_22 _ = happyReduce_42

action_23 _ = happyReduce_46

action_24 _ = happyReduce_48

action_25 _ = happyReduce_47

action_26 _ = happyReduce_45

action_27 (46) = happyShift action_20
action_27 (49) = happyShift action_21
action_27 (50) = happyShift action_83
action_27 (55) = happyShift action_22
action_27 (56) = happyShift action_23
action_27 (57) = happyShift action_24
action_27 (58) = happyShift action_25
action_27 (59) = happyShift action_26
action_27 (22) = happyGoto action_28
action_27 (24) = happyGoto action_81
action_27 (25) = happyGoto action_82
action_27 _ = happyFail

action_28 _ = happyReduce_49

action_29 (34) = happyShift action_35
action_29 (35) = happyShift action_36
action_29 (36) = happyShift action_37
action_29 (37) = happyShift action_38
action_29 (39) = happyShift action_39
action_29 (40) = happyShift action_40
action_29 (41) = happyShift action_41
action_29 (42) = happyShift action_42
action_29 (43) = happyShift action_43
action_29 (44) = happyShift action_44
action_29 (45) = happyShift action_45
action_29 (50) = happyShift action_80
action_29 (53) = happyShift action_47
action_29 _ = happyFail

action_30 (34) = happyShift action_35
action_30 (35) = happyShift action_36
action_30 (36) = happyShift action_37
action_30 (37) = happyShift action_38
action_30 (39) = happyShift action_39
action_30 (40) = happyShift action_40
action_30 (41) = happyShift action_41
action_30 (42) = happyShift action_42
action_30 (43) = happyShift action_43
action_30 (44) = happyShift action_44
action_30 (45) = happyShift action_45
action_30 (53) = happyShift action_47
action_30 _ = happyReduce_62

action_31 (34) = happyShift action_35
action_31 (35) = happyShift action_36
action_31 (36) = happyShift action_37
action_31 (37) = happyShift action_38
action_31 (39) = happyShift action_39
action_31 (40) = happyShift action_40
action_31 (41) = happyShift action_41
action_31 (42) = happyShift action_42
action_31 (43) = happyShift action_43
action_31 (44) = happyShift action_44
action_31 (45) = happyShift action_45
action_31 (52) = happyShift action_79
action_31 (53) = happyShift action_47
action_31 _ = happyFail

action_32 _ = happyReduce_38

action_33 (34) = happyShift action_35
action_33 (35) = happyShift action_36
action_33 (36) = happyShift action_37
action_33 (37) = happyShift action_38
action_33 (39) = happyShift action_39
action_33 (40) = happyShift action_40
action_33 (41) = happyShift action_41
action_33 (42) = happyShift action_42
action_33 (43) = happyShift action_43
action_33 (44) = happyShift action_44
action_33 (45) = happyShift action_45
action_33 (47) = happyShift action_77
action_33 (53) = happyShift action_47
action_33 (14) = happyGoto action_78
action_33 _ = happyFail

action_34 (34) = happyShift action_35
action_34 (35) = happyShift action_36
action_34 (36) = happyShift action_37
action_34 (37) = happyShift action_38
action_34 (39) = happyShift action_39
action_34 (40) = happyShift action_40
action_34 (41) = happyShift action_41
action_34 (42) = happyShift action_42
action_34 (43) = happyShift action_43
action_34 (44) = happyShift action_44
action_34 (45) = happyShift action_45
action_34 (47) = happyShift action_77
action_34 (53) = happyShift action_47
action_34 (14) = happyGoto action_76
action_34 _ = happyFail

action_35 (46) = happyShift action_20
action_35 (49) = happyShift action_21
action_35 (55) = happyShift action_22
action_35 (56) = happyShift action_23
action_35 (57) = happyShift action_24
action_35 (58) = happyShift action_25
action_35 (59) = happyShift action_26
action_35 (22) = happyGoto action_28
action_35 (24) = happyGoto action_75
action_35 _ = happyFail

action_36 (46) = happyShift action_20
action_36 (49) = happyShift action_21
action_36 (55) = happyShift action_22
action_36 (56) = happyShift action_23
action_36 (57) = happyShift action_24
action_36 (58) = happyShift action_25
action_36 (59) = happyShift action_26
action_36 (22) = happyGoto action_28
action_36 (24) = happyGoto action_74
action_36 _ = happyFail

action_37 (46) = happyShift action_20
action_37 (49) = happyShift action_21
action_37 (55) = happyShift action_22
action_37 (56) = happyShift action_23
action_37 (57) = happyShift action_24
action_37 (58) = happyShift action_25
action_37 (59) = happyShift action_26
action_37 (22) = happyGoto action_28
action_37 (24) = happyGoto action_73
action_37 _ = happyFail

action_38 (46) = happyShift action_20
action_38 (49) = happyShift action_21
action_38 (55) = happyShift action_22
action_38 (56) = happyShift action_23
action_38 (57) = happyShift action_24
action_38 (58) = happyShift action_25
action_38 (59) = happyShift action_26
action_38 (22) = happyGoto action_28
action_38 (24) = happyGoto action_72
action_38 _ = happyFail

action_39 (46) = happyShift action_20
action_39 (49) = happyShift action_21
action_39 (55) = happyShift action_22
action_39 (56) = happyShift action_23
action_39 (57) = happyShift action_24
action_39 (58) = happyShift action_25
action_39 (59) = happyShift action_26
action_39 (22) = happyGoto action_28
action_39 (24) = happyGoto action_71
action_39 _ = happyFail

action_40 (46) = happyShift action_20
action_40 (49) = happyShift action_21
action_40 (55) = happyShift action_22
action_40 (56) = happyShift action_23
action_40 (57) = happyShift action_24
action_40 (58) = happyShift action_25
action_40 (59) = happyShift action_26
action_40 (22) = happyGoto action_28
action_40 (24) = happyGoto action_70
action_40 _ = happyFail

action_41 (46) = happyShift action_20
action_41 (49) = happyShift action_21
action_41 (55) = happyShift action_22
action_41 (56) = happyShift action_23
action_41 (57) = happyShift action_24
action_41 (58) = happyShift action_25
action_41 (59) = happyShift action_26
action_41 (22) = happyGoto action_28
action_41 (24) = happyGoto action_69
action_41 _ = happyFail

action_42 (46) = happyShift action_20
action_42 (49) = happyShift action_21
action_42 (55) = happyShift action_22
action_42 (56) = happyShift action_23
action_42 (57) = happyShift action_24
action_42 (58) = happyShift action_25
action_42 (59) = happyShift action_26
action_42 (22) = happyGoto action_28
action_42 (24) = happyGoto action_68
action_42 _ = happyFail

action_43 (46) = happyShift action_20
action_43 (49) = happyShift action_21
action_43 (55) = happyShift action_22
action_43 (56) = happyShift action_23
action_43 (57) = happyShift action_24
action_43 (58) = happyShift action_25
action_43 (59) = happyShift action_26
action_43 (22) = happyGoto action_28
action_43 (24) = happyGoto action_67
action_43 _ = happyFail

action_44 (46) = happyShift action_20
action_44 (49) = happyShift action_21
action_44 (55) = happyShift action_22
action_44 (56) = happyShift action_23
action_44 (57) = happyShift action_24
action_44 (58) = happyShift action_25
action_44 (59) = happyShift action_26
action_44 (22) = happyGoto action_28
action_44 (24) = happyGoto action_66
action_44 _ = happyFail

action_45 (46) = happyShift action_20
action_45 (49) = happyShift action_21
action_45 (55) = happyShift action_22
action_45 (56) = happyShift action_23
action_45 (57) = happyShift action_24
action_45 (58) = happyShift action_25
action_45 (59) = happyShift action_26
action_45 (22) = happyGoto action_28
action_45 (24) = happyGoto action_65
action_45 _ = happyFail

action_46 _ = happyReduce_44

action_47 (55) = happyShift action_64
action_47 _ = happyFail

action_48 (46) = happyShift action_20
action_48 (49) = happyShift action_21
action_48 (55) = happyShift action_22
action_48 (56) = happyShift action_23
action_48 (57) = happyShift action_24
action_48 (58) = happyShift action_25
action_48 (59) = happyShift action_26
action_48 (22) = happyGoto action_28
action_48 (24) = happyGoto action_63
action_48 _ = happyFail

action_49 (55) = happyShift action_62
action_49 _ = happyFail

action_50 _ = happyReduce_27

action_51 _ = happyReduce_7

action_52 (29) = happyShift action_17
action_52 (32) = happyShift action_18
action_52 (33) = happyShift action_19
action_52 (46) = happyShift action_20
action_52 (49) = happyShift action_21
action_52 (55) = happyShift action_22
action_52 (56) = happyShift action_23
action_52 (57) = happyShift action_24
action_52 (58) = happyShift action_25
action_52 (59) = happyShift action_26
action_52 (12) = happyGoto action_50
action_52 (15) = happyGoto action_10
action_52 (16) = happyGoto action_11
action_52 (20) = happyGoto action_12
action_52 (21) = happyGoto action_13
action_52 (22) = happyGoto action_14
action_52 (23) = happyGoto action_15
action_52 (24) = happyGoto action_16
action_52 _ = happyReduce_1

action_53 (49) = happyShift action_61
action_53 _ = happyFail

action_54 _ = happyReduce_5

action_55 (27) = happyShift action_59
action_55 (29) = happyShift action_17
action_55 (32) = happyShift action_18
action_55 (33) = happyShift action_19
action_55 (46) = happyShift action_20
action_55 (48) = happyShift action_60
action_55 (49) = happyShift action_21
action_55 (55) = happyShift action_22
action_55 (56) = happyShift action_23
action_55 (57) = happyShift action_24
action_55 (58) = happyShift action_25
action_55 (59) = happyShift action_26
action_55 (10) = happyGoto action_56
action_55 (11) = happyGoto action_57
action_55 (12) = happyGoto action_8
action_55 (13) = happyGoto action_58
action_55 (15) = happyGoto action_10
action_55 (16) = happyGoto action_11
action_55 (20) = happyGoto action_12
action_55 (21) = happyGoto action_13
action_55 (22) = happyGoto action_14
action_55 (23) = happyGoto action_15
action_55 (24) = happyGoto action_16
action_55 _ = happyFail

action_56 _ = happyReduce_19

action_57 (27) = happyShift action_59
action_57 (48) = happyShift action_102
action_57 (10) = happyGoto action_101
action_57 _ = happyFail

action_58 (27) = happyShift action_59
action_58 (29) = happyShift action_17
action_58 (32) = happyShift action_18
action_58 (33) = happyShift action_19
action_58 (46) = happyShift action_20
action_58 (48) = happyShift action_100
action_58 (49) = happyShift action_21
action_58 (55) = happyShift action_22
action_58 (56) = happyShift action_23
action_58 (57) = happyShift action_24
action_58 (58) = happyShift action_25
action_58 (59) = happyShift action_26
action_58 (10) = happyGoto action_56
action_58 (11) = happyGoto action_99
action_58 (12) = happyGoto action_50
action_58 (15) = happyGoto action_10
action_58 (16) = happyGoto action_11
action_58 (20) = happyGoto action_12
action_58 (21) = happyGoto action_13
action_58 (22) = happyGoto action_14
action_58 (23) = happyGoto action_15
action_58 (24) = happyGoto action_16
action_58 _ = happyFail

action_59 (55) = happyShift action_98
action_59 _ = happyFail

action_60 _ = happyReduce_16

action_61 (55) = happyShift action_97
action_61 (8) = happyGoto action_96
action_61 _ = happyReduce_10

action_62 (38) = happyShift action_95
action_62 _ = happyFail

action_63 (34) = happyShift action_35
action_63 (35) = happyShift action_36
action_63 (36) = happyShift action_37
action_63 (37) = happyShift action_38
action_63 (39) = happyShift action_39
action_63 (40) = happyShift action_40
action_63 (41) = happyShift action_41
action_63 (42) = happyShift action_42
action_63 (43) = happyShift action_43
action_63 (44) = happyShift action_44
action_63 (45) = happyShift action_45
action_63 (52) = happyShift action_94
action_63 (53) = happyShift action_47
action_63 _ = happyFail

action_64 (49) = happyShift action_93
action_64 _ = happyReduce_43

action_65 (34) = happyShift action_35
action_65 (35) = happyShift action_36
action_65 (36) = happyShift action_37
action_65 (37) = happyShift action_38
action_65 (39) = happyShift action_39
action_65 (40) = happyShift action_40
action_65 (41) = happyShift action_41
action_65 (42) = happyShift action_42
action_65 (43) = happyShift action_43
action_65 (53) = happyShift action_47
action_65 _ = happyReduce_61

action_66 (34) = happyShift action_35
action_66 (35) = happyShift action_36
action_66 (36) = happyShift action_37
action_66 (37) = happyShift action_38
action_66 (39) = happyShift action_39
action_66 (40) = happyShift action_40
action_66 (41) = happyShift action_41
action_66 (42) = happyShift action_42
action_66 (43) = happyShift action_43
action_66 (53) = happyShift action_47
action_66 _ = happyReduce_60

action_67 (34) = happyShift action_35
action_67 (35) = happyShift action_36
action_67 (36) = happyShift action_37
action_67 (37) = happyShift action_38
action_67 (39) = happyFail
action_67 (40) = happyFail
action_67 (41) = happyFail
action_67 (42) = happyFail
action_67 (43) = happyFail
action_67 (53) = happyShift action_47
action_67 _ = happyReduce_59

action_68 (34) = happyShift action_35
action_68 (35) = happyShift action_36
action_68 (36) = happyShift action_37
action_68 (37) = happyShift action_38
action_68 (39) = happyFail
action_68 (40) = happyFail
action_68 (41) = happyFail
action_68 (42) = happyFail
action_68 (43) = happyFail
action_68 (53) = happyShift action_47
action_68 _ = happyReduce_58

action_69 (34) = happyShift action_35
action_69 (35) = happyShift action_36
action_69 (36) = happyShift action_37
action_69 (37) = happyShift action_38
action_69 (39) = happyFail
action_69 (40) = happyFail
action_69 (41) = happyFail
action_69 (42) = happyFail
action_69 (43) = happyFail
action_69 (53) = happyShift action_47
action_69 _ = happyReduce_57

action_70 (34) = happyShift action_35
action_70 (35) = happyShift action_36
action_70 (36) = happyShift action_37
action_70 (37) = happyShift action_38
action_70 (39) = happyFail
action_70 (40) = happyFail
action_70 (41) = happyFail
action_70 (42) = happyFail
action_70 (43) = happyFail
action_70 (53) = happyShift action_47
action_70 _ = happyReduce_56

action_71 (34) = happyShift action_35
action_71 (35) = happyShift action_36
action_71 (36) = happyShift action_37
action_71 (37) = happyShift action_38
action_71 (39) = happyFail
action_71 (40) = happyFail
action_71 (41) = happyFail
action_71 (42) = happyFail
action_71 (43) = happyFail
action_71 (53) = happyShift action_47
action_71 _ = happyReduce_55

action_72 (53) = happyShift action_47
action_72 _ = happyReduce_54

action_73 (53) = happyShift action_47
action_73 _ = happyReduce_53

action_74 (36) = happyShift action_37
action_74 (37) = happyShift action_38
action_74 (53) = happyShift action_47
action_74 _ = happyReduce_52

action_75 (36) = happyShift action_37
action_75 (37) = happyShift action_38
action_75 (53) = happyShift action_47
action_75 _ = happyReduce_51

action_76 (30) = happyShift action_91
action_76 (31) = happyShift action_92
action_76 (17) = happyGoto action_88
action_76 (18) = happyGoto action_89
action_76 (19) = happyGoto action_90
action_76 _ = happyReduce_37

action_77 (29) = happyShift action_17
action_77 (32) = happyShift action_18
action_77 (33) = happyShift action_19
action_77 (46) = happyShift action_20
action_77 (48) = happyShift action_87
action_77 (49) = happyShift action_21
action_77 (55) = happyShift action_22
action_77 (56) = happyShift action_23
action_77 (57) = happyShift action_24
action_77 (58) = happyShift action_25
action_77 (59) = happyShift action_26
action_77 (12) = happyGoto action_8
action_77 (13) = happyGoto action_86
action_77 (15) = happyGoto action_10
action_77 (16) = happyGoto action_11
action_77 (20) = happyGoto action_12
action_77 (21) = happyGoto action_13
action_77 (22) = happyGoto action_14
action_77 (23) = happyGoto action_15
action_77 (24) = happyGoto action_16
action_77 _ = happyFail

action_78 _ = happyReduce_30

action_79 _ = happyReduce_39

action_80 _ = happyReduce_50

action_81 (34) = happyShift action_35
action_81 (35) = happyShift action_36
action_81 (36) = happyShift action_37
action_81 (37) = happyShift action_38
action_81 (39) = happyShift action_39
action_81 (40) = happyShift action_40
action_81 (41) = happyShift action_41
action_81 (42) = happyShift action_42
action_81 (43) = happyShift action_43
action_81 (44) = happyShift action_44
action_81 (45) = happyShift action_45
action_81 (53) = happyShift action_47
action_81 _ = happyReduce_67

action_82 (50) = happyShift action_84
action_82 (51) = happyShift action_85
action_82 _ = happyFail

action_83 _ = happyReduce_66

action_84 _ = happyReduce_65

action_85 (46) = happyShift action_20
action_85 (49) = happyShift action_21
action_85 (55) = happyShift action_22
action_85 (56) = happyShift action_23
action_85 (57) = happyShift action_24
action_85 (58) = happyShift action_25
action_85 (59) = happyShift action_26
action_85 (22) = happyGoto action_28
action_85 (24) = happyGoto action_115
action_85 _ = happyFail

action_86 (29) = happyShift action_17
action_86 (32) = happyShift action_18
action_86 (33) = happyShift action_19
action_86 (46) = happyShift action_20
action_86 (48) = happyShift action_114
action_86 (49) = happyShift action_21
action_86 (55) = happyShift action_22
action_86 (56) = happyShift action_23
action_86 (57) = happyShift action_24
action_86 (58) = happyShift action_25
action_86 (59) = happyShift action_26
action_86 (12) = happyGoto action_50
action_86 (15) = happyGoto action_10
action_86 (16) = happyGoto action_11
action_86 (20) = happyGoto action_12
action_86 (21) = happyGoto action_13
action_86 (22) = happyGoto action_14
action_86 (23) = happyGoto action_15
action_86 (24) = happyGoto action_16
action_86 _ = happyFail

action_87 _ = happyReduce_28

action_88 _ = happyReduce_34

action_89 (30) = happyShift action_91
action_89 (31) = happyShift action_92
action_89 (17) = happyGoto action_112
action_89 (19) = happyGoto action_113
action_89 _ = happyReduce_37

action_90 _ = happyReduce_31

action_91 (46) = happyShift action_20
action_91 (49) = happyShift action_21
action_91 (55) = happyShift action_22
action_91 (56) = happyShift action_23
action_91 (57) = happyShift action_24
action_91 (58) = happyShift action_25
action_91 (59) = happyShift action_26
action_91 (22) = happyGoto action_28
action_91 (24) = happyGoto action_111
action_91 _ = happyFail

action_92 (47) = happyShift action_77
action_92 (14) = happyGoto action_110
action_92 _ = happyFail

action_93 (46) = happyShift action_20
action_93 (49) = happyShift action_21
action_93 (50) = happyShift action_109
action_93 (55) = happyShift action_22
action_93 (56) = happyShift action_23
action_93 (57) = happyShift action_24
action_93 (58) = happyShift action_25
action_93 (59) = happyShift action_26
action_93 (22) = happyGoto action_28
action_93 (24) = happyGoto action_81
action_93 (25) = happyGoto action_108
action_93 _ = happyFail

action_94 _ = happyReduce_40

action_95 (46) = happyShift action_20
action_95 (49) = happyShift action_21
action_95 (55) = happyShift action_22
action_95 (56) = happyShift action_23
action_95 (57) = happyShift action_24
action_95 (58) = happyShift action_25
action_95 (59) = happyShift action_26
action_95 (22) = happyGoto action_28
action_95 (24) = happyGoto action_107
action_95 _ = happyFail

action_96 (50) = happyShift action_106
action_96 _ = happyFail

action_97 (54) = happyShift action_105
action_97 _ = happyFail

action_98 (49) = happyShift action_104
action_98 _ = happyFail

action_99 (27) = happyShift action_59
action_99 (48) = happyShift action_103
action_99 (10) = happyGoto action_101
action_99 _ = happyFail

action_100 _ = happyReduce_13

action_101 _ = happyReduce_20

action_102 _ = happyReduce_14

action_103 _ = happyReduce_15

action_104 (55) = happyShift action_97
action_104 (8) = happyGoto action_121
action_104 _ = happyReduce_10

action_105 (55) = happyShift action_120
action_105 _ = happyFail

action_106 (28) = happyShift action_119
action_106 _ = happyReduce_8

action_107 (34) = happyShift action_35
action_107 (35) = happyShift action_36
action_107 (36) = happyShift action_37
action_107 (37) = happyShift action_38
action_107 (39) = happyShift action_39
action_107 (40) = happyShift action_40
action_107 (41) = happyShift action_41
action_107 (42) = happyShift action_42
action_107 (43) = happyShift action_43
action_107 (44) = happyShift action_44
action_107 (45) = happyShift action_45
action_107 (52) = happyShift action_118
action_107 (53) = happyShift action_47
action_107 _ = happyFail

action_108 (50) = happyShift action_117
action_108 (51) = happyShift action_85
action_108 _ = happyFail

action_109 _ = happyReduce_64

action_110 _ = happyReduce_36

action_111 (34) = happyShift action_35
action_111 (35) = happyShift action_36
action_111 (36) = happyShift action_37
action_111 (37) = happyShift action_38
action_111 (39) = happyShift action_39
action_111 (40) = happyShift action_40
action_111 (41) = happyShift action_41
action_111 (42) = happyShift action_42
action_111 (43) = happyShift action_43
action_111 (44) = happyShift action_44
action_111 (45) = happyShift action_45
action_111 (47) = happyShift action_77
action_111 (53) = happyShift action_47
action_111 (14) = happyGoto action_116
action_111 _ = happyFail

action_112 _ = happyReduce_35

action_113 _ = happyReduce_32

action_114 _ = happyReduce_29

action_115 (34) = happyShift action_35
action_115 (35) = happyShift action_36
action_115 (36) = happyShift action_37
action_115 (37) = happyShift action_38
action_115 (39) = happyShift action_39
action_115 (40) = happyShift action_40
action_115 (41) = happyShift action_41
action_115 (42) = happyShift action_42
action_115 (43) = happyShift action_43
action_115 (44) = happyShift action_44
action_115 (45) = happyShift action_45
action_115 (53) = happyShift action_47
action_115 _ = happyReduce_68

action_116 _ = happyReduce_33

action_117 _ = happyReduce_63

action_118 _ = happyReduce_41

action_119 (55) = happyShift action_124
action_119 _ = happyFail

action_120 (51) = happyShift action_123
action_120 _ = happyReduce_11

action_121 (50) = happyShift action_122
action_121 _ = happyFail

action_122 (47) = happyShift action_77
action_122 (54) = happyShift action_127
action_122 (14) = happyGoto action_126
action_122 _ = happyFail

action_123 (55) = happyShift action_97
action_123 (8) = happyGoto action_125
action_123 _ = happyReduce_10

action_124 _ = happyReduce_9

action_125 _ = happyReduce_12

action_126 _ = happyReduce_17

action_127 (55) = happyShift action_128
action_127 _ = happyFail

action_128 (47) = happyShift action_77
action_128 (14) = happyGoto action_129
action_128 _ = happyFail

action_129 _ = happyReduce_18

happyReduce_1 = happyMonadReduce 2 4 happyReduction_1
happyReduction_1 ((HappyAbsSyn13  happy_var_2) `HappyStk`
	(HappyAbsSyn6  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseProgram happy_var_1 happy_var_2  l))
	) (\r -> happyReturn (HappyAbsSyn4 r))

happyReduce_2 = happyMonadReduce 1 4 happyReduction_2
happyReduction_2 ((HappyAbsSyn6  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseProgram happy_var_1 (ASTStatements [])  l))
	) (\r -> happyReturn (HappyAbsSyn4 r))

happyReduce_3 = happyMonadReduce 1 4 happyReduction_3
happyReduction_3 ((HappyAbsSyn13  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseProgram (ASTClasses []) happy_var_1  l))
	) (\r -> happyReturn (HappyAbsSyn4 r))

happyReduce_4 = happyMonadReduce 0 4 happyReduction_4
happyReduction_4 (happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseProgram (ASTClasses []) (ASTStatements [])  l))
	) (\r -> happyReturn (HappyAbsSyn4 r))

happyReduce_5 = happyMonadReduce 2 5 happyReduction_5
happyReduction_5 ((HappyAbsSyn9  happy_var_2) `HappyStk`
	(HappyAbsSyn7  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassExp happy_var_1 happy_var_2 l))
	) (\r -> happyReturn (HappyAbsSyn5 r))

happyReduce_6 = happyMonadReduce 1 6 happyReduction_6
happyReduction_6 ((HappyAbsSyn5  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassExps happy_var_1 (ASTClasses []) l))
	) (\r -> happyReturn (HappyAbsSyn6 r))

happyReduce_7 = happyMonadReduce 2 6 happyReduction_7
happyReduction_7 ((HappyAbsSyn5  happy_var_2) `HappyStk`
	(HappyAbsSyn6  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassExps happy_var_2 happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn6 r))

happyReduce_8 = happyMonadReduce 5 7 happyReduction_8
happyReduction_8 (_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassSig happy_var_2 Nothing happy_var_4 l))
	) (\r -> happyReturn (HappyAbsSyn7 r))

happyReduce_9 = happyMonadReduce 7 7 happyReduction_9
happyReduction_9 ((HappyTerminal (Id happy_var_7)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassSig happy_var_2 (Just happy_var_7) happy_var_4 l))
	) (\r -> happyReturn (HappyAbsSyn7 r))

happyReduce_10 = happyMonadReduce 0 8 happyReduction_10
happyReduction_10 (happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ ASTFormalArgs []))
	) (\r -> happyReturn (HappyAbsSyn8 r))

happyReduce_11 = happyMonadReduce 3 8 happyReduction_11
happyReduction_11 ((HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseFormalArgs happy_var_1 happy_var_3  (ASTFormalArgs []) l))
	) (\r -> happyReturn (HappyAbsSyn8 r))

happyReduce_12 = happyMonadReduce 5 8 happyReduction_12
happyReduction_12 ((HappyAbsSyn8  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseFormalArgs happy_var_1 happy_var_3 happy_var_5 l))
	) (\r -> happyReturn (HappyAbsSyn8 r))

happyReduce_13 = happyMonadReduce 3 9 happyReduction_13
happyReduction_13 (_ `HappyStk`
	(HappyAbsSyn13  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassBody happy_var_2 (ASTMethods []) l))
	) (\r -> happyReturn (HappyAbsSyn9 r))

happyReduce_14 = happyMonadReduce 3 9 happyReduction_14
happyReduction_14 (_ `HappyStk`
	(HappyAbsSyn11  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassBody (ASTStatements []) happy_var_2 l))
	) (\r -> happyReturn (HappyAbsSyn9 r))

happyReduce_15 = happyMonadReduce 4 9 happyReduction_15
happyReduction_15 (_ `HappyStk`
	(HappyAbsSyn11  happy_var_3) `HappyStk`
	(HappyAbsSyn13  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassBody happy_var_2 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn9 r))

happyReduce_16 = happyMonadReduce 2 9 happyReduction_16
happyReduction_16 (_ `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassBody (ASTStatements []) (ASTMethods []) l))
	) (\r -> happyReturn (HappyAbsSyn9 r))

happyReduce_17 = happyMonadReduce 6 10 happyReduction_17
happyReduction_17 ((HappyAbsSyn14  happy_var_6) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseMethodExp happy_var_2 happy_var_4 Nothing happy_var_6 l))
	) (\r -> happyReturn (HappyAbsSyn10 r))

happyReduce_18 = happyMonadReduce 8 10 happyReduction_18
happyReduction_18 ((HappyAbsSyn14  happy_var_8) `HappyStk`
	(HappyTerminal (Id happy_var_7)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseMethodExp happy_var_2 happy_var_4 (Just happy_var_7) happy_var_8 l))
	) (\r -> happyReturn (HappyAbsSyn10 r))

happyReduce_19 = happyMonadReduce 1 11 happyReduction_19
happyReduction_19 ((HappyAbsSyn10  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseMethods happy_var_1 (ASTMethods []) l))
	) (\r -> happyReturn (HappyAbsSyn11 r))

happyReduce_20 = happyMonadReduce 2 11 happyReduction_20
happyReduction_20 ((HappyAbsSyn10  happy_var_2) `HappyStk`
	(HappyAbsSyn11  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseMethods happy_var_2 happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn11 r))

happyReduce_21 = happyMonadReduce 1 12 happyReduction_21
happyReduction_21 ((HappyAbsSyn16  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn12 r))

happyReduce_22 = happyMonadReduce 1 12 happyReduction_22
happyReduction_22 ((HappyAbsSyn15  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn12 r))

happyReduce_23 = happyMonadReduce 1 12 happyReduction_23
happyReduction_23 ((HappyAbsSyn20  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn12 r))

happyReduce_24 = happyMonadReduce 1 12 happyReduction_24
happyReduction_24 ((HappyAbsSyn21  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn12 r))

happyReduce_25 = happyMonadReduce 1 12 happyReduction_25
happyReduction_25 ((HappyAbsSyn23  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn12 r))

happyReduce_26 = happyMonadReduce 1 13 happyReduction_26
happyReduction_26 ((HappyAbsSyn12  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatements happy_var_1 (ASTStatements []) l))
	) (\r -> happyReturn (HappyAbsSyn13 r))

happyReduce_27 = happyMonadReduce 2 13 happyReduction_27
happyReduction_27 ((HappyAbsSyn12  happy_var_2) `HappyStk`
	(HappyAbsSyn13  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatements happy_var_2 happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn13 r))

happyReduce_28 = happyMonadReduce 2 14 happyReduction_28
happyReduction_28 (_ `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementBlock (ASTStatements []) l))
	) (\r -> happyReturn (HappyAbsSyn14 r))

happyReduce_29 = happyMonadReduce 3 14 happyReduction_29
happyReduction_29 (_ `HappyStk`
	(HappyAbsSyn13  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementBlock happy_var_2  l))
	) (\r -> happyReturn (HappyAbsSyn14 r))

happyReduce_30 = happyMonadReduce 3 15 happyReduction_30
happyReduction_30 ((HappyAbsSyn14  happy_var_3) `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseWhileExp happy_var_2 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn15 r))

happyReduce_31 = happyMonadReduce 4 16 happyReduction_31
happyReduction_31 ((HappyAbsSyn19  happy_var_4) `HappyStk`
	(HappyAbsSyn14  happy_var_3) `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseIfExp happy_var_2 happy_var_3 (ASTElifs []) happy_var_4 l))
	) (\r -> happyReturn (HappyAbsSyn16 r))

happyReduce_32 = happyMonadReduce 5 16 happyReduction_32
happyReduction_32 ((HappyAbsSyn19  happy_var_5) `HappyStk`
	(HappyAbsSyn18  happy_var_4) `HappyStk`
	(HappyAbsSyn14  happy_var_3) `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseIfExp happy_var_2 happy_var_3 happy_var_4 happy_var_5 l))
	) (\r -> happyReturn (HappyAbsSyn16 r))

happyReduce_33 = happyMonadReduce 3 17 happyReduction_33
happyReduction_33 ((HappyAbsSyn14  happy_var_3) `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseElifExp happy_var_2 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn17 r))

happyReduce_34 = happyMonadReduce 1 18 happyReduction_34
happyReduction_34 ((HappyAbsSyn17  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseElifs happy_var_1 (ASTElifs []) l))
	) (\r -> happyReturn (HappyAbsSyn18 r))

happyReduce_35 = happyMonadReduce 2 18 happyReduction_35
happyReduction_35 ((HappyAbsSyn17  happy_var_2) `HappyStk`
	(HappyAbsSyn18  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseElifs happy_var_2 happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn18 r))

happyReduce_36 = happyMonadReduce 2 19 happyReduction_36
happyReduction_36 ((HappyAbsSyn14  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseElseExp (Just happy_var_2) l))
	) (\r -> happyReturn (HappyAbsSyn19 r))

happyReduce_37 = happyMonadReduce 0 19 happyReduction_37
happyReduction_37 (happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseElseExp Nothing l))
	) (\r -> happyReturn (HappyAbsSyn19 r))

happyReduce_38 = happyMonadReduce 2 20 happyReduction_38
happyReduction_38 (_ `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseReturnExp Nothing l))
	) (\r -> happyReturn (HappyAbsSyn20 r))

happyReduce_39 = happyMonadReduce 3 20 happyReduction_39
happyReduction_39 (_ `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseReturnExp (Just happy_var_2) l))
	) (\r -> happyReturn (HappyAbsSyn20 r))

happyReduce_40 = happyMonadReduce 4 21 happyReduction_40
happyReduction_40 (_ `HappyStk`
	(HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn22  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseAssignmentExp happy_var_1 Nothing happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn21 r))

happyReduce_41 = happyMonadReduce 6 21 happyReduction_41
happyReduction_41 (_ `HappyStk`
	(HappyAbsSyn24  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn22  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseAssignmentExp happy_var_1 (Just happy_var_3) happy_var_5 l))
	) (\r -> happyReturn (HappyAbsSyn21 r))

happyReduce_42 = happyMonadReduce 1 22 happyReduction_42
happyReduction_42 ((HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseLeftExp Nothing happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn22 r))

happyReduce_43 = happyMonadReduce 3 22 happyReduction_43
happyReduction_43 ((HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseLeftExp (Just happy_var_1) happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn22 r))

happyReduce_44 = happyMonadReduce 2 23 happyReduction_44
happyReduction_44 (_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseBareExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn23 r))

happyReduce_45 = happyMonadReduce 1 24 happyReduction_45
happyReduction_45 ((HappyTerminal (QString happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ ASTRightExp (StrExp happy_var_1 l) ))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_46 = happyMonadReduce 1 24 happyReduction_46
happyReduction_46 ((HappyTerminal (Qint happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ ASTRightExp (IntExp (read happy_var_1) l) ))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_47 = happyMonadReduce 1 24 happyReduction_47
happyReduction_47 (_ `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ ASTRightExp (BoolExp False l) ))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_48 = happyMonadReduce 1 24 happyReduction_48
happyReduction_48 (_ `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ ASTRightExp (BoolExp True l) ))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_49 = happyMonadReduce 1 24 happyReduction_49
happyReduction_49 ((HappyAbsSyn22  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ ASTRightExp (LeftExp (getLeftExp happy_var_1) l) ))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_50 = happyMonadReduce 3 24 happyReduction_50
happyReduction_50 (_ `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseParExp happy_var_2 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_51 = happyMonadReduce 3 24 happyReduction_51
happyReduction_51 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "+" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_52 = happyMonadReduce 3 24 happyReduction_52
happyReduction_52 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "-" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_53 = happyMonadReduce 3 24 happyReduction_53
happyReduction_53 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "*" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_54 = happyMonadReduce 3 24 happyReduction_54
happyReduction_54 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "/" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_55 = happyMonadReduce 3 24 happyReduction_55
happyReduction_55 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "==" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_56 = happyMonadReduce 3 24 happyReduction_56
happyReduction_56 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "<=" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_57 = happyMonadReduce 3 24 happyReduction_57
happyReduction_57 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp ">=" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_58 = happyMonadReduce 3 24 happyReduction_58
happyReduction_58 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "<" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_59 = happyMonadReduce 3 24 happyReduction_59
happyReduction_59 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp ">" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_60 = happyMonadReduce 3 24 happyReduction_60
happyReduction_60 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "and" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_61 = happyMonadReduce 3 24 happyReduction_61
happyReduction_61 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "or" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_62 = happyMonadReduce 2 24 happyReduction_62
happyReduction_62 ((HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseNotExp happy_var_2 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_63 = happyMonadReduce 6 24 happyReduction_63
happyReduction_63 (_ `HappyStk`
	(HappyAbsSyn25  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseFuncCallExp happy_var_1 happy_var_3 happy_var_5 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_64 = happyMonadReduce 5 24 happyReduction_64
happyReduction_64 (_ `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseFuncCallExp happy_var_1 happy_var_3 (ASTActualArgs []) l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_65 = happyMonadReduce 4 24 happyReduction_65
happyReduction_65 (_ `HappyStk`
	(HappyAbsSyn25  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseConstructorExp happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_66 = happyMonadReduce 3 24 happyReduction_66
happyReduction_66 (_ `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseConstructorExp happy_var_1 (ASTActualArgs []) l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_67 = happyMonadReduce 1 25 happyReduction_67
happyReduction_67 ((HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseActualArgs happy_var_1 (ASTActualArgs []) ))
	) (\r -> happyReturn (HappyAbsSyn25 r))

happyReduce_68 = happyMonadReduce 3 25 happyReduction_68
happyReduction_68 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn25  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseActualArgs happy_var_3 happy_var_1 ))
	) (\r -> happyReturn (HappyAbsSyn25 r))

happyNewToken action sts stk
	= happyLexer(\tk -> 
	let cont i = action i i tk (HappyState action) sts stk in
	case tk of {
	EOF -> action 60 60 tk (HappyState action) sts stk;
	Class happy_dollar_dollar -> cont 26;
	Def happy_dollar_dollar -> cont 27;
	Extends happy_dollar_dollar -> cont 28;
	If happy_dollar_dollar -> cont 29;
	Elif happy_dollar_dollar -> cont 30;
	Else happy_dollar_dollar -> cont 31;
	While happy_dollar_dollar -> cont 32;
	Return happy_dollar_dollar -> cont 33;
	Plus happy_dollar_dollar -> cont 34;
	Minus happy_dollar_dollar -> cont 35;
	Times happy_dollar_dollar -> cont 36;
	Divide happy_dollar_dollar -> cont 37;
	Gets happy_dollar_dollar -> cont 38;
	Equals happy_dollar_dollar -> cont 39;
	Atmost happy_dollar_dollar -> cont 40;
	Atleast happy_dollar_dollar -> cont 41;
	Less happy_dollar_dollar -> cont 42;
	More happy_dollar_dollar -> cont 43;
	And happy_dollar_dollar -> cont 44;
	Or happy_dollar_dollar -> cont 45;
	Not happy_dollar_dollar -> cont 46;
	LeftCurly happy_dollar_dollar -> cont 47;
	RightCurly happy_dollar_dollar -> cont 48;
	LeftPar happy_dollar_dollar -> cont 49;
	RightPar happy_dollar_dollar -> cont 50;
	Comma happy_dollar_dollar -> cont 51;
	Semi happy_dollar_dollar -> cont 52;
	Period happy_dollar_dollar -> cont 53;
	Colon happy_dollar_dollar -> cont 54;
	Id happy_dollar_dollar -> cont 55;
	Qint happy_dollar_dollar -> cont 56;
	BooleanTrue -> cont 57;
	BooleanFalse -> cont 58;
	QString happy_dollar_dollar -> cont 59;
	_ -> happyError' tk
	})

happyError_ 60 tk = happyError' tk
happyError_ _ tk = happyError' tk

happyThen :: () => P a -> (a -> P b) -> P b
happyThen = (thenP)
happyReturn :: () => a -> P a
happyReturn = (returnP)
happyThen1 = happyThen
happyReturn1 :: () => a -> P a
happyReturn1 = happyReturn
happyError' :: () => (Token) -> P a
happyError' tk = parseErrorP tk

quackParse = happySomeParser where
  happySomeParser = happyThen (happyParse action_0) (\x -> case x of {HappyAbsSyn4 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


-- parse ASTNode functions
parseProgram :: ASTNode -> ASTNode -> Int -> ASTNode
parseProgram cs ss line = let ASTClasses cs'  = cs
                              ASTStatements ss'  = ss
                           in
                              ASTProgram (ProgramExp cs' ss' line) 

parseClassExp :: ASTNode -> ASTNode -> Int -> ASTNode
parseClassExp s b line = let ASTClassSig s'  = s
                             ASTClassBody b'  = b
                          in 
                             ASTClassExp (ClassDecExp s' b' line)  

parseClassExps :: ASTNode -> ASTNode -> Int -> ASTNode
parseClassExps c cs line = let ASTClassExp c'  = c 
                               ASTClasses cs'   = cs 
                            in ASTClasses (c':cs') 







parseClassSig :: String -> Maybe String -> ASTNode -> Int -> ASTNode
parseClassSig n e as line = let ASTFormalArgs as'  = as
                            in case e of
                             Just e' -> ASTClassSig (ExtendedClassSigExp n as' e' line) 
                             Nothing -> ASTClassSig (ClassSigExp n as' line) 



parseClassBody :: ASTNode -> ASTNode -> Int -> ASTNode
parseClassBody ss ms line = let ASTStatements ss'  = ss
                                ASTMethods ms'  = ms
                             in
                                ASTClassBody (ClassBodyExp ss' ms' line) 


parseFormalArgs :: String -> String -> ASTNode -> Int -> ASTNode
parseFormalArgs a t as line = let a' = FormalArgExp a t line
                                  ASTFormalArgs as'  = as 
                              in ASTFormalArgs (a':as') 


parseMethodExp :: String -> ASTNode -> Maybe String -> ASTNode -> Int -> ASTNode
parseMethodExp i as t sb line = let ASTFormalArgs as'  = as
                                    ASTStatementBlock sb'  = sb
                                 in case t of
                                      Just t' -> ASTMethod (TypedMethodExp i as' t' sb' line) 
                                      Nothing -> ASTMethod (MethodExp i as' sb' line) 



parseMethods :: ASTNode -> ASTNode -> Int -> ASTNode
parseMethods m ms line = let  ASTMethod m'  = m 
                              ASTMethods ms'   = ms 
                            in ASTMethods (m':ms') 


parseStatementExp :: ASTNode -> Int -> ASTNode
parseStatementExp e line = case e of
                        ASTReturnExp e'  -> ASTStatement (ReturnExp e' line)  
                        ASTBareExp e' -> ASTStatement (BareExp e' line)  
                        ASTAssignmentExp e'  -> ASTStatement (AssignmentExp e' line)  
                        ASTWhileExp e' ->  ASTStatement (WhileExp e' line)
                        ASTIfExp e' -> ASTStatement (IfExp e' line)




parseStatements :: ASTNode -> ASTNode -> Int -> ASTNode
parseStatements s ss line = let ASTStatement s'  = s
                                ASTStatements ss' = ss
                            in
                             case ss' of
                                [] -> ASTStatements [s'] 
                                _ -> ASTStatements (s':ss') 


parseStatementBlock :: ASTNode -> Int -> ASTNode
parseStatementBlock ss line = let ASTStatements ss'  = ss in
                                ASTStatementBlock (StatementBlockExp ss' line)  

parseWhileExp :: ASTNode -> ASTNode -> Int -> ASTNode
parseWhileExp c b line = let ASTRightExp c'  = c
                             ASTStatementBlock b'  = b
                          in
                              ASTWhileExp (WhileLoop (c',b') line) 



parseIfExp :: ASTNode -> ASTNode -> ASTNode -> ASTNode -> Int -> ASTNode
parseIfExp c sb el e line = let ASTRightExp c'  = c
                                ASTStatementBlock sb'  = sb
                                ASTElifs el'  = el
                                ASTElseExp e'  = e
                              in
                                 ASTIfExp (IfBlockExp c' sb' el' e' line) 


parseElifExp :: ASTNode -> ASTNode -> Int -> ASTNode
parseElifExp e sb line = let ASTRightExp e'  = e
                             ASTStatementBlock sb'  = sb 
                          in 
                            ASTElifExp (ElifBlockExp e' sb' line) 


parseElifs :: ASTNode -> ASTNode -> Int -> ASTNode
parseElifs e es line = let ASTElifExp e'  = e 
                           ASTElifs es'   = es 
                            in ASTElifs (e':es') 



parseElseExp :: Maybe ASTNode -> Int -> ASTNode
parseElseExp sb line = case sb of
                          Just sb' -> let ASTStatementBlock sb''  = sb' in
                                          ASTElseExp (ElseBlockExp sb'' line) 
                          Nothing -> ASTElseExp (ElseBlockExp (StatementBlockExp [] line) line) 



parseReturnExp :: Maybe ASTNode -> Int -> ASTNode
parseReturnExp r line = case r of
                      Just r' -> ASTReturnExp (RetExp (getRightExp r') line) 
                      Nothing -> ASTReturnExp $ NoReturnExp line


parseAssignmentExp :: ASTNode -> Maybe String -> ASTNode -> Int -> ASTNode
parseAssignmentExp l t r line = let l' = getLeftExp l
                                    r' = getRightExp r
                                 in
                                    case t of
                                        Just t' -> ASTAssignmentExp (TypedAssignExp l' t' r' line) 
                                        Nothing -> ASTAssignmentExp (UntypedAssignExp l'  r' line) 
                                                 




parseLeftExp :: Maybe ASTNode -> String -> Int -> ASTNode
parseLeftExp ast id line = case ast of
                              Just e -> let e' = getRightExp e in
                                                        ASTLeftExp (ObjIdRef e' id line) 
                              Nothing -> ASTLeftExp (IdRef id line) 







parseBareExp e line = ASTBareExp (RightExp (getRightExp e) line) 


parseActualArgs :: ASTNode -> ASTNode  -> ASTNode
parseActualArgs a as  = let ASTRightExp a' = a
                            ASTActualArgs as' = as
                          in
                           case as' of
                              [] -> ASTActualArgs $  [a'] 
                              _ -> ASTActualArgs $  (a':as') 


parseFuncCallExp e id args line = let ASTActualArgs as  = args 
                                    in case e of
                                        ASTRightExp e'  -> ASTRightExp (FuncCallExp e' id as  line) 
                                        ASTLeftExp l  -> case l of
                                                          IdRef s line' -> ASTRightExp (NullExp (show l) line) 
                                                          ObjIdRef  r s  line' -> ASTRightExp r 
                                        _ -> ASTRightExp (NullExp "" line) 


 
parseConstructorExp id args line = let ASTActualArgs as  = args in
                                ASTRightExp (ConstructorExp id as line)  




parseNotExp e line = let ASTRightExp e'  = e in
                        ASTRightExp (NotExp e' line) 


parseBinExp op l r line = let ASTRightExp l'  = l
                              ASTRightExp r'  = r
                          in
                              ASTRightExp (BinaryExp op l' r' line) 

parseParExp e line = let ASTRightExp e' = e in
                        ASTRightExp $ ParExp e' line


data Result a = Ok a | Failed String
type P a =  String -> Int -> Result a



thenP :: P a -> (a -> P b) -> P b
m `thenP` k = \s l -> case m s l of
                Ok a -> k a s l
                Failed e -> Failed e


returnP :: a -> P a
returnP a = \s l -> Ok a


failP :: String -> P a
failP err = \s l -> Failed (err)



catchP :: P a -> (String -> P a) -> P a
catchP m k = \s l -> case m s l of
                        Ok a -> Ok a
                        Failed e -> k e s l


getLineNo :: P Int
getLineNo = \s l -> Ok l


happyLexer :: (Token -> P a) -> P a
happyLexer cont s  = case s of 
                        ('\n':[]) -> (\line -> happyLexer cont [] (line))
                        ('\n':ss) ->  (\line -> happyLexer cont ss (line+1))
                        _ -> (\l -> case happyScanner s l of
                                Left lexErr -> Failed (lexErr ++ " at line " ++ (show l))
                                Right (tok,s',l') -> case tok of 
                                                    Comment _ -> happyLexer cont s' l'
                                                    BlockComment _ -> happyLexer cont s' l'
                                                    _ -> cont tok s' l')
            

printHappyLexer ::  String -> IO ()
printHappyLexer  s  = do
                        putStrLn $ s                      
                        putStrLn $ show (happyScanner s 1) 

parseErrorP :: Token -> P a 
parseErrorP t = \s l -> case t of
                        _ -> Failed ("Parse error at line " ++ (show l))






cycleResult  (cls,  line, (err, path)) = 
          let pathStr = cls ++ (concat $ map ( ((++) " -> ")) path)
            in case err of
              NoErr -> ""
              Cycle -> "Cycle in class heirarchy for " ++ (show cls) ++ " at line " ++ (show line) ++ " (" ++ pathStr ++ ")\n"
              DeadEnd c -> "Dead end in class heirarchy for " ++ (show cls) ++ " at line " ++ (show line)  ++ ", extends unknown class " ++ (show c)  
                              ++ " (" ++ pathStr ++ "->" ++ c ++ ")\n"



--getInheritanceErrors ::  [(String,(Int,(Bool,[QId])))] -> [String]
getInheritanceErrors classes = let m (k, (line, (res,path) ) ) = cycleResult (k,line,(res,path)) in map m classes 

printInheritanceErrors ast = let inheritances = getInheritanceErrors $ classInheritances ast
                              in
                                  mapM_ (putStr) inheritances    

printUndeclaredConsErrors ast = let errors = badConstructors ast in
                                    case errors of
                                     [] -> putStr ""
                                     _ -> mapM_ (\(i,l) -> putStrLn $ "Undeclared constructor \""++i++"\" at line " ++ (show l)) errors



old_main = do
        p <- getContents
        ast <- return $ quackParse p 1
        
        case ast of
          Ok x -> do
                   let st = buildSymbolTable x in
                  {-    case (getMethodField st "Square" "translate" "wat",getMethod st "Square" "translate",getClass st "Square") of
                            (Just f,Just m,Just c) -> do
                                                --  print $ Map.toList (syms m)
                                                  print $  typeFields st {-"Square" "translate"-} ( (syms m)) --Map.empty
                            (Nothing,Just m,Just c) -> do print st; putStrLn "------";print $  typeFields st {-"Square" "translate"-} ( (syms m))-- Map.empty
                            (_,Nothing,_) -> putStrLn "did not find method"
                            (_,Nothing,_) -> putStrLn "did not find class"
                            (_,_,_) -> putStrLn "did not find class and method" -}
                      case (getClass st "Pt") of
                            Just sqClass -> let newSyms = (typeClass sqClass st) in
                                              case getClass newSyms "Pt" of
                                                Just c -> let inf = inferClasses . Map.toList $ (quackClasses newSyms)
                                                          in do print inf
                                                                print $  map (\dep -> showDepChain $ getDepChain (let (n,d) = dep in d) (Map.fromList inf) newSyms [])  inf
                                                                print $  map (\dep -> typeDepChain newSyms $ getDepChain (let (n,d) = dep in d) (Map.fromList inf) newSyms [])  inf
                                                Nothing -> print ""
                                               {-  case ((getMethod  (typeClass sqClass st) "Pt" "STR" ),(getMethod (typeClass sqClass st) "Pt" "newMethod")) of
                                                  (Just c,Just c1) -> let newSyms' =  resolveClassMethods [c,c1] (typeClass sqClass st) in
                                                                         case getClass newSyms "Pt" of
                                                                          Just c -> print c
                                                                          Nothing -> print ""
                                                  _ -> putStrLn "Didn't find " -}
                                
                            Nothing -> putStrLn "Didn't find class square"
                 {-  putStrLn "Finished parse with no syntax errors." 
                   printInheritanceErrors x                
                   printUndeclaredConsErrors x -}

                          
          Failed w -> print w


{-
imc_main = do 
    p <- getContents
    ast <- return $ quackParse p 1
    case ast of
      Failed w -> print w
      Ok x -> do
                let st = buildSymbolTable x 
                    quackCode = toImc st x in
                 do --putStrLn $ c_boilerplate (declareTemps store) quackCode
                    putStrLn $ show x-}


testInheritedMethods  =
  do 
      p <- getContents
      ast <- return $ quackParse p 1
      case ast of
        Failed w -> print w
        Ok x -> let st = buildSymbolTable x
                    out = case getClass st "Pt" of
                           Just c -> (show c) ++ "\n" ++ (show $ getInheritedMethods (typeClass c st) c )
                           Nothing -> ""
                in putStrLn out
                    --putStrLn $ c_boilerplate (declareTemps store) quackCode

testImcTable =
  do
    p <- getContents
    ast <- return $ quackParse p 1
    case ast of
      Failed w -> print w
      Ok x -> let st = buildSymbolTable x
                  st' = typeProgram st
                  imcSt = buildImcSymsTable x st' (ImTable {imClasses=Map.empty})
              in putStrLn (show imcSt)


testImc = 
    do
      p <- getContents
      ast <- return $ quackParse p 1
      case ast of
        Failed w -> print w
        Ok x -> let st = buildSymbolTable x
                    st' = typeProgram st
                    imcSt = buildImcSymsTable x st' (ImTable {imClasses=Map.empty})
                in do
                       putStr . c_boilerplate $ (toImc st' x imcSt)


{-testImcTable = do
  p <- getContents
  ast <- return $ quackParse p 1-}
{-
testTemps = do
    p <- getContents
    ast <- return $ quackParse p 1
    case ast of
      Failed w -> print w
      Ok x -> let store = makeTemps x emptystore 
                  x' = replaceTemps x store 
                  st = buildSymbolTable x
                  st' = typeClassN  "Pt" st
                  quackCode = toImc st' x
              in do print . show $ x
                    print . show $ store
                    print . show $ x'
                    
                    putStrLn $ (printImc (declareTemps store) quackCode)
                  --  putStrLn $ show x-}


_testOutput  = 
    do
      p <- getContents
      let ast =  quackParse p 1 in
        case ast of
          Failed w -> putStr $ printf "Errors:\n%s" w
          Ok x -> let st = buildSymbolTable x
                      st' = typeProgram st
                      imcSt = buildImcSymsTable x st' (ImTable {imClasses=Map.empty})
                  in writeFile "output.c" (c_boilerplate  (toImc st' x imcSt))

testOutput =  _testOutput


testGettingInput = 
  do
      f <- getLine
 
      putStrLn f

main = testOutput
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "<built-in>" #-}
{-# LINE 1 "<command-line>" #-}
{-# LINE 8 "<command-line>" #-}
# 1 "/usr/include/stdc-predef.h" 1 3 4

# 17 "/usr/include/stdc-predef.h" 3 4










































{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "/usr/lib/ghc/include/ghcversion.h" #-}

















{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
-- Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp 

{-# LINE 13 "templates/GenericTemplate.hs" #-}

{-# LINE 46 "templates/GenericTemplate.hs" #-}








{-# LINE 67 "templates/GenericTemplate.hs" #-}

{-# LINE 77 "templates/GenericTemplate.hs" #-}

{-# LINE 86 "templates/GenericTemplate.hs" #-}

infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is (1), it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
        happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
         (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action

{-# LINE 155 "templates/GenericTemplate.hs" #-}

-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k - ((1) :: Int)) sts of
         sts1@(((st1@(HappyState (action))):(_))) ->
                let r = fn stk in  -- it doesn't hurt to always seq here...
                happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction

{-# LINE 256 "templates/GenericTemplate.hs" #-}
happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery ((1) is the error token)

-- parse error if we are in recovery and we fail again
happyFail (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--      trace "failing" $ 
        happyError_ i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  (1) tk old_st (((HappyState (action))):(sts)) 
                                                (saved_tok `HappyStk` _ `HappyStk` stk) =
--      trace ("discarding state, depth " ++ show (length stk))  $
        action (1) (1) tk (HappyState (action)) sts ((saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail  i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
        action (1) (1) tk (HappyState (action)) sts ( (HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--      happySeq = happyDoSeq
-- otherwise it emits
--      happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.

{-# LINE 322 "templates/GenericTemplate.hs" #-}
{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
