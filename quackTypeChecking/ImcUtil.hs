module ImcUtil (module ImcUtil) where

import QuackAST
import qualified Data.Map.Strict as Map
import Text.Printf (printf)



c_boilerplate :: String  -> String
c_boilerplate  quackCode =  printf baseString quackCode


baseString = "\
\#include <stdio.h>\n\
\#include <stdlib.h>\n\
\ \n\
\#include \"Builtins.h\"\n\
\ \n\
\void quackmain();\n \
\ \n\
\int main(int argc, char** argv) {\n\
\  quackmain();\n\
\  printf(\"--- Terminated successfully (woot!) ---\\n\");\n\
\  exit(0);\n\
\}\n\
\ \n\
\ %s\n"

isBuiltin :: String -> Bool
isBuiltin t = t `elem` ["Obj","Nothing","String","Int","Boolean"]

makeTempVar :: Int -> String
makeTempVar c = "tmp"++(show c)


argsCode args = 
  let argCode arg = 
        let FormalArgExp name typ _ = arg in
          printf "obj_%s %s" typ name
  in case args of
    [] -> ""
    a:[] -> argCode a
    a:as -> foldl (\a b -> a ++ "," ++ (argCode b)) (argCode $ head args) (tail args) 

decArgsCode args = 
  let argCode arg = 
        let FormalArgExp name typ _ = arg in
          printf "obj_%s" typ 
  in case args of
    [] -> ""
    a:[] -> argCode a
    a:as -> foldl (\a b -> a ++ "," ++ (argCode b)) (argCode $ head args) (tail args) 


makeMethodList :: String -> [(QId,QMethod)] -> [(QId,(String,String,String))]
makeMethodList cls methods =
  case methods of
    [] -> []
    ((name,m):ms) ->  
      let args = decArgsCode ([FormalArgExp "this" cls 0] ++ (margs m))
          mtyp = case (mtype m) of
                  Type t -> t
                  _ -> "BAD METHOD TYPE!!!!!!"
          sig = methodSigTemplate (mtyp) name args
          dec = printf "%s_method_%s" cls name
      in (name, (cls, dec, sig)):(makeMethodList cls ms)

getInheritedMethods symbols c = 
  let super = (csuper c)
      m' = Map.fromList $ makeMethodList (cname c) (Map.toList $ cmethods c) 
      m'' = Map.insert "PRINT" (cname c, printf "%s_method_PRINT" (cname c), methodSigTemplate "String" "PRINT" (decArgsCode [FormalArgExp "this" (cname c) 0])) m'
  in case getClass symbols super of
      Just superC -> 
        case (cname c,cname superC) of
            ("Obj","Obj") -> Map.fromList $ makeMethodList "Obj" (Map.toList $ cmethods c) 
            (_,_) -> let m' = Map.fromList $ makeMethodList (cname c) (Map.toList $ cmethods c) 
                         in Map.union  m''  (getInheritedMethods symbols superC)
      Nothing ->  m'' 


{- formatter className className className className className className className className className methodSigs className className -}
classTemplate = ("struct class_%s_struct;\n"++ --class name
                "typedef struct class_%s_struct* class_%s;\n"++ --class name --class name
                "typedef struct obj_%s_struct {\n"++ --class name
                "  class_%s  clazz;\n"++ --class name
                "  %s"++ --fields
                "} * obj_%s;\n"++ --class name
                "struct  class_%s_struct  the_class_%s_struct;\n"++ --class name class name
                "struct class_%s_struct {\n"++ --class name
                "  /* Method table */\n"++
                "\n%s\n"++ -- methods (signatures)
                "};\n"++
                "extern class_%s the_class_%s;\n"++ -- classname classname 
                "\n%s\n%s\nclass_%s the_class_%s = &the_class_%s_struct;") --methodCode classStruct classname classname classname



printMethodFirst cls = (printf "obj_String %s_method_PRINT(obj_%s this) {\n" cls cls ) ++ "fprintf(stdout, \"%s\","
printMethodSecond cls =  printf ("(%s_method_STRING(this))->text);\n"++ --sl
                       "return this;\n"++
                       "}\n") cls

printMethodTemplate cls = (printMethodFirst cls) ++ (printMethodSecond cls)

{-"  obj_Pt (*constructor) (obj_Int, obj_Int );  "++
"  obj_String (*STRING) (obj_Obj);           /* Inherit for now */"++
"  obj_Pt (*PRINT) (obj_Pt);                 /* Overridden */"++
"  obj_Boolean (*EQUALS) (obj_Obj, obj_Obj); /* Inherited */"++
"  obj_Pt (*PLUS) (obj_Pt, obj_Pt);          /* Introduced */"++

"  obj_Pt (*TEST) (obj_Pt, obj_Int);"++-}
methodSigTemplate :: String -> String -> String -> String
methodSigTemplate = printf ("obj_%s (*%s) (%s);\n") -- methodType methodName args

classStructTemplate :: String ->  String -> String -> String
classStructTemplate = 
  printf ("struct class_%s_struct the_class_%s_struct = {\n"++ -- className classname
        --  "new_%s,\n"++ -- classname
          "\n%s\n"++ --methodlist
          "};\n")
 
{-"/* Constructor */"++
"obj_Pt new_Pt(obj_Int x, obj_Int y ) {"++
"  obj_Pt new_thing = (obj_Pt)"++
"    malloc(sizeof(struct obj_Pt_struct));"++
"  new_thing->clazz = the_class_Pt;"++
"  /* Quack code: "++
"    this.x = x;"++
"    this.y = y; "++
"  */"++
"  new_thing->x = x;"++
"  new_thing->y = y; "++
"  return new_thing; "++
"}")-}

constructorTemplate :: String -> String -> String -> String -> String -> String -> String -> String -> String -> String
constructorTemplate = printf ("obj_%s new_%s(%s){\n"++ --clsname clsname args
                              "  obj_%s new = (obj_%s) malloc(sizeof(struct obj_%s_struct));\n"++ --clsname clsname clsname
                              "  new->clazz = the_class_%s;\n"++ --clsname
                              "  obj_%s this;\n"++ -- clsname
                              "  this = new;\n" ++
                              "\n%s\n"++ --constructor code
                              "  return new;\n}\n") 

methodTemplate :: String -> String -> String -> String -> String
methodTemplate = printf ("obj_%s %s(%s){\n"++ -- methodType methodName args
                  "%s\n"++ --methodBody
                  "}\n")
  

