#include <stdio.h>
#include <stdlib.h>
 
#include "Builtins.h"
 
void quackmain();
  
int main(int argc, char** argv) {
  quackmain();
  printf("\n--- Terminated successfully (woot!) ---\n");
  exit(0);
}
 
 struct class_Pt_struct;
typedef struct class_Pt_struct* class_Pt;
typedef struct obj_Pt_struct {
  class_Pt  clazz;
  obj_Int x;
obj_Int y;
} * obj_Pt;
struct  class_Pt_struct  the_class_Pt_struct;
struct class_Pt_struct {
  /* Method table */

obj_Pt (*constructor) (obj_Int,obj_Int);
obj_Boolean (*EQUALS) (obj_Obj,obj_Obj);
obj_Pt (*PLUS) (obj_Pt,obj_Pt);
obj_String (*PRINT) (obj_Pt);
obj_String (*STRING) (obj_Pt);
obj_Int (*_x) (obj_Pt);
obj_Int (*_y) (obj_Pt);

};
extern class_Pt the_class_Pt;

obj_Pt new_Pt(obj_Int x,obj_Int y){
  obj_Pt new = (obj_Pt) malloc(sizeof(struct obj_Pt_struct));
  new->clazz = the_class_Pt;
  obj_Pt this;
  this = new;


//StatementExp UntypedAssignExp

//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp1;
tmp1 = this;
;

obj_Int tmp2;
tmp2 = tmp1->x;


//LEFT EXPRESSION IdRef
obj_Int tmp0;
tmp0 = x;
;

this->x = tmp0;

//StatementExp UntypedAssignExp

//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp6;
tmp6 = this;
;

obj_Int tmp7;
tmp7 = tmp6->y;


//LEFT EXPRESSION IdRef
obj_Int tmp5;
tmp5 = y;
;

this->y = tmp5;


  return new;
}

obj_Pt Pt_method_PLUS(obj_Pt this,obj_Pt other){

//StatementExp UntypedAssignExp


//CONSTRUCTOR CALL EXP


//BINARY EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp0;
tmp0 = this;
;

obj_Int tmp1;
tmp1 = tmp0->x;
;



//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp2;
tmp2 = other;
;

obj_Int tmp3;
tmp3 = tmp2->x;
;

obj_Int tmp4;
tmp4 = tmp1->clazz->PLUS(tmp1,tmp3);



//BINARY EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp5;
tmp5 = this;
;

obj_Int tmp6;
tmp6 = tmp5->y;
;



//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp7;
tmp7 = other;
;

obj_Int tmp8;
tmp8 = tmp7->y;
;

obj_Int tmp9;
tmp9 = tmp6->clazz->PLUS(tmp6,tmp8);
obj_Pt tmp10;
tmp10 = the_class_Pt->constructor(tmp4, tmp9);
;

return tmp10;


}

obj_String Pt_method_STRING(obj_Pt this){

//StatementExp UntypedAssignExp


//BINARY EXPRESSION


//BINARY EXPRESSION


//BINARY EXPRESSION


//BINARY EXPRESSION



//FUNC CALL EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp0;
tmp0 = this;
;

obj_Int tmp1;
tmp1 = tmp0->x;
;


obj_String tmp2;
tmp2 = tmp1->clazz->STRING(tmp1);
;

obj_String tmp3;
tmp3 = str_literal("(")->clazz->PLUS(str_literal("("),tmp2);


obj_String tmp4;
tmp4 = tmp3->clazz->PLUS(tmp3,str_literal(","));



//FUNC CALL EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp5;
tmp5 = this;
;

obj_Int tmp6;
tmp6 = tmp5->y;
;


obj_String tmp7;
tmp7 = tmp6->clazz->STRING(tmp6);
;

obj_String tmp8;
tmp8 = tmp4->clazz->PLUS(tmp4,tmp7);


obj_String tmp9;
tmp9 = tmp8->clazz->PLUS(tmp8,str_literal(")"));

return tmp9;


}

obj_Int Pt_method__x(obj_Pt this){

//StatementExp UntypedAssignExp


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp0;
tmp0 = this;
;

obj_Int tmp1;
tmp1 = tmp0->x;
;

return tmp1;


}

obj_Int Pt_method__y(obj_Pt this){

//StatementExp UntypedAssignExp


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Pt tmp0;
tmp0 = this;
;

obj_Int tmp1;
tmp1 = tmp0->y;
;

return tmp1;


}

obj_String Pt_method_PRINT(obj_Pt this) {
fprintf(stdout, "%s",(this->clazz->STRING(this))->text);
return this;
}

struct class_Pt_struct the_class_Pt_struct = {

new_Pt,
Obj_method_EQUALS,
Pt_method_PLUS,
Pt_method_PRINT,
Pt_method_STRING,
Pt_method__x,
Pt_method__y
};

class_Pt the_class_Pt = &the_class_Pt_struct;


struct class_Rect_struct;
typedef struct class_Rect_struct* class_Rect;
typedef struct obj_Rect_struct {
  class_Rect  clazz;
  obj_Pt ll;
obj_Pt ur;
} * obj_Rect;
struct  class_Rect_struct  the_class_Rect_struct;
struct class_Rect_struct {
  /* Method table */

obj_Rect (*constructor) (obj_Pt,obj_Pt);
obj_Boolean (*EQUALS) (obj_Obj,obj_Obj);
obj_String (*PRINT) (obj_Rect);
obj_String (*STRING) (obj_Rect);
obj_Rect (*translate) (obj_Rect,obj_Pt);

};
extern class_Rect the_class_Rect;

obj_Rect new_Rect(obj_Pt ll,obj_Pt ur){
  obj_Rect new = (obj_Rect) malloc(sizeof(struct obj_Rect_struct));
  new->clazz = the_class_Rect;
  obj_Rect this;
  this = new;


//StatementExp UntypedAssignExp

//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Rect tmp1;
tmp1 = this;
;

obj_Pt tmp2;
tmp2 = tmp1->ll;


//LEFT EXPRESSION IdRef
obj_Pt tmp0;
tmp0 = ll;
;

this->ll = tmp0;

//StatementExp UntypedAssignExp

//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Rect tmp6;
tmp6 = this;
;

obj_Pt tmp7;
tmp7 = tmp6->ur;


//LEFT EXPRESSION IdRef
obj_Pt tmp5;
tmp5 = ur;
;

this->ur = tmp5;


  return new;
}

obj_String Rect_method_STRING(obj_Rect this){
obj_Pt lr;
obj_Pt ul;

//StatementExp UntypedAssignExp


//CONSTRUCTOR CALL EXP


//FUNC CALL EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Rect tmp0;
tmp0 = this;
;

obj_Pt tmp1;
tmp1 = tmp0->ur;
;


obj_Int tmp2;
tmp2 = tmp1->clazz->_y(tmp1);
;



//FUNC CALL EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Rect tmp3;
tmp3 = this;
;

obj_Pt tmp4;
tmp4 = tmp3->ll;
;


obj_Int tmp5;
tmp5 = tmp4->clazz->_x(tmp4);
;
obj_Pt tmp6;
tmp6 = the_class_Pt->constructor(tmp2, tmp5);
;

obj_Pt tmp9;
tmp9 = (lr = tmp6);

//StatementExp UntypedAssignExp


//CONSTRUCTOR CALL EXP


//FUNC CALL EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Rect tmp10;
tmp10 = this;
;

obj_Pt tmp11;
tmp11 = tmp10->ll;
;


obj_Int tmp12;
tmp12 = tmp11->clazz->_x(tmp11);
;



//FUNC CALL EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Rect tmp13;
tmp13 = this;
;

obj_Pt tmp14;
tmp14 = tmp13->ur;
;


obj_Int tmp15;
tmp15 = tmp14->clazz->_y(tmp14);
;
obj_Pt tmp16;
tmp16 = the_class_Pt->constructor(tmp12, tmp15);
;

obj_Pt tmp19;
tmp19 = (ul = tmp16);

//StatementExp UntypedAssignExp


//BINARY EXPRESSION


//FUNC CALL EXPRESSION

//LEFT EXPRESSION IdRef
obj_Pt tmp20;
tmp20 = lr;
;


obj_String tmp21;
tmp21 = tmp20->clazz->STRING(tmp20);
;



//FUNC CALL EXPRESSION

//LEFT EXPRESSION IdRef
obj_Pt tmp22;
tmp22 = ul;
;


obj_String tmp23;
tmp23 = tmp22->clazz->STRING(tmp22);
;

obj_String tmp24;
tmp24 = tmp21->clazz->PLUS(tmp21,tmp23);

return tmp24;


}

obj_Rect Rect_method_translate(obj_Rect this,obj_Pt delta){

//StatementExp UntypedAssignExp


//CONSTRUCTOR CALL EXP


//BINARY EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Rect tmp0;
tmp0 = this;
;

obj_Pt tmp1;
tmp1 = tmp0->ll;
;


//LEFT EXPRESSION IdRef
obj_Pt tmp2;
tmp2 = delta;
;

obj_Pt tmp3;
tmp3 = tmp1->clazz->PLUS(tmp1,tmp2);



//BINARY EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Rect tmp4;
tmp4 = this;
;

obj_Pt tmp5;
tmp5 = tmp4->ur;
;


//LEFT EXPRESSION IdRef
obj_Pt tmp6;
tmp6 = delta;
;

obj_Pt tmp7;
tmp7 = tmp5->clazz->PLUS(tmp5,tmp6);
obj_Rect tmp8;
tmp8 = the_class_Rect->constructor(tmp3, tmp7);
;

return tmp8;


}

obj_String Rect_method_PRINT(obj_Rect this) {
fprintf(stdout, "%s",(this->clazz->STRING(this))->text);
return this;
}

struct class_Rect_struct the_class_Rect_struct = {

new_Rect,
Obj_method_EQUALS,
Rect_method_PRINT,
Rect_method_STRING,
Rect_method_translate
};

class_Rect the_class_Rect = &the_class_Rect_struct;


struct class_Square_struct;
typedef struct class_Square_struct* class_Square;
typedef struct obj_Square_struct {
  class_Square  clazz;
  obj_Pt ll;
obj_Pt ur;
} * obj_Square;
struct  class_Square_struct  the_class_Square_struct;
struct class_Square_struct {
  /* Method table */

obj_Square (*constructor) (obj_Pt,obj_Int);
obj_Boolean (*EQUALS) (obj_Obj,obj_Obj);
obj_String (*PRINT) (obj_Square);
obj_String (*STRING) (obj_Rect);
obj_Rect (*translate) (obj_Rect,obj_Pt);

};
extern class_Square the_class_Square;

obj_Square new_Square(obj_Pt ll,obj_Int side){
  obj_Square new = (obj_Square) malloc(sizeof(struct obj_Square_struct));
  new->clazz = the_class_Square;
  obj_Square this;
  this = new;


//StatementExp UntypedAssignExp

//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Square tmp1;
tmp1 = this;
;

obj_Pt tmp2;
tmp2 = tmp1->ll;


//LEFT EXPRESSION IdRef
obj_Pt tmp0;
tmp0 = ll;
;

this->ll = tmp0;

//StatementExp UntypedAssignExp

//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Square tmp16;
tmp16 = this;
;

obj_Pt tmp17;
tmp17 = tmp16->ur;



//CONSTRUCTOR CALL EXP


//BINARY EXPRESSION


//FUNC CALL EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Square tmp5;
tmp5 = this;
;

obj_Pt tmp6;
tmp6 = tmp5->ll;
;


obj_Int tmp7;
tmp7 = tmp6->clazz->_x(tmp6);
;


//LEFT EXPRESSION IdRef
obj_Int tmp8;
tmp8 = side;
;

obj_Int tmp9;
tmp9 = tmp7->clazz->PLUS(tmp7,tmp8);



//BINARY EXPRESSION


//FUNC CALL EXPRESSION


//LEFT EXPRESSION ObjIdRef

//LEFT EXPRESSION IdRef
obj_Square tmp10;
tmp10 = this;
;

obj_Pt tmp11;
tmp11 = tmp10->ll;
;


obj_Int tmp12;
tmp12 = tmp11->clazz->_y(tmp11);
;


//LEFT EXPRESSION IdRef
obj_Int tmp13;
tmp13 = side;
;

obj_Int tmp14;
tmp14 = tmp12->clazz->PLUS(tmp12,tmp13);
obj_Pt tmp15;
tmp15 = the_class_Pt->constructor(tmp9, tmp14);
;

this->ur = tmp15;


  return new;
}

obj_String Square_method_PRINT(obj_Square this) {
fprintf(stdout, "%s",(this->clazz->STRING(this))->text);
return this;
}

struct class_Square_struct the_class_Square_struct = {

new_Square,
Obj_method_EQUALS,
Square_method_PRINT,
Rect_method_STRING,
Rect_method_translate
};

class_Square the_class_Square = &the_class_Square_struct;




void quackmain() {
obj_Square a_square;
obj_Rect b_square;

//StatementExp UntypedAssignExp


//CONSTRUCTOR CALL EXP


//CONSTRUCTOR CALL EXP

obj_Pt tmp0;
tmp0 = the_class_Pt->constructor(int_literal(1), int_literal(1));
;



//CONSTRUCTOR CALL EXP

obj_Pt tmp1;
tmp1 = the_class_Pt->constructor(int_literal(1), int_literal(1));
;
obj_Rect tmp2;
tmp2 = the_class_Rect->constructor(tmp0, tmp1);
;

obj_Rect tmp5;
tmp5 = (b_square = tmp2);

//StatementExp UntypedAssignExp


//CONSTRUCTOR CALL EXP


//CONSTRUCTOR CALL EXP

obj_Pt tmp6;
tmp6 = the_class_Pt->constructor(int_literal(3), int_literal(3));
;

obj_Square tmp7;
tmp7 = the_class_Square->constructor(tmp6, int_literal(5));
;

obj_Square tmp10;
tmp10 = (a_square = tmp7);


//FUNC CALL EXPRESSION

//LEFT EXPRESSION IdRef
obj_Square tmp11;
tmp11 = a_square;
;


obj_String tmp12;
tmp12 = tmp11->clazz->PRINT(tmp11);
;
//StatementExp UntypedAssignExp


//FUNC CALL EXPRESSION

//LEFT EXPRESSION IdRef
obj_Square tmp14;
tmp14 = a_square;
;



//CONSTRUCTOR CALL EXP

obj_Pt tmp13;
tmp13 = the_class_Pt->constructor(int_literal(2), int_literal(2));
;

obj_Rect tmp15;
tmp15 = tmp14->clazz->translate(tmp14,tmp13);
;

obj_Rect tmp18;
tmp18 = (b_square = tmp15);


//FUNC CALL EXPRESSION


obj_String tmp19;
tmp19 = str_literal(" -> ")->clazz->PRINT(str_literal(" -> "));
;

//FUNC CALL EXPRESSION

//LEFT EXPRESSION IdRef
obj_Rect tmp20;
tmp20 = b_square;
;


obj_String tmp21;
tmp21 = tmp20->clazz->PRINT(tmp20);
;

}

