module QuackAST (module QuackAST) where

import Data.List

import qualified Data.Map.Strict as Map
import Text.Printf (printf)

type QStr  = String 
type QId = String
type QInt  = Int   
type ActualArgs =  [RightExp]
type Statements = [Statement]
type CondPair = (RightExp,StatementBlock)
type Elifs = [ElifExp]
type Methods = [Method]
type FormalArgs = [FormalArg]
type Classes = [ClassExp]

data ASTNode = ASTProgram Program 
             | ASTClassExp ClassExp 
             | ASTClassSig ClassSig 
             | ASTClassBody ClassBody 
             | ASTFormalArg FormalArg 
             | ASTMethod Method 
             | ASTStatement Statement 
             | ASTStatementBlock StatementBlock 
             | ASTWhileExp WhileExp 
             | ASTIfExp IfExp 
             | ASTElifExp ElifExp 
             | ASTElifs Elifs 
             | ASTElseExp ElseExp
             | ASTReturnExp ReturnExp 
             | ASTAssignmentExp AssignmentExp 
             | ASTBareExp BareExp 
             | ASTLeftExp LeftExp 
             | ASTRightExp RightExp 
             | ASTActualArgs ActualArgs 
             | ASTFormalArgs FormalArgs 
             | ASTStatements Statements 
             | ASTMethods Methods 
             | ASTClasses Classes 
             | ASTCondPair CondPair 
               deriving Show
                              


{- Might want to make these record types -}                      
data Program = ProgramExp Classes Statements Int
             | ForPrinting -- deriving Show
      
data ClassExp = ClassDecExp ClassSig ClassBody Int deriving Show

data ClassSig = ClassSigExp QId FormalArgs Int
              | ExtendedClassSigExp QId FormalArgs QId Int
              deriving Show

data ClassBody = ClassBodyExp Statements Methods Int deriving Show

data FormalArg = FormalArgExp QId QId Int deriving Show
    
data Method = MethodExp QId FormalArgs StatementBlock Int
            | TypedMethodExp QId FormalArgs QId StatementBlock  Int
            deriving Show

data Statement  
             = IfExp IfExp Int
             | WhileExp WhileExp Int
             | ReturnExp ReturnExp Int
             | AssignmentExp AssignmentExp Int
             | BareExp BareExp Int 
             deriving Show

data StatementBlock = StatementBlockExp Statements Int deriving Show

data WhileExp = WhileLoop CondPair Int deriving Show

data IfExp = IfBlockExp RightExp StatementBlock Elifs ElseExp Int deriving Show

data ElifExp = ElifBlockExp RightExp StatementBlock Int deriving Show

data ElseExp = ElseBlockExp StatementBlock Int deriving Show


data ReturnExp = NoReturnExp Int | RetExp RightExp Int deriving Show

data AssignmentExp = UntypedAssignExp LeftExp RightExp Int
                   | TypedAssignExp LeftExp QStr RightExp Int
                   deriving Show

data BareExp = RightExp RightExp Int deriving Show

--Left Expressions
data LeftExp = IdRef QId Int
          | ObjIdRef RightExp QId Int
          deriving Show

data RightExp
        = StrExp QStr  Int
        | IntExp QInt Int
        | BoolExp Bool Int
        | BinaryExp String RightExp RightExp Int
        | NotExp RightExp Int
        | ParExp RightExp Int
        | FuncCallExp RightExp QId ActualArgs Int
        | ConstructorExp QId ActualArgs  Int
        | LeftExp LeftExp Int
        | NullExp String Int  deriving Show

{-
instance Show RightExp where
  show (StrExp _ _) = show ""
  show (IntExp _ _) = show ""
  show (BinaryExp _ _ _ _) = show ""
  show (NotExp _ _) = show ""
  show (ParExp _ _) = show ""
  show (FuncCallExp _ _ _ _) = show ""
  show (ConstructorExp _ _ _) = show ""
  show (LeftExp _ _) = show ""
  show (NullExp _ _) = show ""-}



instance Show Program where
  show (ProgramExp _ _ _) = show ""
  show (ForPrinting) = show ""



-- AST 'getter' functions
getRightExp :: ASTNode -> RightExp
--getRightExp e = let ASTRightExp e' _ = e in e'
getRightExp e = case e of
                  ASTRightExp e' ->  e'
                  ASTLeftExp l' -> case l' of 
                                       IdRef i l -> NullExp "String idRef" l
                                       ObjIdRef r i l -> r

getLeftExp :: ASTNode -> LeftExp
getLeftExp e = let ASTLeftExp e' = e in e'

-- AST STUFF
data SymTable = SymTable { classes :: Map.Map String (String,Int) } deriving Show

extractClasses :: ASTNode -> [(QId,(QId,Int))]
extractClasses ast = case ast of
                    ASTProgram prog -> let ProgramExp cs ss l = prog in extractClasses (ASTClasses cs)
                    ASTClasses cs -> concat $ map (extractClasses . ASTClassExp) (reverse cs)
                    ASTClassExp cls -> let ClassDecExp sig body l = cls in extractClasses (ASTClassSig sig)
                    ASTClassSig sig -> case sig of
                                        ClassSigExp id args l -> [(id,("Obj",l))]
                                        ExtendedClassSigExp id args ext l ->   [(id,(ext,l)) ]
                    _ -> []
          
classSymbols ast = SymTable { classes = Map.fromList $  (defaultClasses ++ (extractClasses ast)) }     
defaultClasses = [("Obj", ("Obj", 0)),("Boolean", ("Obj",0)),("Int",("Obj",0)),("String",("Obj",0)),("Nothing",("Obj",0))]

extractConstructors :: ASTNode -> [(String,Int)]
extractConstructors ast = case ast of
                    ASTProgram prog -> let ProgramExp cs ss _ = prog in concat [(extractConstructors $ ASTClasses cs),(extractConstructors $ ASTStatements ss)]
                    ASTClasses cs -> concat $ map (extractConstructors . ASTClassExp) (reverse cs)
                    ASTClassExp cls -> let ClassDecExp _ body _ = cls in extractConstructors (ASTClassBody body)
                    ASTClassBody body -> let ClassBodyExp ss ms _ = body in (extractConstructors $ ASTStatements ss) ++  (extractConstructors $ ASTMethods ms)
                    ASTMethods ms -> concat $ map (extractConstructors . ASTMethod) (reverse ms)
                    ASTMethod method -> case method of
                                              MethodExp _ _ body _ -> extractConstructors $ ASTStatementBlock body
                                              TypedMethodExp _ _ _ body _ -> extractConstructors $ ASTStatementBlock body
                    ASTStatementBlock sb -> let StatementBlockExp ss _ = sb in extractConstructors $ ASTStatements ss
                    ASTStatements ss -> concat $ map (extractConstructors . ASTStatement) (reverse ss)
                    ASTStatement s -> case s of
                                        IfExp i _ -> extractConstructors $ ASTIfExp i 
                                        WhileExp w _ -> extractConstructors $ ASTWhileExp w
                                        ReturnExp r _ -> extractConstructors $ ASTReturnExp r
                                        AssignmentExp a _ -> extractConstructors $ ASTAssignmentExp a
                                        BareExp b _ -> extractConstructors $ ASTBareExp b
                    ASTIfExp i -> let IfBlockExp cond body elifs els _ = i in
                                    concat [(extractConstructors $ ASTRightExp cond),(extractConstructors $ ASTStatementBlock body),
                                            (extractConstructors $ ASTElifs elifs),(extractConstructors $ ASTElseExp els)]
                    ASTElifs elifs -> concat $ map (extractConstructors . ASTElifExp) (reverse elifs)
                    ASTElifExp elif -> let ElifBlockExp cond body _ = elif in
                                        concat [(extractConstructors $ ASTRightExp cond),(extractConstructors $ ASTStatementBlock body)]
                    ASTElseExp els -> let ElseBlockExp body _ = els in extractConstructors $ ASTStatementBlock body
                    ASTWhileExp while -> let WhileLoop (cond,body) _ = while in 
                                        concat [(extractConstructors $ ASTRightExp cond),(extractConstructors $ ASTStatementBlock body)]
                    ASTReturnExp ret -> case ret of
                                           NoReturnExp _ -> []
                                           RetExp r _ -> extractConstructors $ ASTRightExp r
                    ASTAssignmentExp a -> case a of
                                            UntypedAssignExp l r _ -> concat [(extractConstructors $ ASTLeftExp l),(extractConstructors $ ASTRightExp r)]
                                            TypedAssignExp l _ r _ -> concat [(extractConstructors $ ASTLeftExp l),(extractConstructors $ ASTRightExp r)] 
                    ASTBareExp b -> case b of RightExp r _ ->  extractConstructors $ ASTRightExp r
                    ASTLeftExp l -> case l of
                                      IdRef _ _ -> []
                                      ObjIdRef r _ _ -> extractConstructors $ ASTRightExp r
                    ASTRightExp r -> case r of
                                       StrExp _ _ -> []
                                       IntExp _ _ -> []
                                       BoolExp _ _ -> []
                                       BinaryExp _ r1 r2 _ -> concat [(extractConstructors $ ASTRightExp r1),(extractConstructors $ ASTRightExp r2)]
                                       NotExp r _ -> extractConstructors $ ASTRightExp r
                                       ParExp r _ -> extractConstructors $ ASTRightExp r
                                       FuncCallExp r _ args _ -> concat [(extractConstructors $ ASTRightExp r),(extractConstructors $ ASTActualArgs args)]
                                       ConstructorExp id args line -> [(id,line)] ++ (extractConstructors $ ASTActualArgs args)
                                       LeftExp l _ -> extractConstructors $ ASTLeftExp l
                                       NullExp _ _ -> []
                    ASTActualArgs args -> concat $ map (extractConstructors . ASTRightExp) (reverse args)
                    _ -> []

-- Class Checks
badConstructors ast = let cs =  classes $ classSymbols ast in filter (\(c,l) -> c `Map.notMember` cs) (extractConstructors ast)

data ClassInherErr = Cycle | DeadEnd QId | NoErr deriving Show

--classCycles :: QId -> Map.Map String (String,Int) -> [QId] ->  (Bool,[QId])
classCycles c cs classes path =  case Map.lookup c cs of
                              Just ("Obj",line) -> (NoErr,path)
                              Just (ext,line) -> classCycles ext (Map.delete c cs) classes (path ++ [c])
                              Nothing -> case Map.lookup c classes of
                                          Just (e,l) -> (Cycle,path)
                                          Nothing -> (DeadEnd c,path)

classInheritances :: ASTNode -> [(String, (Int,(ClassInherErr,[QId]) ) )]
classInheritances ast = let cs = classes $ classSymbols ast in
                         Map.toList $ Map.mapWithKey (\k (c,l) -> (l, classCycles c (classes $ classSymbols ast) cs []) ) cs




-- Type Checking
data QMethod = QMethod {mname::QId,syms::(Map.Map QId QField),mline::Int,mtype::TypeCheck,mclass::QId,mbody::StatementBlock,margs::FormalArgs} deriving Show
data QField = QField {fname::QId,fline::Int,ftype::TypeCheck,fexp::Maybe RightExp} deriving Show
data QClass = QClass {cname::QId,csuper::QId,cmethods::(Map.Map QId QMethod),cfields::(Map.Map QId QField),cconstructor::QMethod,cline::Int} deriving Show
data SymbolTable = SymbolTable {quackClasses ::(Map.Map QId QClass), quackFields :: (Map.Map QId QField), localScope :: (Map.Map QId QField)} deriving Show


emptyBlock = StatementBlockExp [] 0

qMethodPrint = QMethod {mname="PRINT",syms=Map.empty,mline=0,mtype=Type "String",mclass="Obj",mbody=emptyBlock,
                                  margs= []}

defaultMethods = Map.fromList [("PRINT",qMethodPrint),
                 ("EQUALS", QMethod {mname="EQUALS",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Obj",mbody=emptyBlock,
                                      margs=[FormalArgExp "other" "Obj" 0]}),
                  ("STRING",    QMethod {mname="STR",syms=Map.empty,mline=0,mtype=Type "String",mclass="Obj",mbody=emptyBlock,
                                      margs=[]})]


intDefaultMethods = 
  let def = Map.fromList [ ("PLUS",QMethod {mname="PLUS",syms=Map.empty,mline=0,mtype=Type "Int",mclass="Obj",mbody=emptyBlock,
                                    margs=[FormalArgExp "other" "Int" 0]}),
                           ("MINUS",QMethod {mname="MINUS",syms=Map.empty,mline=0,mtype=Type "Int",mclass="Obj",mbody=emptyBlock,
                                    margs= [FormalArgExp "other" "Int" 0]}),
                           ("TIMES",QMethod {mname="TIMES",syms=Map.empty,mline=0,mtype=Type "Int",mclass="Obj",mbody=emptyBlock,
                                    margs=[FormalArgExp "other" "Int" 0]}),
                           ("DIVIDE",QMethod {mname="DIVIDE",syms=Map.empty,mline=0,mtype=Type "Int",mclass="Obj",mbody=emptyBlock,
                                    margs=[FormalArgExp "other" "Int" 0]}),
                           ("LESS",QMethod {mname="LESS",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Obj",mbody=emptyBlock,
                                    margs= [FormalArgExp "other" "Int" 0]}),
                           ("MORE",QMethod {mname="MORE",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Obj",mbody=emptyBlock,
                                    margs= [FormalArgExp "other" "Int" 0]}),
                           ("LEQ",QMethod {mname="LEQ",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Obj",mbody=emptyBlock,
                                    margs=[FormalArgExp "other" "Int" 0]}),
                           ("GEQ",QMethod {mname="GEQ",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Obj",mbody=emptyBlock,
                                    margs=[FormalArgExp "other" "Int" 0]}),
                           ("EQUALS",QMethod {mname="EQUALS",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Obj",mbody=emptyBlock,
                                    margs= [ FormalArgExp "other" "Obj" 0]})
                         ]
  in Map.union def defaultMethods

stringDefaultMethods =
  let def = Map.fromList [ ("PLUS",QMethod {mname="PLUS",syms=Map.empty,mline=0,mtype=Type "String",mclass="String",mbody=emptyBlock,
                                    margs=[FormalArgExp "other" "String" 0]}) ]
  in Map.union def defaultMethods
{-
boolDefaultMethods = 
  let def = Map.fromList [ ("PLUS",QMethod {mname="PLUS",syms=Map.empty,mline=0,mtype=Type "Int",mclass="Obj",mbody=emptyBlock,
                                    margs=[FormalArgExp "other" "Int" 0]}),
                           ("MINUS",QMethod {mname="MINUS",syms=Map.empty,mline=0,mtype=Type "Int",mclass="Obj",mbody=emptyBlock,
                                    margs= [FormalArgExp "other" "Int" 0]}),
                           ("TIMES",QMethod {mname="TIMES",syms=Map.empty,mline=0,mtype=Type "Int",mclass="Obj",mbody=emptyBlock,
                                    margs=[FormalArgExp "other" "Int" 0]}),
                           ("DIVIDE",QMethod {mname="DIVIDE",syms=Map.empty,mline=0,mtype=Type "Int",mclass="Obj",mbody=emptyBlock,
                                    margs=[FormalArgExp "other" "Int" 0]}),
                           ("LESS",QMethod {mname="LESS",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Obj",mbody=emptyBlock,
                                    margs= [FormalArgExp "other" "Int" 0]}),
                           ("MORE",QMethod {mname="MORE",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Obj",mbody=emptyBlock,
                                    margs= [FormalArgExp "other" "Int" 0]}),
                           ("LEQ",QMethod {mname="LEQ",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Obj",mbody=emptyBlock,
                                    margs=[FormalArgExp "other" "Int" 0]}),
                           ("GEQ",QMethod {mname="GEQ",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Obj",mbody=emptyBlock,
                                    margs=[FormalArgExp "other" "Int" 0]}),
                           ("EQUALS",QMethod {mname="EQUALS",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Obj",mbody=emptyBlock,
                                    margs= [ FormalArgExp "other" "Obj" 0]})
                         ]
-}

                          
builtInObj = QClass {cname="Obj",cfields=Map.empty, csuper="",cmethods=defaultMethods,cline=0,
                              cconstructor=QMethod{mname="Obj",syms=Map.empty,mline=0,mtype=Type "Obj",mclass="Obj",mbody=emptyBlock,margs=[]}}
builtInInt = QClass{cname="Int",cfields=Map.empty,cmethods=intDefaultMethods,csuper="Obj",cline=0,
                          cconstructor=QMethod{mname="Int",syms=Map.empty,mline=0,mtype=Type "Int",mclass="Int",mbody=emptyBlock,margs=[]}}
builtInString = QClass{cname="String",cfields=Map.empty,cmethods=stringDefaultMethods,csuper="Obj",cline=0,
                          cconstructor=QMethod{mname="String",syms=Map.empty,mline=0,mtype=Type "String",mclass="String",mbody=emptyBlock,margs=[]}}
builtInBoolean = QClass{cname="Boolean",cfields=Map.empty,cmethods=defaultMethods,csuper="Obj",cline=0,
                          cconstructor=QMethod{mname="Boolean",syms=Map.empty,mline=0,mtype=Type "Boolean",mclass="Boolean",mbody=emptyBlock,margs=[]}}
builtInNothing = QClass{cname="Nothing",cfields=Map.empty,cmethods=defaultMethods,csuper="Obj",cline=0,
                          cconstructor=QMethod{mname="Nothing",syms=Map.empty,mline=0,mtype=Type "Nothing",mclass="Nothing",mbody=emptyBlock,margs=[]}}

builtIns = Map.fromList [("Obj", builtInObj),
           ("Int", builtInInt),
           ("String",builtInString),
           ("Boolean",builtInBoolean),
           ("Nothing",builtInNothing)]

buildSymbolTable ast = let cs = map (\c->(cname c,c)) $ extractClasses2 ast
                           fs = map (\f->(fname f,f)) $ getFieldDeclarations ast
                       in
                           SymbolTable { quackClasses=Map.union builtIns (Map.fromList cs), quackFields=Map.fromList fs, localScope=Map.empty }


getInheritedST st = 
 st{quackClasses= Map.fromList (addInheritedMethodsToClasses st (Map.toList $ quackClasses st))}

addInheritedMethodsToClasses st classes = 
   case classes of 
    [] -> []
    ((name,c):cs) -> let c' = addInheritedMethods st c  in
                  ((name,c'):(addInheritedMethodsToClasses st cs))

addInheritedMethods st c = 
  case getClass st (csuper c) of
    Nothing -> c
    Just super ->
      case (cname c, cname super) of
          ("Obj","Obj") -> c
          _ -> let super' = addInheritedMethods st super 
                   newMs = Map.union (cmethods c) (cmethods super')
               in c{cmethods=newMs}


getName :: SymbolTable -> LeftExp -> (QId,String)
getName symbols (IdRef name line) = (name,"")
getName symbols (ObjIdRef r name line) = 
  case (r,getType symbols r ) of
    (LeftExp l _,Type t) ->
      let (objId,objCls) = getName symbols l
      in (name,objId)
    (els,Type t) -> (name,t)
    _ -> (name, "BAD OBJ REF TYPE!!!")
 

_getFieldDeclarations :: Statements -> [Maybe QField]
_getFieldDeclarations ss = concat $ map gqf  ss where
                              gqf s = case s of
                                  AssignmentExp a l -> case a of
                                                        UntypedAssignExp (IdRef  name _) r line -> [Just QField {fname=name,fline=line,ftype=Untyped,fexp=Just r}]
                                                        TypedAssignExp (IdRef name _) typ r line -> [Just QField {fname=name,fline=line,ftype=Type typ,fexp=Just r}]
                                                        _ -> []

                                  WhileExp w l -> let WhileLoop (cond,StatementBlockExp ss _) line = w in
                                                      _getFieldDeclarations ss

                                  IfExp i l -> let IfBlockExp cond (StatementBlockExp ss _) elifs els line = i 
                                                   gqfElif e = let ElifBlockExp cond (StatementBlockExp elifst _) _ = e in
                                                                _getFieldDeclarations elifst
                                                   elifFields = concat $ map gqfElif elifs
                                                   ElseBlockExp (StatementBlockExp elss _) _ = els 
                                               in
                                                   foldl (++) [] [_getFieldDeclarations ss, elifFields, _getFieldDeclarations elss]
                                  _ -> [Nothing] 

getFieldDeclarations :: ASTNode ->  [QField]
getFieldDeclarations ast = let filterNothings c = case c of
                                                      Just c' -> True
                                                      Nothing -> False
                               mapQFields c = let Just c' = c in c'
                           in case ast of
                              ASTProgram prog -> let ProgramExp cs ss l = prog in 
                                                    map mapQFields . filter filterNothings . _getFieldDeclarations $ ss
                              ASTStatementBlock sb -> let StatementBlockExp bodyStmts l = sb in
                                                        map mapQFields . filter filterNothings .  _getFieldDeclarations $ bodyStmts
                              _ -> []

qFieldsToMap :: [QField] -> Map.Map QId QField
qFieldsToMap qs = Map.fromList . map (\q -> (fname q, q)) $ qs

qMethodsToMap ms = Map.fromList . map (\m -> (mname m, m)) $ ms

getArgs :: FormalArgs -> [QField]
getArgs args = case args of
                      [] -> []
                      (a:as) -> let FormalArgExp name typ l = a in
                                 (QField {fname=name,fline=l,ftype=Type typ, fexp=Nothing}):(getArgs as)



{-
getRetStmt :: StatementBlock -> Statements
getRetStmt sb = filter getReturns sb where
                          getReturns s = case s of
                                           ReturnExp r l -> True
                                           _ -> False
-}

getRetExp s = case s of
                ReturnExp r l -> case r of
                                    NoReturnExp _ -> []
                                    RetExp rexp _ -> [rexp]

                WhileExp w l -> let WhileLoop (cond,StatementBlockExp ss _) _ = w in
                                      concat $ map getRetExp ss

                IfExp i l -> let IfBlockExp cond (StatementBlockExp ss _) elifs els line = i 
                                 ifrets = map getRetExp ss                
                                 getElifRets e = let ElifBlockExp cond (StatementBlockExp elifst _) _ = e in
                                              map getRetExp elifst
                                 elifRets = concat $ map getElifRets elifs
                                 ElseBlockExp (StatementBlockExp elss _) _ = els 
                                 elseRets = map getRetExp elss
                             in
                                 concat $ ifrets ++ elifRets ++ elseRets
                _ -> []

getReturns :: StatementBlock -> [RightExp]
getReturns (StatementBlockExp ss _) = concat $ map getRetExp ss

getQMethod :: QId -> Method  -> QMethod
getQMethod cls (MethodExp name args body line) = let argSyms = getArgs args
                                                     bodySyms = getFieldDeclarations . ASTStatementBlock $ body
                                             in 
                                                 QMethod {mname=name,syms=qFieldsToMap $ argSyms++bodySyms,mline=line,mtype=Untyped,mclass=cls,mbody=body,margs=args}

getQMethod cls (TypedMethodExp name args typ body line) = let argSyms = getArgs args
                                                              bodySyms = getFieldDeclarations . ASTStatementBlock $ body
                                                          in
                                                              QMethod {mname=name,syms=qFieldsToMap $ argSyms++bodySyms,mline=line,mtype=Type typ,mclass=cls,mbody=body,margs=args}  



_getClassFieldDeclarations :: Statements -> [QField]
_getClassFieldDeclarations ss = concat $ map gqf  ss where
    gqf s = case s of
                AssignmentExp _ _ ->  case getThisReference s of
                                        Just (name,typ,line,r) -> [QField {fname=name,fline=line,ftype=typ,fexp=Just r}]
                                        Nothing -> []
                WhileExp w l -> let WhileLoop (cond,StatementBlockExp ss _) line = w in
                                    _getClassFieldDeclarations ss
                IfExp i l -> let IfBlockExp cond (StatementBlockExp ss _) elifs els line = i 
                                 gqfElif e = let ElifBlockExp cond (StatementBlockExp elifst _) _ = e in
                                              _getClassFieldDeclarations elifst
                                 elifFields = concat $ map gqfElif elifs
                                 ElseBlockExp (StatementBlockExp elss _) _ = els 
                             in
                                 foldl (++) [] [_getClassFieldDeclarations ss, elifFields, _getClassFieldDeclarations elss]
                _ -> [] 


getThisReference (AssignmentExp a l) = case a of
                                      UntypedAssignExp l r _ -> case (l,r) of
                                                                   (ObjIdRef (LeftExp (IdRef "this" _)_) name line,_) -> Just  (name,Untyped,line,r)
                                                                   _ -> Nothing
                                      TypedAssignExp l typ r _ -> case (l,r) of
                                                                   (ObjIdRef (LeftExp (IdRef "this" _)_) name line,_) -> Just  (name,Type typ,line,r)
                                                                   _ -> Nothing


getClassFieldDeclarations ast = let filterNothings c = case c of
                                                      Just c' -> True
                                                      Nothing -> False
                                    mapQFields c = let Just c' = c in c'
                                in case ast of
                                  ASTStatementBlock sb -> let StatementBlockExp bodyStmts l = sb in
                                                            _getClassFieldDeclarations bodyStmts
                                  _ -> []



getClassSig :: ClassSig -> (QId,QId,FormalArgs,Int)
getClassSig s = case s of
                  ClassSigExp id args l -> (id,"Obj",args,l)
                  ExtendedClassSigExp id args ext l -> (id,ext,args,l)                 



extractClasses2 :: ASTNode -> [QClass]
extractClasses2 ast = case ast of
                    ASTProgram prog -> let ProgramExp cs ss l = prog in extractClasses2 (ASTClasses cs)
                    ASTClasses cs -> concat $ map (extractClasses2 . ASTClassExp) (reverse cs)
                    ASTClassExp cls -> let ClassDecExp sig classbody l = cls 
                                           ClassBodyExp body methods lb = classbody
                                           (name,super,args,line) = getClassSig sig
                                           argSyms = getArgs args
                                           consLocalFields = getFieldDeclarations . ASTStatementBlock $ StatementBlockExp body lb
                                           classFields = getClassFieldDeclarations . ASTStatementBlock $ StatementBlockExp body lb
                                           constructorMethod = QMethod {mname=name,syms=qFieldsToMap $ argSyms++consLocalFields,
                                                                        mline=line,mtype=Type name,mclass=name,mbody=StatementBlockExp body lb,margs=args}
                                           ms = qMethodsToMap $ (map (getQMethod name)  methods)++[(qMethodPrint)]
                                           
                                       in
                                           [QClass {cname=name,csuper=super,cfields=qFieldsToMap classFields,
                                                    cline=l,cconstructor=constructorMethod,cmethods={-Map.insert ("$"++name) constructorMethod-} ms}]                       
  

opToFunc op = 
  case op of
    "+" -> "PLUS"
    "-" -> "MINUS"
    "/" -> "DIVIDE"
    "*" -> "TIMES"
    ">" -> "MORE"
    "<" -> "LESS"
    ">=" -> "GEQ"
    "<=" -> "LEQ"
    "and" -> "AND"
    "or" -> "OR"
    "==" -> "EQUALS"
    _ -> "$$$unknown method"

data TypeCheck = Untyped | Type QId | TypeError String | Dependency InferType {-| This String-} | MethodReturn | NoShow String deriving (Eq,Show)



getType :: SymbolTable -> RightExp -> TypeCheck
getType st (StrExp s l) = Type "String"
getType st (IntExp s l) = Type "Int"
getType st (BoolExp b l) = Type "Boolean"
getType st (BinaryExp op a b l) = case (getType st a, getType st b) of
                                        (TypeError e,TypeError e2) -> TypeError e
                                        (TypeError e, _) -> TypeError e
                                        (_,TypeError e) -> TypeError e
                                        (Type a',Type b') -> 
                                             if a' == b' then
                                              case getMethod st a' (opToFunc op) of
                                                Just m -> (mtype m)
                                                Nothing -> if (op `elem` ["or","and"]) && a' == "Boolean" then Type "Boolean" else 
                                                            TypeError $ printf "Type error at line %d: Call to undefined method %s of class %s" l (opToFunc op) a'
                                             else TypeError $ printf "Type error at line %d: Expected type %s but got %s" l a' b'
                                        (a',b') -> TypeError $ (printf "Type error in binary expression at line %d" l) ++ " " ++ ( show b')
getType st (NotExp n l) = Type "Boolean"
getType st (ParExp p l) = getType st p
getType st (FuncCallExp r method args l) = case getType st r of
                                         TypeError e -> TypeError e
                                         Type t -> let cls = Map.lookup t (quackClasses st) in case cls of
                                                        Nothing -> NoShow $ "Call to undefined class " ++ t ++ " at line " ++ (show l)
                                                        Just cls' -> case Map.lookup method (cmethods cls') of
                                                              Nothing -> TypeError $ printf "Type error at line %d: Call to undefined method %s of class %s" l method t 
                                                              Just m -> case (mtype m) of
                                                                          TypeError e -> TypeError e
                                                                          Untyped -> Dependency $ Itype{itclass=t,itmethod=(mname m),itfield="",
                                                                                                        itnode=IMethod,itdep=Nothing}
                                                                          MethodReturn -> Dependency $ Itype{itclass=t,itmethod=(mname m),itfield="",
                                                                                                              itnode=IMethod,itdep=Nothing} 
                                                                          tt -> tt  
                                         --This 
getType st (ConstructorExp cls _ line) = case Map.lookup cls (quackClasses st) of
                                      Nothing -> TypeError $ printf "Type error at line %d: Call to constructor of undefined class \"%s\"" line cls
                                      _ -> Type cls
getType st (LeftExp lexp l) = case lexp of
                             IdRef name l ->  case name of 
                                              --  "this" -> This name ""
                                                _ -> case Map.lookup name (localScope st) of
                                                          Nothing -> TypeError $ printf "Type error at line %d: Undeclared identifier %s" l name
                                                          Just f -> case (ftype f) of
                                                                    Untyped -> Dependency $ Itype{itclass="",itmethod="",itfield=(fname f),
                                                                                                        itnode=IField,itdep=Nothing} 
                                                                    Type t -> Type t
                                                                    _ -> TypeError ("Weird: " ++ (show (ftype f)))
                             ObjIdRef r name l -> let cls = getType st r in 
                                                    case cls of
                                                    --  This n _ -> This n name
                                                      TypeError e -> TypeError e
                                                      Untyped -> NoShow $ "Accessing field of object with unknown type at line " ++ (show l)
                                                      Type cls' -> case Map.lookup cls' (quackClasses st) of
                                                                     Nothing -> NoShow $ "access to field of undeclared class at line " ++ (show l)
                                                                     Just c -> case Map.lookup name (cfields c) of
                                                                                Nothing -> NoShow $ "Reference to undeclared field at line " ++ (show l)
                                                                                Just f -> case ftype f of
                                                                                            Untyped -> Dependency $ Itype{itclass=cls',itmethod="constructor",itfield=(fname f),
                                                                                                        itnode=IConstructorField,itdep=Nothing} 
                                                                                            _ -> (ftype f)




getClass :: SymbolTable -> QId -> Maybe QClass
getClass symbols cname = Map.lookup cname (quackClasses symbols)
                                     
updateClass :: SymbolTable -> QId -> QClass -> SymbolTable
updateClass symbols cname c = let newClasses = Map.insert cname c (quackClasses symbols) in symbols{quackClasses=newClasses}

getClassField symbols cname fname = 
  case Map.lookup cname (quackClasses symbols) of
    Just c -> Map.lookup fname (cfields c)
    Nothing -> Nothing

updateClassField symbols cname fname f = 
  case Map.lookup cname (quackClasses symbols) of
    Just c -> let newFields = Map.insert fname f (cfields c)
                  c'= c{cfields=newFields}
              in updateClass symbols cname c'
    Nothing -> symbols
  
updateConstructor :: SymbolTable -> QId -> QMethod -> SymbolTable
updateConstructor symbols cname cons = case getClass symbols cname of
                                         Just c -> updateClass symbols cname c{cconstructor=cons}
                                         Nothing -> symbols
                                         

updateMethod :: SymbolTable -> QId -> QId -> QMethod -> SymbolTable
updateMethod symbols cname mname m = case Map.lookup cname (quackClasses symbols) of
                                      Nothing -> symbols
                                      Just cls -> let newMethods = Map.insert mname m (cmethods cls)
                                                      newClass = cls{cmethods=newMethods}
                                                  in updateClass symbols cname newClass

getMethod :: SymbolTable -> QId -> QId -> Maybe QMethod
getMethod symbols cname mname = case getClass symbols cname of
                                    Nothing -> Nothing
                                    Just cls -> Map.lookup mname (cmethods cls)


updateMethodField :: SymbolTable -> QId -> QId -> QId -> QField -> SymbolTable
updateMethodField symbols cname mname fname f = case Map.lookup cname (quackClasses symbols) of
                                                 Nothing -> symbols
                                                 Just cls -> case Map.lookup mname (cmethods cls) of
                                                              Nothing -> symbols
                                                              Just m -> let newFields = Map.insert fname f (syms m)
                                                                            newMethod = m{syms=newFields}
                                                                        in updateMethod symbols cname mname newMethod
getMethodField :: SymbolTable -> QId -> QId -> QId -> Maybe QField
getMethodField symbols cname mname fname = case getClass symbols cname of
                                            Nothing -> Nothing
                                            Just cls -> case getMethod symbols cname mname of
                                                          Nothing -> Nothing
                                                          Just m -> Map.lookup fname (syms m)
                                                                                                   
buildMethodScope symbols m = symbols{quackFields=(syms m)}


filterTypeErrors [] = []
filterTypeErrors (t:ts) = case t of 
                            TypeError _ -> t:(filterTypeErrors ts)
                            _ -> filterTypeErrors ts

typeField :: QField -> SymbolTable -> TypeCheck
typeField f symbols = case (ftype f) of
                        Untyped -> case (fexp f) of 
                                      Just r -> getType symbols r
                                          
                                      Nothing -> TypeError $ "Could not type at line " ++ (show $ fline f)
                        _ -> (ftype f)


--typeFields :: (Map.Map QId QField) -> SymbolTable -> (Map.Map QId QField)
typeFields symbols fs  = Map.map (\f -> f {ftype=typeField f symbols}) fs 

--typeFields :: [(QId,QField)] -> SymbolTable -> [(QId,QField)]
{-typeFields symbols cname mname [] newFields = case getMethod symbols cname mname of
                                                  Just m -> (updateMethod symbols cname mname (m{syms=newFields}))
                                                  Nothing -> symbols
typeFields symbols cname mname ((name,field):ts) newFields = case (ftype field) of
                                        Untyped -> let 
                                                       (_,f) = (name,field{ftype=typeField f symbols})
                                                       x = show $ Map.insert name f newFields
                                                   in  typeFields symbols{localScope=(Map.insert name f newFields)} cname mname ts newFields--(Map.insert name f newFields)
                                        t -> typeFields symbols{localScope=(Map.insert name field newFields)} cname mname ts newFields--(Map.insert name field newFields)
-}



typeConstructorFields symbols cname [] = symbols
typeConstructorFields symbols cname ((name,field):ts) = case getClass symbols cname of
                                                          Just c -> let (_,f) = (name,field{ftype=typeField f symbols})    
                                                                        constructor = (cconstructor c)
                                                                        newSymbols = updateConstructor symbols cname constructor
                                                                    in typeConstructorFields newSymbols cname ts
                                                          Nothing -> symbols


addThis :: QId -> Map.Map QId QField -> Map.Map QId QField
addThis cname fields = Map.insert "this" QField{fname=cname,ftype=Type cname,fline=0,fexp=Nothing} fields


--typeMethod :: QMethod -> SymbolTable -> QMethod
typeMethod cname methodName m symbols = 
  let thisScope = symbols{localScope=Map.union (addThis cname Map.empty) (syms m)}
      typedFields = typeFields thisScope {-cname methodName-} (syms m) -- (Map.empty)
      --newFields = localScope typedFields
--      returnExps = getReturns $ mbody m
   --   retTypes =  map (getType thisScope)  returnExps   
      methodType = mtype m                     
  in case mtype m of
    Untyped -> updateMethod symbols cname methodName m {syms=typedFields,mtype=MethodReturn}
    Type t -> updateMethod symbols cname methodName m {syms=typedFields}
 {-case retTypes of
      [] -> updateMethod symbols cname methodName m {syms=typedFields,
                                                            mtype=TypeError $ "Method " ++ ((mclass m) ++ "." ++ (mname m)) ++ " ends without returning"}
      _ -> case filterTypeErrors retTypes of  
           errs -> updateMethod symbols cname methodName m{syms=typedFields,mtype=head errs}       
           [] -> case length . nub $ retTypes of
                   1 -> let typ = case methodType of
                                    Type t -> if t == head retTypes then Type t else TypeError $ "Return type mismatch for method " ++ ((mclass m) ++ "." ++ (mname m))
                                    Untyped -> head retTypes
                                    TypeError e ->  TypeError e
                                    Dependency d -> head retTypes
                        in updateMethod symbols cname methodName m {syms=typedFields,mtype=typ}
                   _ -> updateMethod symbols cname methodName m {syms=typedFields,mtype=TypeError $ "Return type mismatch for method " ++ ((mclass m) ++ "." ++ (mname m))}
                    -}  


typeMethods symbols cname [] = symbols
typeMethods symbols cname ((methname,m):ms)  = case getClass symbols cname of
                                     Nothing -> symbols
                                     Just c -> let newSymbols = typeMethod cname methname m symbols in typeMethods newSymbols cname ms
                                                            

addClassFields :: Map.Map QId QField -> Map.Map QId QField -> Map.Map QId QField
addClassFields classFields fields = let renamed = Map.mapKeys (\fname  -> "$this."++fname) classFields in
                                        Map.union renamed  fields
     
removeClassFields :: Map.Map QId QField ->  (Map.Map QId QField,Map.Map QId QField)
removeClassFields fields = let filterClassFields fname f = ("$this.") == take 6 fname
                               mapClassFields fname = drop 6 fname
                               classFields = Map.mapKeys mapClassFields . Map.filterWithKey filterClassFields $ fields
                               otherFields = Map.filterWithKey filterClassFields fields
                          in
                              (classFields,otherFields)

typeConstructor cls symbols = let clsname = (cname cls)
                                  constructor = (cconstructor cls)
                                  thisScope = Map.union (addThis clsname Map.empty) (addClassFields (cfields cls) (syms constructor))
                                  localFields = typeFields symbols{localScope=thisScope} ((cfields cls))
                                  consFields = typeFields symbols{localScope=thisScope} (syms constructor)
                                  newConstructor = constructor{syms=addClassFields localFields consFields}
                                  newClass = cls{cconstructor=newConstructor,cfields=localFields,cmethods=Map.insert (cname cls) newConstructor (cmethods cls)}
                              in updateClass symbols clsname newClass


typeClass :: QClass -> SymbolTable -> SymbolTable
typeClass c symbols = let newSyms = typeConstructor c symbols 
                          newSyms' = typeMethods newSyms (cname c) (Map.toList (cmethods c))
                      in newSyms'

typeClassN cls symbols = 
  case getClass symbols cls of
    Just c -> typeClass c symbols
    Nothing -> symbols


typeClasses classes symbols =
  case classes of
    [] -> symbols
    (c:cs) -> 
      let symbols' = typeClass c symbols in
        typeClasses cs symbols'

typeProgram p symbols = 
  let cs = map (\(name,c) -> c) . Map.toList $  quackClasses symbols 
      symbols' = typeClasses cs symbols
      inferredClasses = inferClasses . Map.toList $ (quackClasses symbols')
      
      fs = typeFields symbols' (quackFields symbols)
      symbols'' = symbols'{quackFields=fs}
      ProgramExp astcs astss _ = p
      statementErrs = typeStatements astss symbols''{localScope=fs} []
    
      allErrs = statementErrs
  in (symbols'',allErrs)


typeStatements statements symbols errs = 
  case statements of
    [] -> []
    (s:ss) -> (typeStatement s symbols) ++ (typeStatements ss symbols errs)


typeElifs elifs symbols =
  case elifs of
    [] -> []
    (e:es) -> let ElifBlockExp r sb line = e 
                  StatementBlockExp ss _  = sb
                  condErrs = case getType symbols r of
                               TypeError e -> [TypeError e]
                               Type t -> if t /= "Boolean" then [TypeError $ te_ElifCond line] else []
                               _ -> []
                  blockErrs = typeStatements ss symbols []
              in (condErrs ++ blockErrs) ++ (typeElifs es symbols)
  
typeStatement s symbols = 
  case s of

      BareExp b _ ->
        let RightExp r _ = b 
            errs = case getType symbols r of
                    TypeError e -> [TypeError e]
                    _ -> []
        in errs
      

      IfExp i line -> 
        let IfBlockExp r sb elifs els _ = i
            StatementBlockExp ss _ = sb 
            ifBlockErrs = typeStatements ss symbols []
            ifCondErrs = case getType symbols r of
                               TypeError e -> [TypeError e]
                               Type t -> if t /= "Boolean" then [TypeError $ te_IfCond line] else []
                               _ -> []
            elifErrs = typeElifs elifs symbols
            ElseBlockExp elseStatements _ = els
            StatementBlockExp elseSS _ = elseStatements
            elseBlockErrs = typeStatements elseSS symbols []
        in ifBlockErrs ++ ifCondErrs ++ elifErrs ++ elseBlockErrs 


      WhileExp w _ ->
        let WhileLoop (r,StatementBlockExp ss _) line = w
            errs = typeStatements ss symbols []
            condErrs = case getType symbols r of
                         TypeError e -> [TypeError (e ++ ("\n"++show r))]
                         Type t -> if t /= "Boolean" then [TypeError $ te_whileCond line] else []
                         _ -> []
        in condErrs ++ errs

      AssignmentExp a _ -> case a of
        UntypedAssignExp l r line ->
          case getName symbols l of
            (name,"") ->
              case (getType symbols  ( LeftExp l 0), getType symbols r) of {-()-}
                (Type t1, Type t2) -> if not (isSuperType symbols t1 t2) then [TypeError $ te_AssignMismatch line t2 t1] else []
                _ -> []
            (name,cls) ->
              case (getType symbols  ( LeftExp l 0), getType symbols r) of
                (Type t1, Type t2) -> if not (isSuperType symbols t1 t2) then [TypeError (te_AssignMismatch line t2 t1)] else []
                _ -> [] 
        TypedAssignExp l idType r line ->
          case getName symbols l of
            (name,"") ->
              case (getType symbols  ( LeftExp l 0), getType symbols r) of
                (Type t1, Type t2) -> if not (isSuperType symbols t1 t2) then [TypeError (te_AssignMismatch line t2 t1)] else []
                els -> []--[TypeError $ show els]
            (name,cls) ->
              case (getType symbols  ( LeftExp l 0), getType symbols r) of
                (Type t1, Type t2) -> if not (isSuperType symbols t1 t2) then [TypeError (te_AssignMismatch line t2 t1)] else []
                _ -> []
      _ -> []
    

getTypeChain st c cur  =
   case getClass st (csuper c) of
      Just super -> if (cname super) == "Obj" then cur++["Obj"] else  (getTypeChain st super (cur++[cname super]))
      Nothing -> cur


--c1 is a subtype of c2
isSubType st c1 c2 = if c1 == c2 then True else
  case getClass st c1 of
    Just c1' ->
        let c1TypeChain = getTypeChain st c1' []
        in c2 `elem` c1TypeChain
    Nothing -> False

isSuperType st c1 c2 = isSubType st c2 c1

 
collectTypeErrs symbols = 
    let fieldErrs = filterTypeErrors . map (\(name,f) -> ftype f) . Map.toList  $ (quackFields symbols)
    in fieldErrs


te_IfCond line = printf "Type error at line %d: If condition must be a Boolean" line
te_ElifCond line = printf "Type error at line %d: Elif condition must be a Boolean" line
te_whileCond line = printf "Type error at line %d: While loop condition must be a Boolean" line
te_AssignMismatch line t1 t2 = printf "Type error at line %d: Assigning type %s to an object of type %s, but %s does not extend %s\n" line t1 t2 t1 t2











filterDependencies t = case t of 
                         Dependency _ -> True
                         _ -> False

filterGoodTypes t = case t of
                      Type t -> True
                      _ -> False

filterErrs t = case t of
                        TypeError e -> True
                        _ -> False

filterBadTypes t = case t of
                      Dependency _ -> False
                      Type _ -> False
                      TypeError _ -> False
                      _ -> True        

makeDepName it = case (itnode it) of
                      IProgramField -> (itfield it)
                      IMethod -> (itclass it) ++ "." ++ (itmethod it)
                      IClassField -> (itclass it) ++ "$" ++ (itfield it)
                      IClass -> (itclass it)                      
                      IMethodField -> (itclass it) ++ "." ++ (itmethod it) ++ "$" ++ (itfield it)
                      IConstructorField -> (itclass it) ++ "." ++ (itmethod it) ++ "$" ++ (itfield it)
                      IField -> (itclass it) ++ "." ++ (itmethod it) ++ "$" ++ (itfield it)
{-
getLookupName dep = case itnode dep of
                     -- IProgramField -> (itfield it)
                     -- IMethod -> (itclass it) ++ "." ++ (itmethod it)
                     -- IClassField -> (itclass it) ++ "$" ++ (itfield it)
                     -- IClass -> (itclass it)                      
                      IMethodField -> (itclass it) ++ "." ++ (itmethod it) ++ "$" ++ (itfield it)
                     -- IConstructorField -> (itclass it) ++ "." ++ (itmethod it) ++ "$" ++ (itfield it)
                     -- IField -> (itclass it) ++ "." ++ (itmethod it) ++ "$" ++ (itfield it)-}
{-
getMethodDependencies m = let name = (mname m)
                              fields = Map.toList (syms m)
                              fieldDeps = map (\(fn,f) -> (makeDepName name "" fn,f) . filter (\(fn,f) -> filterDependencies (ftype f)) $ fields
                          in ():fieldDeps
  -}                            
inferClass c = let name = (cname c)
                   classFields = Map.toList (cfields c)
                   methods = (cmethods c)
                   constructor = (cconstructor c)
                   consDeps = inferMethodAndFields c constructor

                   fieldMap = map (\(fn,f) -> (makeDepName (inferClassField c f),inferClassField c f))
                   fieldFilter = filter (\(fn,f) -> filterDependencies (ftype f))
                   fieldDeps = fieldMap . fieldFilter $ classFields

                   methodMap = map (\(mn,m) -> inferMethodAndFields c m)
                  -- methodFilter = filter (\(mn,m) -> filterDependencies (mtype m))
                   methodDeps = concat . methodMap $ ( Map.toList   (cmethods c )  )
               in nub $ fieldDeps  ++ methodDeps -- ++ consDeps

inferClasses cs = concat $ map (\(cn,c) -> inferClass c) cs

data InferTypeNode= IClass | IMethod | IMethodField | IProgramField | IClassField | IConstructorField | IField deriving (Show,Eq)


data InferType = Itype {itnode::InferTypeNode,itclass::QId,itmethod::QId,itfield::QId,itdep::Maybe InferType} deriving (Show,Eq)


inferProgramField f = let node = IProgramField
                          field = fname f
                          dep = case ftype f of
                                  Dependency d -> Just d
                                  _ -> Nothing
                      in Itype{itnode=node,itclass="",itmethod="",itfield=field,itdep=dep} 

inferClassField c f = let node = IClassField
                          cls = cname c
                          field = fname f
                          dep = case ftype f of
                                  Dependency d -> Just d
                                  _ -> Nothing
                      in Itype{itnode=node,itclass=cls,itmethod="",itfield=field,itdep=dep}

inferMethodField c m f = let node = IMethodField
                             cls = cname c
                             field = fname f
                             method = mname m
                             dep = case ftype f of
                                    Dependency d -> Just d
                                    _ -> Nothing
                         in Itype{itnode=node,itclass=cls,itmethod=method,itfield=field,itdep=dep}



inferMethod c m = let dep = case mtype m of
                                  Dependency d -> Just d
                                 {- MethodReturn ->
                                    let returnExps = getReturns $ mbody m
                                        retTypes =  map (getType thisScope)  returnExps   
                                        goodRetTypes = filter filterGoodTypes retTypes
                                        retTypeErrs = filter filterTypeErrs retTypes
                                        retDeps = filter filterDependencies retTypes
                                        badRetTypes = filter filterDependencies retTypes
                                        case (badRetTypes,retTypeErrs,goodRetTypes,retDeps) of
                                          (b,_,_,_) -> 
                                  _ -> Nothing -}
                  in Itype{itnode=IMethod,itclass=cname c,itmethod=mname m,itfield="",itdep=dep}
                      --itfields = map (\f -> inferMethodField c m f) (syms m)
                --  in itmethod:itfields

inferMethodAndFields c m = let 
                               fieldMap = map (\(fn,f) -> (makeDepName (inferMethodField c m f),inferMethodField c m f))
                               fieldFilter = filter (\(fn,f) -> filterDependencies (ftype f))
                               itfields = fieldMap . fieldFilter . Map.toList . syms $ m
                               ret = case mtype m of
                                  Dependency d -> let itmethod = Itype{itnode=IMethod,itclass=cname c,itmethod=mname m,itfield="",itdep=Just d}
                                                  in (makeDepName itmethod,itmethod):itfields
                                  _ -> itfields
                           in ret
                     
getDepChain it deps symbols chain = 
  case itdep it of
   Nothing -> chain++[it]
   Just dep -> 
    case (dep `elem` (chain++[it]),itnode dep) of
      (True,_) -> (chain++[it])++[dep]
      (_,IMethod) -> case Map.lookup (makeDepName dep) deps of
                      Just dep' -> (getDepChain dep deps symbols (chain ++ [it])) 
                      Nothing -> case getMethod symbols (itclass it) (itmethod dep) of
                                  Just m -> case mtype m of
                                              Type t -> chain ++ [it] ++ [dep]
                                              _ -> (getDepChain dep deps symbols (chain ++ [it])) 
                                  Nothing -> (chain++[it])++[dep]
      (_,IField) -> case getMethodField symbols (itclass it) (itmethod it) (itfield dep) of
                      Just f -> case ftype f of
                                  Type t -> chain ++ [it] ++ [dep{itclass=(itclass it),itmethod=(itmethod it)}]
                                  _ -> (getDepChain dep deps symbols (chain ++ [it])) 
                      Nothing -> (chain++[it])++[dep]
      (_,IMethodField) -> case getMethodField symbols (itclass it) (itmethod it) (itfield dep) of
                            Just f -> case ftype f of
                                        Type t -> chain ++ [it] ++ [dep]
                                        _ -> (getDepChain dep deps symbols (chain ++ [it])) 
                            Nothing -> (chain++[it])++[dep]
      (_,IClassField) -> case getClass symbols (itclass it) of
                            Just c -> case Map.lookup (itfield dep) (cfields c) of
                                        Just f -> case ftype f of
                                          Type t -> chain ++ [it] ++ [dep]
                                          _ -> (getDepChain dep deps symbols (chain ++ [it])) 
                                        Nothing -> (chain++[it])++[dep]
                            Nothing -> (chain++[it])++[dep]
      
 
showDepChain deps = map (\d -> makeDepName d) deps  

typeDepChain :: SymbolTable -> [(InferType)] -> TypeCheck
typeDepChain symbols deps  = 
  if nub deps /= deps then TypeError $ "Circular inheritance: " ++ (show deps)
  else let dep = last deps in
    case itnode dep of
      IMethod -> case getMethod symbols (itclass dep) (itmethod dep) of
                  Just m -> case (mtype m) of
                              Type t -> Type t
                              TypeError e -> TypeError e
                              t -> TypeError $ "wat - IMethod 1" ++ (show t)
                  Nothing -> TypeError "wat - IMethod 2"
      IField -> case getMethodField symbols (itclass dep) (itmethod dep) (itfield dep) of
                        Just f -> case (ftype f) of
                                    Type t -> Type t
                                    TypeError e -> TypeError e
                                    t -> TypeError $ "wat - IMethodField 1" ++ (show t)
                        Nothing -> TypeError "wat - IMethodField 2"
      IMethodField -> case getMethodField symbols (itclass dep) (itmethod dep) (itfield dep) of
                        Just f -> case (ftype f) of
                                    Type t -> Type t
                                    TypeError e -> TypeError e
                                    t -> TypeError $ "wat - IMethodField 1" ++ (show t)
                        Nothing -> TypeError "wat - IMethodField 2"
      IClassField -> case getClass symbols (itclass dep) of
                      Just c -> case Map.lookup (itfield dep) (cfields c) of
                                  Just f -> case (ftype f) of
                                              Type t -> Type t
                                              TypeError e -> TypeError e
                                              _ -> TypeError "wat - IClassField"
                                  Nothing -> TypeError "wat - IClassField"
                      Nothing -> TypeError "wat - IClassField"
                              
                      

mapDepChains symbols inf = map (\dep -> let (n,d) = dep in (d ,typeDepChain symbols $ getDepChain d (Map.fromList inf) symbols []))  inf

updateDeps symbols typedDeps =
  case typedDeps of
    [] -> symbols
    ((dep,t):ds) -> 
      let symbols' =  case (itnode dep) of
                          IMethod -> case getMethod symbols (itclass dep) (itmethod dep) of
                              Just m -> updateMethod symbols (itclass dep) (itmethod dep) m{mtype=t}
                              Nothing -> symbols--updateMethod symbols (itclass dep) (itmethod dep) m{mtype=NoShow "updating unknown method"}

                          IField -> case getMethodField symbols (itclass dep) (itmethod dep) (itfield dep) of
                              Just f -> updateMethodField symbols (itclass dep) (itmethod dep) (itfield dep) f{ftype=t} 
                              Nothing -> symbols--updateMethodField symbols (itclass dep) (itmethod dep) (itfield dep) f{ftype=NoShow "updating unknown field"}

                          IMethodField -> case getMethodField symbols (itclass dep) (itmethod dep) (itfield dep) of
                                          Just f -> updateMethodField symbols (itclass dep) (itmethod dep) (itfield dep) f{ftype=t} 
                                          Nothing -> symbols--updateMethodField symbols (itclass dep) (itmethod dep) (itfield dep) f{ftype=NoShow "updating unknown method field"}
                 
                          IClassField -> case getClassField symbols (itclass dep)  (itfield dep) of
                                          Just f -> updateClassField symbols (itclass dep) (itfield dep) f{ftype=t} 
                                          Nothing ->symbols--updateClassField symbols (itclass dep) (itfield dep) f{ftype=NoShow "updating unknown class field"}

      in updateDeps symbols' ds                                               



