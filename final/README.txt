Quack Compiler
Author: Dustin Reid


How to compile:

./quack sourcefile

Outputs a file called output.c, if compilation is successful. Otherwise, error messages are displayed.

Upon successful compilation, run

./compile output.c

And then

./output

to run the resulting program.



Line numbers in error messages are not correct. For my "bad" samples, I put comments where type errors should occur with the corresponding error message.



As far as code generation goes, almost everything works assuming the input program has types declared.
Dynamic method dispatch and short circuit evaluation are working.

What isn't working:
  -There are no comparison operators for strings
 
  -Fields inherited from super classes must be initialized in the constructors for subclasses. I can't remember if we were supposed to automatically call the constructor
    for the super class when calling the constructor for a class. If they aren't initialized, this is will still compile and probably result in runtime errors.



Issues with class inheritance are not checked, but I believe are working in my submission for the abstract syntax tree.



There is also no garbage collection, so there are definitely memory leaks.



Currently, type checking is not completely working. Some type checks work as demonstrated in samples in the bad directory.
What doesn't work includes 

Checking for incompatible overridden methods

Bad method return types, or methods without return statements

Bad argument types/argument length

Type checking across all execution paths

Assignments using previously defined variables give a type error when they shouldn't. 
For example:

  x = 3;
  y = x;

will result in a type error saying 'x' is an unidentified variable on the line y = x;

Statements in method and constructor bodies are not currently being type checked. I would assume my current type checking works as is in those cases,
but I didn't have time to implement.


