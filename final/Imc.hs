module Imc (module Imc) where

import ImcUtil
import QuackAST
import qualified Data.Map.Strict as Map
import Text.Printf (printf)


printImc tempdecs source =  c_boilerplate (printf source tempdecs)

--toImc :: SymbolTable -> ASTNode -> String
toImc symbols ast imcST =
  case ast of
    ASTProgram p -> let (cs,ss) = programCode symbols imcST p in
                       cs ++ "\n\nvoid quackmain() {\n"++ss++"\n}\n" 
    
    ASTClasses cs ->  foldl (++) "" . map (\c -> (++) (toImc symbols (ASTClassExp c) imcST) "\n\n\n") $ reverse cs

    ASTClassExp c -> 
      let ClassDecExp sig _ _ = c 
          (clsname,consArgsCode,consDecArgsCode,super) = classSigCode symbols sig
      in case Map.lookup clsname (imClasses imcST) of
          Nothing -> ""
          Just cls -> 
            let constructorCode = (imConstructorCode cls)
                constructorSig = methodSigTemplate clsname ("constructor") consDecArgsCode
                fieldsCode = imFieldsCode cls
                methodList = (immethodDecs cls)
                methodDecs = map (\(name,(_,dec)) -> (dec)) methodList
                methodRefs = map (\(name,(ref,_)) -> (ref)) methodList

                structDecMethodList = foldl (\a b -> a ++ "" ++ b)  constructorSig methodDecs 
                structMethodList = foldl (\a b -> a ++ ",\n" ++ b)  ("new_"++clsname) methodRefs  
                
                structCode = classStructTemplate clsname clsname  structMethodList

                methods = map (\(name,m) -> imMethodCode m) (Map.toList $ imMethods cls)
                --(methodFunctions) = map ( \(_,_,f) -> f ) methods

                printMethod = printMethodTemplate clsname
                formattedMethods = (foldl (\s1 s2 -> s1 ++ "\n" ++ s2) constructorCode methods) ++ "\n" ++ printMethod
                
            in (printf classTemplate  clsname clsname clsname clsname clsname fieldsCode clsname clsname clsname clsname
                                     structDecMethodList clsname clsname formattedMethods structCode clsname clsname clsname)

    ASTStatements ss -> let (code,count) = newStatementsCode symbols 0 ss in code -- foldl (++) "" . map (\s -> (++)  (statementCode symbols s) "\n") $ reverse ss
    ASTStatement s -> let (code,count) = statementCode symbols 0 s in code 
    ASTRightExp r -> let (rCode, rTemp, c) = rightExpCode symbols 0 r in rCode--TODO: check counter here
       
{-
    ASTClassExp ClassExp 
    ASTClassSig ClassSig 
    ASTClassBody ClassBody 
    ASTFormalArg FormalArg 
    ASTMethod Method -}
    _ -> ""


programCode symbols  imcST p =
  let ProgramExp cs ss line = p
      classCode =  (toImc symbols (ASTClasses cs) imcST)
      vars = vST (reverse $ ss) symbols{localScope=quackFields symbols} []
      varCode = printVarDecs (Map.toList . Map.fromList $ vars)
      inheritedSyms = getInheritedST symbols
      statements = (toImc inheritedSyms{localScope=(quackFields symbols)}  (ASTStatements . reverse $  ss)) imcST
  in (classCode,varCode++"\n"++statements)



classCode symbols c = 
  let ClassDecExp sig body _ = c 
      (clsname,consArgsCode,consDecArgsCode,super) = classSigCode symbols sig
  in  case getClass symbols clsname of
        Nothing -> ("BAD CLASS !!!!","")
        Just cls -> 
          let ClassBodyExp ss ms _ = body
              constructorMethod = (cconstructor cls)
              constructorScope = Map.union (addThis clsname Map.empty) (addClassFields (cfields cls) (syms constructorMethod))
              constructorVars = vST (reverse $ ss) symbols{localScope=constructorScope} []
              constructorVarsCode = printVarDecs (Map.toList . Map.fromList $ constructorVars)
              (constructorBody,count) = newStatementsCode symbols{localScope=constructorScope} 0 (reverse ss)
              constructorSig = methodSigTemplate clsname ("constructor") consDecArgsCode
              constructorCode = constructorTemplate clsname clsname consArgsCode  clsname clsname clsname clsname clsname (constructorVarsCode++"\n"++constructorBody)
              fieldsCode = classFieldsCode symbols (map (\(name,field) -> field) . Map.toList $ cfields cls)
          in  (constructorCode,fieldsCode)


classSigCode symbols sig =
  case sig of
     ClassSigExp clsname args _ -> (clsname,argsCode args,decArgsCode args,"Obj")
     ExtendedClassSigExp clsname args super _ -> (clsname,argsCode args,decArgsCode args,super)



classFieldCode symbols f =
    case (ftype f) of
      Type t -> printf "obj_%s %s;\n" t (fname f)
      _ -> "BAD FIELD!!!!\n"

classFieldsCode symbols fs =
    case fs of 
      [] -> ""
      (f:fs) -> classFieldCode symbols f ++ (classFieldsCode symbols fs)
  
                  

data ImcMethod = ImMethodSkip | ImMethodErr  | ImMethod { immethod ::QMethod, imMethodName :: String, imSigCode :: String, imRefCode ::String, imMethodCode :: String } deriving Show
data ImcClass = ImClass {imclass :: QClass, imClsName :: String, imConstructorCode :: String, imFieldsCode :: String, imMethods :: Map.Map String ImcMethod,
                            immethodDecs :: [(QId,(String,String))] } deriving Show
data ImcSymTable = ImTable {imClasses :: Map.Map String ImcClass} deriving Show

{-
defaultImcClasses =
  Map.fromList [
    ("Obj", ImClass {imclass :: builtInObj, imClsName="Obj",imConstructorCode="",imFieldsCode="",
      imMethods=()
  ]-}

buildImcSymsTable ast st imcST =
  case ast of
    ASTProgram p -> let ProgramExp cs ss line = p  
                        imcST' = buildImcSymsTable (ASTClasses cs) st imcST
                    in getMethodDecs st imcST' 
    ASTClasses cs -> 
      case cs of
        [] ->  imcST
        (c:cs) -> let newImcST = buildImcSymsTable (ASTClassExp c) st imcST
                  in buildImcSymsTable (ASTClasses cs) st newImcST

    ASTClassExp c ->
      let ClassDecExp sig body _ = c
          ClassBodyExp ss ms _ = body 
          (clsname,consArgsCode,consDecArgsCode,super) = classSigCode st sig
      in case getClass st clsname of
        Nothing -> imcST
        Just cls -> 
          let (constructorCode,fieldsCode) =  classCode st c
              methods = Map.fromList . map (\m -> methodCode st cls m) $  ms
              imCls = ImClass {imclass=cls,imClsName=clsname,imConstructorCode=constructorCode,imFieldsCode=fieldsCode,imMethods=methods}
          in imcST{imClasses=Map.insert clsname imCls (imClasses imcST)}




getMethodDecs symbols  imcST =
  let cs = Map.toList $ imClasses imcST
      cleanMethods = map (\(name,(_,r,d)) -> (name,(r,d))) . filter (\(name,(cls,_,_)) -> name /= cls)  
      cs' = map (\(name,c) -> (name,c{immethodDecs=cleanMethods . Map.toList $ getInheritedMethods symbols (imclass c)})) cs
  in imcST{imClasses=Map.fromList cs'}





methodCode :: SymbolTable -> QClass -> Method -> (String,ImcMethod)
methodCode symbols c m = 
  case m of 
    MethodExp name args sb _ -> 
      case getMethod symbols  (cname c) name of
        Nothing -> ("$bad_method_"++(cname c)++name,ImMethodErr)
        Just m ->{- if (mclass m) /= (cname c) then ("",ImMethodSkip) else  -}
                    let StatementBlockExp ss _  = sb
                        (methodBodyCode,count) = newStatementsCode symbols{localScope=Map.union (addThis (cname c) Map.empty) (syms m)} 0 (reverse ss)
                        methodVars = vST (reverse $ ss) symbols{localScope=Map.union (addThis (cname c) Map.empty) (syms m)} []
                        methodVarsCode = printVarDecs (Map.toList . Map.fromList $ methodVars)
                        methodBodyCode' = methodVarsCode ++ "\n" ++ methodBodyCode
                        methodSigCode = methodSigTemplate (cname c) (mname m) (argsCode (args)) --("obj_%s (*%s) (%s);\n") -- className methodName args
                        methodRefCode =  printf "%s_method_%s" (cname c) name
                        mCode = case (mtype m) of
                                  Type t -> methodTemplate t ((cname c) ++ "_method_" ++ mname m) (argsCode ([FormalArgExp "this" (cname c) 0] ++ args)) methodBodyCode'
                                  _ -> "BAD METHOD TYPE !!!!"
                    in (name,ImMethod {immethod=m,imMethodName=name,imSigCode=methodSigCode,imRefCode=methodRefCode,imMethodCode=mCode})
    TypedMethodExp name args typ sb _ ->
      case getMethod symbols  (cname c) name of
        Nothing -> ("$bad_method_"++(cname c)++name,ImMethodErr)
        Just m -> {-if (mclass m) /= (cname c) then ("", else -}
                    let StatementBlockExp ss _  = sb
                        (methodBodyCode,count) = newStatementsCode symbols{localScope=Map.union (addThis (cname c) Map.empty) (syms m)} 0 (reverse ss)
                        methodVars = vST (reverse $ ss) symbols{localScope=Map.union (addThis (cname c) Map.empty) (syms m)} []
                        methodVarsCode = printVarDecs (Map.toList . Map.fromList $ methodVars)
                        methodBodyCode' = methodVarsCode ++ "\n" ++ methodBodyCode
                        methodSigCode = methodSigTemplate (cname c) (mname m) (argsCode args) --("obj_%s (*%s) (%s);\n") -- className methodName args
                        methodRefCode =  printf "%s_method_%s" (cname c) name
                        mCode = case (mtype m) of
                                  Type t -> methodTemplate t ((cname c) ++ "_method_" ++ mname m) (argsCode ([FormalArgExp "this" (cname c) 0] ++ args)) methodBodyCode'
                                  _ -> "BAD METHOD TYPE !!!!"
                       -- mCode = methodTemplate (mtype m) (mname m) (argsCode args) methodBodyCode
                    in (name,ImMethod {immethod=m,imMethodName=name,imSigCode=methodSigCode,imRefCode=methodRefCode,imMethodCode=mCode})



elifCode symbols tempCount elifs = 
  case elifs of 
    [] -> ("",tempCount)
    (e:es) ->
      let ElifBlockExp r sb _ = e 
          (elifCondCode,elifCondTemp,c) = rightExpCode symbols tempCount r
          StatementBlockExp ss _  = sb 
          (elifBlock,c') = (newStatementsCode symbols{localScope=typeFields symbols (quackFields symbols)} c (reverse ss))         
          formatter = printf ("//ELIF Exp \n"++
                               "%s\n"++ -- elifCondCode
                               "if(%s == lit_true){\n"++ --elifCondTemp           
                               "%s\n"++ -- elifBlock
                               "}else{\n%s\n")                    
          (rest,c'') = (elifCode symbols c' es)
      in  (formatter elifCondCode elifCondTemp elifBlock rest,c'')  


--statementsCode symbols  ss = foldl (++) "" . map (\s -> (++)  (statementCode symbols s) "\n") $ reverse ss

newStatementsCode st c statements = 
  case statements of
    [] -> ("",c)
    (s:ss) -> 
      let (sCode,c') = statementCode st c s
          (rest,c'') = newStatementsCode st c' ss
      in (sCode ++ "\n" ++ rest, c'')

statementCode symbols c s = 
  case s of
    IfExp i line -> 
      let-- vars = vST [s] symbols []
         -- varsCode = printVarDecs vars
          IfBlockExp r sb elifs els _ = i
          (ifCondCode,ifCondTemp,c') = rightExpCode symbols c r
          StatementBlockExp ss _ = sb 
          (ifBlock,c'') = (newStatementsCode symbols{localScope=typeFields symbols (quackFields symbols)} c' (reverse ss))
          (elifBlocks,c''') = elifCode symbols c'' elifs
          ElseBlockExp elseStatements _ = els
          StatementBlockExp elseSS _ = elseStatements
          (elseBlock,c'''') = (newStatementsCode symbols{localScope=typeFields symbols (quackFields symbols)} c''' (reverse elseSS))
          rest = foldl (++) (elifBlocks ++ elseBlock) ["\n}\n" | n <- [0..(length elifs)]]
          formatter = printf ("//IF Exp \n"++
                             --  "/*VAR DECS*/\n%s\n"++ -- varsCode
                               "%s\n"++ -- ifCondCode
                               "if(%s == lit_true){\n"++ --ifCondTemp           
                               "%s\n"++ -- ifBlock
                               "}else{\n%s\n") -- rest
      in (formatter  ifCondCode ifCondTemp ifBlock rest,c'''')

    WhileExp w line ->
      let WhileLoop (r,StatementBlockExp ss _) line = w 
          (rCode,rTemp,c') = rightExpCode symbols c r
          (rCode2,rTemp2,c'') = rightExpCode symbols c' r
          (statementCode,c''') = (newStatementsCode symbols{localScope=typeFields symbols (quackFields symbols)}  c'' (reverse ss))


          formatter = printf ("//WHILE Exp\n%s\n"++ --rcode
                              "if(!(%s == lit_true)){\n"++ --if not rTemp
                              "    goto DONE%d;\n"++ --TODO: -- labelnum
                              "}\n"++
                              "LOOP%d: 0;\n"++ -- labelnum
                              "%s\n"++ --statementCode
                              "\n%s\n"++ --rCode2
                              "if(%s == lit_true){\n"++ --if rTemp2
                              "    goto LOOP%d;\n" ++ -- labelnum
                              "}\n" ++
                              "DONE%d: 0;\n") -- labelnum
      in (formatter rCode  rTemp c c statementCode rCode2 rTemp2 c c,c''')

    ReturnExp r line -> case r of
                          NoReturnExp line -> ("//Bare Return Exp\nreturn;\n",c)
                          RetExp r line -> 
                            let (rCode,rTemp,c') = rightExpCode symbols c r 
                                rCode' = (if rCode==rTemp then "" else "\n"++rCode++";\n")
                                formatter = printf ("//StatementExp UntypedAssignExp\n"++
                                                    "%s\n"++ --rCode
                                                    "return %s;\n") --refTemp  = rTemp
                            in (formatter rCode' rTemp,c')

    AssignmentExp a line ->
      case a of      
        UntypedAssignExp l r _ -> 
          case getName symbols l of
            (name,"") ->
              let idType = case getType symbols  ( LeftExp l line) of
                            Type t -> t
                            _ -> "BAD TYPE OR LOCAL FIELD LOOKUP (statementCode UntypedAssignExp)!!!!!!\n"++(show (localScope symbols))
                  (rCode,rTemp,c') = rightExpCode symbols c r
                  (lCode,lTemp,c'') = leftExpCode symbols c' l
                  refTemp = makeTempVar (c''+1)
                  formatter = printf ("//StatementExp UntypedAssignExp\n"++
                                      "%s\n"++ --rCode
                                     -- "obj_%s %s;\n"++ --idType id
                                      "obj_%s %s;\n"++ --idType refTemp
                                      "%s = (%s = %s);\n") --refTemp  = rTemp
                  rCode' = (if rCode==rTemp then "" else "\n"++rCode++";\n")
              in (formatter rCode' {-idType name-} idType refTemp refTemp name rTemp,c''+2)--, refTemp, c+1)
            
            (name,cls) ->
              let idType = case getType symbols  ( LeftExp l line) of
                            Type t -> t
                            _ -> "BAD TYPE OR LOCAL FIELD LOOKUP (statementCode UntypedAssignExp)!!!!!!\n"++(show (localScope symbols))
                  (rCode,rTemp,c') = rightExpCode symbols c r
                  (lCode,lTemp,c'') = leftExpCode symbols c' l
                  refTemp = makeTempVar (c''+1)
                  formatter = printf ("//StatementExp UntypedAssignExp\n"++
                                      "%s\n"++ --lCode
                                      "%s\n"++ --rCode
                                      "%s->%s = %s;\n") -- ++ --lTemp name rTemp
                                     -- "obj_%s %s;\n"++ --idType refTemp
                                      --"%s = %s->%s;\n") --refTemp  = lTemp name
                  rCode' = (if rCode==rTemp then "" else "\n"++rCode++";\n")
               in (formatter lCode rCode' cls name  rTemp ,c''+2)--, refTemp, c+1)

        TypedAssignExp l idType r _ -> 
          case getName symbols l of
            (name,"") -> 
              let (rCode,rTemp,c') = rightExpCode symbols c r
                  (lCode,lTemp,c'') = leftExpCode symbols c' l
                  refTemp = makeTempVar (c''+1)
                  formatter = printf ("//StatementExp UntypedAssignExp\n"++
                                      "%s\n"++ --rCode
                                   --   "obj_%s %s;\n"++ --idType id
                                      "obj_%s %s;\n"++ --idType refTemp
                                      "%s = (%s = %s);\n") --refTemp  = rTemp
                  rCode' = (if rCode==rTemp then "" else "\n"++rCode++";\n")
              in (formatter rCode' {-idType name-} idType refTemp refTemp name rTemp,c''+2)            
            (name,cls) ->
              let idType = case getType symbols  ( LeftExp l line) of
                            Type t -> t
                            _ -> "BAD TYPE OR LOCAL FIELD LOOKUP (statementCode UntypedAssignExp)!!!!!!\n"++(show (localScope symbols))
                  (rCode,rTemp,c') = rightExpCode symbols c r
                  (lCode,lTemp,c'') = leftExpCode symbols c' l
                  refTemp = makeTempVar (c''+1)
                  formatter = printf ("//StatementExp UntypedAssignExp\n"++
                                      "%s\n"++ --lCode
                                      "%s\n"++ --rCode
                                      "%s->%s = %s;\n") -- ++ --lTemp name rTemp
                                     -- "obj_%s %s;\n"++ --idType refTemp
                                      --"%s = %s->%s;\n") --refTemp  = lTemp name
                  rCode' = (if rCode==rTemp then "" else "\n"++rCode++";\n")
               in (formatter lCode rCode' cls name  rTemp ,c''+2)--, refTemp, c+1)


    BareExp b line -> let RightExp r _ = b 
                          (rCode,retTemp,c') = rightExpCode symbols c r
                          bareCode = rCode ++ ";"
                      in (bareCode,c')
    --_ -> ""


leftExpCode :: SymbolTable -> Int -> LeftExp -> (String,String,Int)
leftExpCode symbols c l = 
   case l of 
      IdRef id l -> 
        let idType = case Map.lookup id (localScope symbols) of
                        Nothing -> "BAD LOCAL FIELD LOOKUP (leftExpCode IDRef)!!!!!!"
                        Just f -> case (ftype f) of
                                  Type t -> t
                                  _ -> "BAD TYPE (leftExpCode IDRef)!!!!!" {- TODO: get type of id and set it to a temp var -}
            refTemp = makeTempVar c
            formatter = printf ("//LEFT EXPRESSION IdRef\n"++
                                "obj_%s %s;\n"++ --idType refTemp
                                "%s = %s;\n") --refTemp = id
        in (formatter idType refTemp refTemp id, refTemp, c+1)
                    
      ObjIdRef r id l -> 
        let  (rCode,rTemp,c') = rightExpCode symbols c r
             refTemp = makeTempVar c'
             rType = case  getType symbols r of
                      Type t -> t
                      _ -> "BAD TYPE 1!!!!!" 
             idType = case getClass symbols rType of
                      Nothing -> "BAD CLASS!!!!!"
                      Just c -> case Map.lookup id (cfields c) of
                                  Just f -> case ftype f of 
                                              Type t -> t
                                              _ -> "BAD TYPE 2!!!!!"
                                  Nothing -> "BAD CLASS FIELD LOOKUP!!!!!!" ++ id
             rCode' = (if rCode==rTemp then "" else "\n"++rCode++";\n")
             formatter = printf ("\n//LEFT EXPRESSION ObjIdRef\n"++
                                "%s\n"++ --rcode'
                                "obj_%s %s;\n"++ --id Type refTemp                            
                                "%s = %s->%s;\n") --refTemp = rTemp->id
        in (formatter rCode' idType refTemp refTemp rTemp id,refTemp,c'+1)

rightExpCode :: SymbolTable -> Int -> RightExp -> (String,String,Int)
rightExpCode symbols c r  =
  case r of
    StrExp s line -> ("str_literal(\""++s++"\")","str_literal(\""++s++"\")",c)
    IntExp n line -> ("int_literal("++(show n)++")","int_literal("++(show n)++")",c)
    BoolExp b line -> case b of
                        True -> ("lit_true","lit_true",c)
                        False -> ("lit_false","lit_false",c) --(if b then "lit_true" else "lit_false",if b then "lit_true" else "lit_false",c)
    BinaryExp op a b line -> case op of
                               "and" -> andExpCode symbols c a b
                               "or" -> orExpCode symbols c a b
                               _ -> binaryExpCode symbols c op a b
    NotExp r line -> let (rCode, rTemp, c') = (rightExpCode symbols c r) 
                         refTemp = makeTempVar c'
                         rCode' = (if rCode==rTemp then "" else "\n"++rCode++";\n")
                         formatter = printf ( "\n//NOT\n"++
                                              "%s\n"++ -- rCode'  {- TODO might not be a good condition for this -}         
                                              "obj_Boolean %s;\n\n"++ -- declare refTemp   
                                              "if(%s == lit_false){\n"++ -- if rTemp == lit_false
                                              "  %s = lit_true;\n"++ --refTemp = lit_true
                                              "}else{\n"++
                                              "  %s = lit_false;\n"++ --refTemp = lit_false
                                              "}\n")
                     in (formatter rCode' refTemp rTemp refTemp refTemp,refTemp,c'+1)
    ParExp r line -> let (rCode, rTemp, c') = (rightExpCode symbols c r) 
                         refTemp = makeTempVar c'
                         rType = case getType symbols r of
                                      Type t -> t
                                      _ -> "BAD TYPE!!!!"
                         rCode' = (if rCode==rTemp then "" else "\n"++rCode++";\n")
                         formatter = printf  ("\n//PAR EXPRESSION\n"++
                                              "%s\n"++ --rcode'
                                              "obj_%s %s;\n"++ --r Type refTemp                            
                                              "%s = %s;\n") --refTemp = remp
                     in (formatter rCode' rType refTemp refTemp rTemp,refTemp,c'+1)
    FuncCallExp r method args line -> 
      let (argsCode,argRefs,c') = actualArgsCode symbols c (reverse args)
          (rCode,rTemp,c'') = rightExpCode symbols c' r
          refTemp = makeTempVar c''
          rType = case  getType symbols r of
                    Type t -> t
                    _ -> "BAD TYPE 1!!!!!" 
          methodType = case getMethod symbols rType method of
                        Nothing -> "BAD METHOD!!!!! " ++ (method) ++ " " ++ rType
                        Just m -> case mtype m of 
                                    Type t -> t
                                    _ -> "BAD METHOD TYPE 2!!!!!"
          rCode' = (if rCode==rTemp then "" else "\n"++rCode++";\n")
          argRefs' = case argRefs of 
                      "" -> rTemp
                      els -> rTemp++","++els
          formatter = printf ("\n//FUNC CALL EXPRESSION\n"++
                              "%s\n"++ --rcode'
                              "%s\n"++ --argsCode
                              "obj_%s %s;\n"++ --method Type refTemp                            
                              "%s = %s->clazz->%s(%s);\n") --refTemp = rTemp->clazz->method (args)      
          in (formatter rCode' argsCode methodType refTemp refTemp rTemp method argRefs', refTemp, c''+1)   
    ConstructorExp cls args line ->
        let (argsCode,argRefs,c') = actualArgsCode symbols c (reverse args)
            refTemp = makeTempVar c'
            formatter = printf("\n//CONSTRUCTOR CALL EXP\n"++
                                "%s"++ --argsCode
                                "obj_%s %s;\n"++ -- cls refTemp
                                "%s = the_class_%s->constructor(%s);\n") -- refTemp cls args
        in (formatter argsCode cls refTemp refTemp cls argRefs,refTemp,c'+1)

-- ("the_class_"++cls++"->constructor("++"args"++")","",c)
    LeftExp l line -> leftExpCode symbols c l
    _ -> ("","",c)


actualArgsCode symbols c args =
  case args of
    [] -> ("","",c)
    (a:[]) -> let (acode,aTemp,c') = rightExpCode symbols c a
                  aCode' = (if acode==aTemp then "" else "\n"++acode++";\n") 
              in (aCode',aTemp,c')
    (a:as) -> let (acode,aTemp,c') = rightExpCode symbols c a
                  aCode' = (if acode==aTemp then "" else "\n"++acode++";\n")
                  (rest,restTemps,c'') = actualArgsCode symbols c' as
              in (aCode'++"\n"++rest,aTemp++", "++restTemps,c'')

binaryExpCode :: SymbolTable -> Int -> String -> RightExp -> RightExp -> (String,String,Int)
binaryExpCode symbols c op a b =  
  let (aCode,aTemp,c') = rightExpCode symbols c a
      (bCode,bTemp,c'') = rightExpCode symbols c' b
      aType = getType symbols a
      returnType = case aType of
                      Type t -> case getMethod symbols t func of
                                  Just m -> case mtype m of
                                             Type t' -> t'
                                             _ -> "BAD TYPE 1!!!!"
                                  Nothing -> "BAD METHOD TYPE 2!!!!!" ++ t ++ " " ++ func
                      _ -> "BAD TYPE 3!!!!!" ++ (show aType) ++ (show a)
      binTemp = makeTempVar c''
      c''' = c''+1
      aCode' = (if aCode==aTemp then "" else "\n"++aCode++";\n")
      bCode' = (if bCode==bTemp then "" else "\n"++bCode++";\n")
      formatter = printf  ("\n//BINARY EXPRESSION\n"++
                           "%s\n"++ --aCode
                           "%s\n"++ --bCode
                           "obj_%s %s;\n"++ --binExpType binTemp
                           "%s = %s->clazz->%s(%s,%s)") --binTemp = aTemp->clazz->FUNC(aTemp,bTemp)
      func = case op of
                "+" -> "PLUS"--,"Int")
                "-" -> "MINUS"--,"Int")
                "*" -> "TIMES"--,"Int")
                "/" -> "DIVIDE"--,"Int")
                "<" -> "LESS"--,"Boolean")
                ">" -> "MORE"--,"Boolean")
                "<=" -> "LEQ"--,"Boolean")
                ">=" -> "GEQ"--,"Boolean") 
                "==" -> "EQUALS"
{-                "and" -> (andExpCode symbols c a b,
                "or" -> orExpCode symbols c a b-}
                _ -> "BAD BINARY EXPRESSION!!!!!!"
  in (formatter aCode' bCode' returnType binTemp binTemp aTemp func aTemp bTemp, binTemp, c''')

andExpCode :: SymbolTable -> Int -> RightExp -> RightExp -> (String,String,Int)
andExpCode symbols c a b =
  let (aCode,aTemp,c') = rightExpCode symbols c a 
      (bCode,bTemp,c'') = rightExpCode symbols c' b
      andResultTemp = makeTempVar c''
      c''' = c''+1
      aCode' = (if aCode==aTemp then "" else "\n"++aCode++";\n")
      bCode' = (if bCode==bTemp then "" else "\n"++bCode++";\n")
      andCode = printf  ( "\n//AND\n"++
                          "%s\n"++ -- aCode  {- TODO might not be a good condition for this -}         
                          "obj_Boolean %s;\n\n"++ -- declare andResultTemp   
                          "if(%s == lit_false){\n"++ -- if aTemp == lit_false
                          "  %s = lit_false;\n"++ --andResultTemp = lit_false
                          "}else{\n"++
                          "%s"++ --bcode   
                          "%s = %s;\n"++ --andResultTemp = bTemp
                          "}\n") aCode' andResultTemp aTemp andResultTemp bCode' andResultTemp bTemp
      in
        (andCode,andResultTemp,c''') 

orExpCode :: SymbolTable -> Int -> RightExp -> RightExp -> (String,String,Int)
orExpCode symbols c a b =
  let (aCode,aTemp,c') = rightExpCode symbols c a 
      (bCode,bTemp,c'') = rightExpCode symbols c' b
      orResultTemp = makeTempVar c''
      c''' = c''+1
      aCode' = (if aCode==aTemp then "" else "\n"++aCode++";")
      bCode' = (if bCode==bTemp then "" else "\n"++bCode++";")
      orCode = printf  (  "\n//OR\n"++
                          "%s"++ -- aCode  {- TODO might not be a good condition for this -}         
                          "obj_Boolean %s;\n\n"++ -- declare andResultTemp   
                          "if(%s == lit_true){\n"++ -- if aTemp == lit_false
                          "  %s = lit_true;\n"++ --andResultTemp = lit_false
                          "}else{\n"++
                          "%s"++ --bcode   
                          "  %s = %s;\n"++ --andResultTemp = bTemp
                          "}\n") aCode' orResultTemp aTemp orResultTemp bCode' orResultTemp bTemp
      in
        (orCode,orResultTemp,c''') 





printVarDecs vars =
  case vars of
        [] -> ""
        (v:[]) -> varDec v
        (v:vs) -> foldl (\v1 v2 -> v1 ++ (varDec v2)) (varDec v) (vs )
  where varDec (vname,vtype) = printf "obj_%s %s;\n" vtype vname

elifVars elifs symbols varlist = 
  case elifs of 
    [] -> varlist
    (e:es) ->
      let ElifBlockExp r sb _ = e 
          StatementBlockExp ss _  = sb 
          vars = elifVars es symbols varlist
      in  varlist++vars



vST statements symbols varList {-temps-} = 
  case statements of
    [] -> varList
    (s:ss) -> let varList' = varDecs  s symbols varList
              in    vST ss symbols varList'

--makeTemps :: ASTNode -> ExpressionStore -> ExpressionStore
varDecs s  symbols varList = 
  case s of
    BareExp b  l -> varList -- let RightExp r _ = b in varDecs (ASTRightExp r) symbols varList

            
    IfExp i line -> 
      let IfBlockExp r sb elifs els _ = i
          StatementBlockExp ifSS _ = sb 
          ifVars = vST ifSS symbols varList
          theElifVars = elifVars elifs symbols varList
          ElseBlockExp elseStatements _ = els
          StatementBlockExp elseSS _ = elseStatements
          elsVars = vST elseSS symbols varList
          allVars = case (elifs,elseSS) of
                      ([],[]) -> Map.fromList ifVars
                      (_,[]) -> Map.union  (Map.fromList ifVars) (Map.fromList theElifVars)
                      ([],_) -> Map.union (Map.fromList ifVars) (Map.fromList elsVars)
                      (_,_) -> Map.union (Map.union (Map.fromList ifVars) (Map.fromList theElifVars)) (Map.fromList elsVars)
          --allVars = Map.intersection (Map.fromList ifVars) (Map.fromList elsVars) --Map.intersection (Map.intersection (Map.fromList ifVars) (Map.fromList theElifVars)) (Map.fromList elsVars)
      in  varList++(Map.toList allVars) --(formatter ifCondCode ifCondTemp ifBlock rest,c'''')

    WhileExp w line ->
      let WhileLoop (r,StatementBlockExp ss _) line = w in vST ss symbols varList

    AssignmentExp a _ -> case a of
      UntypedAssignExp l r _ ->
        case getName symbols l of
          (name,"") ->
            let idType = case getType symbols  ( LeftExp l 0) of
                          Type t -> t
                          _ -> "BAD TYPE OR LOCAL FIELD LOOKUP!!!!!!\n"
            in (name,idType):varList
          (name,cls) ->
            let idType = case getType symbols  ( LeftExp l 0) of
                          Type t -> t
                          _ -> "BAD TYPE OR LOCAL FIELD LOOKUP!!!!!!\n"
            in varList--(name,idType):varList
      TypedAssignExp l idType r _ ->
        case getName symbols l of
          (name,"") ->
            let idType = case getType symbols  ( LeftExp l 0) of
                          Type t -> t
                          _ -> "BAD TYPE OR LOCAL FIELD LOOKUP!!!!!!\n"
            in (name,idType):varList
          (name,cls) ->
            let idType = case getType symbols  ( LeftExp l 0) of
                          Type t -> t
                          _ -> "BAD TYPE OR LOCAL FIELD LOOKUP!!!!!!\n"
            in varList--(name,idType):varList

    _ -> varList
    


{- Making temporary variables for literals -}
mST statements store {-temps-} = 
  case statements of
    [] -> store
    (s:ss) -> let store' = makeTemps (ASTStatement s) store
              in    mST ss store' {-((s',store):temps)-}

data ExpressionStore = ExpStore {tint :: Map.Map Int QId, cint :: Int, tstr :: Map.Map String QId, cstr :: Int} deriving Show
emptystore = ExpStore{tint=Map.empty,cint=0,tstr=Map.empty,cstr=0}

makeTemps :: ASTNode -> ExpressionStore -> ExpressionStore
makeTemps ast estore = 
  case ast of
    ASTProgram p -> let ProgramExp cs ss _ = p in
       makeTemps (ASTStatements ss) estore

    ASTStatements statements -> mST (reverse statements) estore

    ASTStatement s -> 
      case s of
        BareExp b  l -> let RightExp r _ = b in makeTemps (ASTRightExp r) estore
        _ -> estore--(ASTStatement s,estore)

    ASTRightExp r -> 
      case r of
        IntExp i l -> let c = cint estore
                          (tint',name,c') = case Map.lookup i (tint estore) of
                                          Just temp -> (tint estore,temp,c)
                                          Nothing -> (Map.insert  i ("tint"++(show c)) (tint estore), ("tint"++(show c)), c+1)
                      in estore{tint=tint',cint=c'}--(ASTRightExp (LeftExp (IdRef name l) l),estore{tint=tint',cint=c'})
        StrExp i l -> let c = cstr estore 
                          (tstr',name, c') = case Map.lookup i (tstr estore) of
                                          Just temp -> (tstr estore,temp,c)
                                          Nothing -> (Map.insert  i ("tstr"++(show c)) (tstr estore),("tstr"++(show c)),c+1)
                      in estore{tstr=tstr',cstr=c'}--(ASTRightExp (LeftExp (IdRef name l) l),estore{tstr=tstr',cstr=c'})
        BoolExp b l -> estore
        BinaryExp _ a b _ -> let astore = makeTemps (ASTRightExp a) estore
                                 bstore = makeTemps (ASTRightExp b) astore in
                             bstore

        _ -> estore


rST statements store = 
    case statements of
      [] -> []
      (s:ss) -> let ASTStatement s' = replaceTemps (ASTStatement s) store
                 in s':(rST ss store)

replaceTemps :: ASTNode -> ExpressionStore -> ASTNode
replaceTemps ast store = 
  case ast of
    ASTProgram p -> let ProgramExp cs ss l = p 
                        ASTStatements ss' = replaceTemps (ASTStatements ss) store
                    in ASTProgram (ProgramExp cs ss' l)

    ASTStatements statements -> ASTStatements $ rST (reverse statements) store

    ASTStatement s -> 
      case s of
        BareExp b  l -> let RightExp r ll = b 
                            ASTRightExp r' =  replaceTemps (ASTRightExp r) store
                        in ASTStatement (BareExp (RightExp r' ll) l) 
        _ -> ASTStatement s

    ASTRightExp r -> 
      case r of
        IntExp i l -> let ints = (tint store)
                      in case Map.lookup i ints of
                          Just id -> ASTRightExp (LeftExp (IdRef id l) l)
                          Nothing -> ASTRightExp r --TODO: Error in this case?
        StrExp i l -> let strs = (tstr store)
                      in case Map.lookup i strs of
                          Just id -> ASTRightExp (LeftExp (IdRef id l) l)
                          Nothing -> ASTRightExp r--(ASTRightExp (LeftExp (IdRef name l) l),estore{tstr=tstr',cstr=c'})
        BinaryExp op a b l -> let ASTRightExp a' = replaceTemps (ASTRightExp a) store
                                  ASTRightExp b' = replaceTemps (ASTRightExp b) store 
                              in
                                  ASTRightExp (BinaryExp op a' b' l)    

        _ -> ASTRightExp r

intDec i id = printf "    obj_Int %s;\n    %s = int_literal(%s);\n" id id (show i)
strDec s id = printf "    obj_String %s;\n    %s = str_literal(%s);\n" id id (show s)

declareTemps :: ExpressionStore -> String
declareTemps store = let ints = Map.toList . tint $ store
                         strs = Map.toList . tstr $ store
                         intDecs = foldl (++) "" . map (\(i,id) -> intDec i id) $ ints
                         strDecs = foldl (++) "" . map  (\(s,id) -> strDec s id) $ strs
                     in  intDecs ++ "\n" ++ strDecs
                     
