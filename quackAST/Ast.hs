module QuackAST (...) where

{- AST Definition -}
type QStr  = String 
type QId = String
type QInt  = Int   
type ActualArgs = [RightExp]
type Statements = [Statement]
type CondPair = (RightExp,StatementBlock)
type Methods = [Method]
type FormalArgs = [FormalArg]
type Classes = [ClassExp]

data AstNode = ASTProgram Program
             | ASTClassExp ClassExp
             | ASTClassSig ClassSig
             | ASTClassBody ClassBody
             | ASTFormalArg FormalArg
             | ASTMethod Method
             | ASTStatement Statement
             | ASTStatementBlock StatementBlock
             | ASTWhileExp WhileExp
             | ASTIfExp IfExp
             | ASTReturnExp ReturnExp
             | ASTAssignmentExp AssignmentExp
             | ASTBareExp BareExp
             | ASTLeftExp LeftExp
             | ASTRightExp RightExp
             | ASTActualArgs ActualArgs
             | ASTFormalArgs FormalArgs
             | ASTStatements Statements
             | ASTMethods Methods
             | ASTClasses Classes

--getAstNode n ast = case 

data Program = ProgramExp Classes Statements
             | ForPrinting deriving Show
      
data ClassExp = ClassDecExp ClassSig ClassBody deriving Show

data ClassSig = ClassSigExp QId FormalArgs
              | ExtendedClassSigExp QId FormalArgs QId
              deriving Show

data ClassBody = ClassBodyExp Statements Methods deriving Show

data FormalArg = FormalArgExp QId QId deriving Show
    
data Method = MethodExp QId FormalArgs StatementBlock
            | TypedMethodExp QId FormalArgs QId StatementBlock 
            deriving Show

data Statement  
             = IfExp IfExp
             | WhileExp WhileExp
             | ReturnExp ReturnExp
             | AssignmentExp AssignmentExp
             | BareExp BareExp
             deriving Show

data StatementBlock = StatementBlockExp Statements deriving Show

data WhileExp = WhileLoop CondPair deriving Show

data IfExp = OnlyIf CondPair
           | IfElif CondPair CondPair 
           | IfElse CondPair StatementBlock
           | IfElifElse CondPair CondPair StatementBlock
           deriving Show

data ReturnExp = NoReturnExp | RetExp RightExp deriving Show

data AssignmentExp = UntypedAssignExp LeftExp RightExp
                   | TypedAssignExp LeftExp QStr RightExp
                   deriving Show

data BareExp = RightExp RightExp deriving Show

--Left Expressions
data LeftExp = IdRef QId
          | ObjIdRef RightExp QId
          deriving Show

--Right Expressions
data RightExp
        = StrExp QStr 
        | IntExp QInt
        | BinaryExp String RightExp RightExp
        | NotExp RightExp
        | ParExp RightExp
        | ObjFuncCallExp RightExp QId ActualArgs
        | FuncCallExp QId ActualArgs 
        | LeftExp LeftExp
        deriving Show



-- AST STUFF
type QClass = (String,String)

{-
getConstructorCalls :: Program -> [String]
getConstructorCalls p = let ProgramExp cls stmts = p in
                          map (\cd -> let -}

gc :: ClassSig -> QClass
gc cl = case cl of 
                  ClassSigExp cn _ -> (cn,"")
                  ExtendedClassSigExp cn _ ec -> (cn, ec)

getClasses :: Program -> [QClass]
getClasses p = let ProgramExp cls stmts = p in
                    map (\cd -> let ClassDecExp s b = cd in gc s) cls  

objExtends :: [QClass] -> [QClass]
objExtends cl = map (\(c,e) -> if e == "" then (c,"Obj") else (c,e)) cl



getPath :: QClass -> [QClass] -> Int
getPath (a,b) xs = let cls = filter (\(c,d) -> c==b) xs in 
                      case cls of
                        [] -> if b == "Obj" then 1 else 0
                        (x':xs') -> case xs' of
                                      [] -> getPath x' (filter (\x -> inList x cls) xs)
                                      _ -> 0


checkClasses :: [QClass] -> Bool
checkClasses cls = any (\i -> i == 0) (map (\c -> getPath c (cls ++ [("Int","Obj"),("String","Obj"),("Nothing","Obj")] ) ) cls)

badClasses cls = let baseClasses = [("Int","Obj"),("String","Obj"),("Nothing","Obj")]
                     check = \c -> (getPath c (cls ++ baseClasses)) == 0 
                 in
                  filter check cls


inList c cs = any (\x -> x == c) cs
