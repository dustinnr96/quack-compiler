module QuackAST (module QuackAST) where

import Data.List

import qualified Data.Map.Strict as Map

type QStr  = String 
type QId = String
type QInt  = Int   
type ActualArgs =  [RightExp]
type Statements = [Statement]
type CondPair = (RightExp,StatementBlock)
type Elifs = [ElifExp]
type Methods = [Method]
type FormalArgs = [FormalArg]
type Classes = [ClassExp]

data ASTNode = ASTProgram Program 
             | ASTClassExp ClassExp 
             | ASTClassSig ClassSig 
             | ASTClassBody ClassBody 
             | ASTFormalArg FormalArg 
             | ASTMethod Method 
             | ASTStatement Statement 
             | ASTStatementBlock StatementBlock 
             | ASTWhileExp WhileExp 
             | ASTIfExp IfExp 
             | ASTElifExp ElifExp 
             | ASTElifs Elifs 
             | ASTElseExp ElseExp
             | ASTReturnExp ReturnExp 
             | ASTAssignmentExp AssignmentExp 
             | ASTBareExp BareExp 
             | ASTLeftExp LeftExp 
             | ASTRightExp RightExp 
             | ASTActualArgs ActualArgs 
             | ASTFormalArgs FormalArgs 
             | ASTStatements Statements 
             | ASTMethods Methods 
             | ASTClasses Classes 
             | ASTCondPair CondPair 
               deriving Show
                              


{- Might want to make these record types -}                      
data Program = ProgramExp Classes Statements Int
             | ForPrinting deriving Show
      
data ClassExp = ClassDecExp ClassSig ClassBody Int deriving Show

data ClassSig = ClassSigExp QId FormalArgs Int
              | ExtendedClassSigExp QId FormalArgs QId Int
              deriving Show

data ClassBody = ClassBodyExp Statements Methods Int deriving Show

data FormalArg = FormalArgExp QId QId Int deriving Show
    
data Method = MethodExp QId FormalArgs StatementBlock Int
            | TypedMethodExp QId FormalArgs QId StatementBlock  Int
            deriving Show

data Statement  
             = IfExp IfExp Int
             | WhileExp WhileExp Int
             | ReturnExp ReturnExp Int
             | AssignmentExp AssignmentExp Int
             | BareExp BareExp Int 
             deriving Show

data StatementBlock = StatementBlockExp Statements Int deriving Show

data WhileExp = WhileLoop CondPair Int deriving Show

data IfExp = IfBlockExp RightExp StatementBlock Elifs ElseExp Int deriving Show

data ElifExp = ElifBlockExp RightExp StatementBlock Int deriving Show

data ElseExp = ElseBlockExp StatementBlock Int deriving Show


data ReturnExp = NoReturnExp Int | RetExp RightExp Int deriving Show

data AssignmentExp = UntypedAssignExp LeftExp RightExp Int
                   | TypedAssignExp LeftExp QStr RightExp Int
                   deriving Show

data BareExp = RightExp RightExp Int deriving Show

--Left Expressions
data LeftExp = IdRef QId Int
          | ObjIdRef RightExp QId Int
          deriving Show

data RightExp
        = StrExp QStr  Int
        | IntExp QInt Int
        | BinaryExp String RightExp RightExp Int
        | NotExp RightExp Int
        | ParExp RightExp Int
        | FuncCallExp RightExp QId ActualArgs Int
        | ConstructorExp QId ActualArgs  Int
        | LeftExp LeftExp Int
        | NullExp String Int
        deriving Show

-- AST 'getter' functions
getRightExp :: ASTNode -> RightExp
--getRightExp e = let ASTRightExp e' _ = e in e'
getRightExp e = case e of
                  ASTRightExp e' ->  e'
                  ASTLeftExp l' -> case l' of 
                                       IdRef i l -> NullExp "String idRef" l
                                       ObjIdRef r i l -> r

getLeftExp :: ASTNode -> LeftExp
getLeftExp e = let ASTLeftExp e' = e in e'

-- AST STUFF
data SymTable = SymTable { classes :: Map.Map String (String,Int) } deriving Show

--type QClass = (QId,(QId,Int))

extractClasses :: ASTNode -> [(QId,(QId,Int))]
extractClasses ast = case ast of
                    ASTProgram prog -> let ProgramExp cs ss l = prog in extractClasses (ASTClasses cs)
                    ASTClasses cs -> concat $ map (extractClasses . ASTClassExp) (reverse cs)
                    ASTClassExp cls -> let ClassDecExp sig body l = cls in extractClasses (ASTClassSig sig)
                    ASTClassSig sig -> case sig of
                                        ClassSigExp id args l -> [(id,("Obj",l))]
                                        ExtendedClassSigExp id args ext l ->   [(id,(ext,l)) ]
                    _ -> []
              
defaultClasses = [("Obj", ("Obj", 0)),("Boolean", ("Obj",0)),("Int",("Obj",0)),("String",("Obj",0)),("Nothing",("Obj",0))]

classSymbols ast = SymTable { classes = Map.fromList $  (defaultClasses ++ (extractClasses ast)) } 

emptyEnv = Map.empty



-- (Type,Line,Env)
--data QClass = QClass { name :: QId, super :: QId, args :: FormalArgs, constructor :: Statements, methods :: Methods, line :: Int }

{-
extractClasses2 :: ASTNode -> [QClass]
extractClasses2 ast = case ast of
                    ASTProgram prog -> let ProgramExp cs ss l = prog in extractClasses (ASTClasses cs)
                    ASTClasses cs -> concat $ map (extractClasses . ASTClassExp) (reverse cs)
                    ASTClassExp cls -> let ClassDecExp sig body l = cls in extractClasses (ASTClassSig sig)
                    ASTClassSig sig -> case sig of
                                        ClassSigExp id args l -> [(id,("Obj",l))]
                                        ExtendedClassSigExp id args ext l ->   [(id,(ext,l)) ]

-}

extractConstructors :: ASTNode -> [(String,Int)]
extractConstructors ast = case ast of
                    ASTProgram prog -> let ProgramExp cs ss _ = prog in concat [(extractConstructors $ ASTClasses cs),(extractConstructors $ ASTStatements ss)]
                    ASTClasses cs -> concat $ map (extractConstructors . ASTClassExp) (reverse cs)
                    ASTClassExp cls -> let ClassDecExp _ body _ = cls in extractConstructors (ASTClassBody body)
                    ASTClassBody body -> let ClassBodyExp ss ms _ = body in (extractConstructors $ ASTStatements ss) ++  (extractConstructors $ ASTMethods ms)
                    ASTMethods ms -> concat $ map (extractConstructors . ASTMethod) (reverse ms)
                    ASTMethod method -> case method of
                                              MethodExp _ _ body _ -> extractConstructors $ ASTStatementBlock body
                                              TypedMethodExp _ _ _ body _ -> extractConstructors $ ASTStatementBlock body
                    ASTStatementBlock sb -> let StatementBlockExp ss _ = sb in extractConstructors $ ASTStatements ss
                    ASTStatements ss -> concat $ map (extractConstructors . ASTStatement) (reverse ss)
                    ASTStatement s -> case s of
                                        IfExp i _ -> extractConstructors $ ASTIfExp i 
                                        WhileExp w _ -> extractConstructors $ ASTWhileExp w
                                        ReturnExp r _ -> extractConstructors $ ASTReturnExp r
                                        AssignmentExp a _ -> extractConstructors $ ASTAssignmentExp a
                                        BareExp b _ -> extractConstructors $ ASTBareExp b
                    ASTIfExp i -> let IfBlockExp cond body elifs els _ = i in
                                    concat [(extractConstructors $ ASTRightExp cond),(extractConstructors $ ASTStatementBlock body),
                                            (extractConstructors $ ASTElifs elifs),(extractConstructors $ ASTElseExp els)]
                    ASTElifs elifs -> concat $ map (extractConstructors . ASTElifExp) (reverse elifs)
                    ASTElifExp elif -> let ElifBlockExp cond body _ = elif in
                                        concat [(extractConstructors $ ASTRightExp cond),(extractConstructors $ ASTStatementBlock body)]
                    ASTElseExp els -> let ElseBlockExp body _ = els in extractConstructors $ ASTStatementBlock body
                    ASTWhileExp while -> let WhileLoop (cond,body) _ = while in 
                                        concat [(extractConstructors $ ASTRightExp cond),(extractConstructors $ ASTStatementBlock body)]
                    ASTReturnExp ret -> case ret of
                                           NoReturnExp _ -> []
                                           RetExp r _ -> extractConstructors $ ASTRightExp r
                    ASTAssignmentExp a -> case a of
                                            UntypedAssignExp l r _ -> concat [(extractConstructors $ ASTLeftExp l),(extractConstructors $ ASTRightExp r)]
                                            TypedAssignExp l _ r _ -> concat [(extractConstructors $ ASTLeftExp l),(extractConstructors $ ASTRightExp r)] 
                    ASTBareExp b -> case b of RightExp r _ ->  extractConstructors $ ASTRightExp r
                    ASTLeftExp l -> case l of
                                      IdRef _ _ -> []
                                      ObjIdRef r _ _ -> extractConstructors $ ASTRightExp r
                    ASTRightExp r -> case r of
                                       StrExp _ _ -> []
                                       IntExp _ _ -> []
                                       BinaryExp _ r1 r2 _ -> concat [(extractConstructors $ ASTRightExp r1),(extractConstructors $ ASTRightExp r2)]
                                       NotExp r _ -> extractConstructors $ ASTRightExp r
                                       ParExp r _ -> extractConstructors $ ASTRightExp r
                                       FuncCallExp r _ args _ -> concat [(extractConstructors $ ASTRightExp r),(extractConstructors $ ASTActualArgs args)]
                                       ConstructorExp id args line -> [(id,line)] ++ (extractConstructors $ ASTActualArgs args)
                                       LeftExp l _ -> extractConstructors $ ASTLeftExp l
                                       NullExp _ _ -> []
                    ASTActualArgs args -> concat $ map (extractConstructors . ASTRightExp) (reverse args)
                    _ -> []

-- Class Checks
badConstructors ast = let cs =  classes $ classSymbols ast in filter (\(c,l) -> c `Map.notMember` cs) (extractConstructors ast)

data ClassInherErr = Cycle | DeadEnd QId | NoErr deriving Show

--classCycles :: QId -> Map.Map String (String,Int) -> [QId] ->  (Bool,[QId])
classCycles c cs classes path =  case Map.lookup c cs of
                              Just ("Obj",line) -> (NoErr,path)
                              Just (ext,line) -> classCycles ext (Map.delete c cs) classes (path ++ [c])
                              Nothing -> case Map.lookup c classes of
                                          Just (e,l) -> (Cycle,path)
                                          Nothing -> (DeadEnd c,path)

classInheritances :: ASTNode -> [(String, (Int,(ClassInherErr,[QId]) ) )]
classInheritances ast = let cs = classes $ classSymbols ast in
                         Map.toList $ Map.mapWithKey (\k (c,l) -> (l, classCycles c (classes $ classSymbols ast) cs []) ) cs




-- Type Checking


classScope = "_class_"
globalScope = "_global_"


--type QType = Maybe QId

--             (name, type,       cond,             line)
--type QSymbol = (QId,  Maybe QId,  [RightExp], Int)
--type  QClass = QId

data QMethod = QMethod {mname::QId,syms::(Map.Map QId QField),mline::Int,mtype::TypeCheck,mclass::QId} deriving Show
data QField = QField {fname::QId,fline::Int,ftype::TypeCheck,fexp::Maybe RightExp} deriving Show
data QClass = QClass {cname::QId,csuper::QId,cmethods::(Map.Map QId QMethod),cfields::(Map.Map QId QField),cconstructor::QMethod,cline::Int} deriving Show
--data SymbolTable = SymbolTable {quackClasses :: [QClass], quackFields :: [QField]}

getName :: LeftExp -> QId
getName (IdRef name line) = name
getName  (ObjIdRef r name line) = name


--data QValue a = Val a | None deriving Show



data TypeCheck = Untyped | Type QId | TypeError String deriving (Eq,Show)

getType :: RightExp -> TypeCheck
getType (StrExp s l) = Type "String"
getType (IntExp s l) = Type "Int"
getType (BinaryExp op a b l) = case (getType a, getType b) of
                                        (TypeError e,TypeError e2) -> TypeError $ (e ++ "\n" ++ e2)
                                        (TypeError e, _) -> TypeError e
                                        (_,TypeError e) -> TypeError e
                                        (a',b') -> if a' == b' then a' else TypeError $ "Type mismatch at line " ++ (show l)
getType (NotExp n l) = getType n
getType (ParExp p l) = getType p
getType (FuncCallExp r method args l) = Untyped-- let cls = getType r in
                                           {-case lookup method in class cls -}     
getType (ConstructorExp cls _ _) = Type cls
getType (LeftExp lexp l) = case lexp of
                             IdRef name l -> Untyped --{- case lookup name in the environment -}
                             ObjIdRef r name l -> let cls = getType r in 
                                                    Untyped{-case lookup method in class cls -}

_getFieldDeclarations :: Statements -> [Maybe QField]
_getFieldDeclarations ss = concat $ map gqf  ss where
                              gqf s = case s of
                                  AssignmentExp a l -> case a of
                                                        UntypedAssignExp (IdRef  name _) r line -> [Just QField {fname=name,fline=line,ftype=Untyped,fexp=Just r}]
                                                        TypedAssignExp (IdRef name _) typ r line -> [Just QField {fname=name,fline=line,ftype=Type typ,fexp=Just r}]
                                                        _ -> []

                                  WhileExp w l -> let WhileLoop (cond,StatementBlockExp ss _) line = w in
                                                      _getFieldDeclarations ss

                                  IfExp i l -> let IfBlockExp cond (StatementBlockExp ss _) elifs els line = i 
                                                   gqfElif e = let ElifBlockExp cond (StatementBlockExp elifst _) _ = e in
                                                                _getFieldDeclarations elifst
                                                   elifFields = concat $ map gqfElif elifs
                                                   ElseBlockExp (StatementBlockExp elss _) _ = els 
                                               in
                                                   foldl (++) [] [_getFieldDeclarations ss, elifFields, _getFieldDeclarations elss]
                                  _ -> [Nothing] 

getFieldDeclarations :: ASTNode ->  [QField]
getFieldDeclarations ast = let filterNothings c = case c of
                                                      Just c' -> True
                                                      Nothing -> False
                               mapQFields c = let Just c' = c in c'
                           in case ast of
                              ASTProgram prog -> let ProgramExp cs ss l = prog in 
                                                    map mapQFields . filter filterNothings . _getFieldDeclarations $ ss
                              ASTStatementBlock sb -> let StatementBlockExp bodyStmts l = sb in
                                                        map mapQFields . filter filterNothings .  _getFieldDeclarations $ bodyStmts
                              _ -> []

qFieldsToMap :: [QField] -> Map.Map QId QField
qFieldsToMap qs = Map.fromList . map (\q -> (fname q, q)) $ qs

qMethodsToMap ms = Map.fromList . map (\m -> (mname m, m)) $ ms

getArgs :: FormalArgs -> [QField]
getArgs args = case args of
                      [] -> []
                      (a:as) -> let FormalArgExp name typ l = a in
                                 (QField {fname=name,fline=l,ftype=Type typ, fexp=Nothing}):(getArgs as)


getQMethod :: QId -> Method  -> QMethod
--getQMethod (MethodExp name args body line) =  QMethod {mName=name,syms=symbols (ASTStatementBlock body),mLine=line,mtype=TypeError "No untyped methods!"}
getQMethod cls (TypedMethodExp name args typ body line) = let argSyms = getArgs args
                                                              bodySyms = getFieldDeclarations . ASTStatementBlock $ body
                                                          in
                                                              QMethod {mname=name,syms=qFieldsToMap $ argSyms++bodySyms,mline=line,mtype=Type typ,mclass=cls}  



_getClassFieldDeclarations :: Statements -> [QField]
_getClassFieldDeclarations ss = concat $ map gqf  ss where
    gqf s = case s of
                AssignmentExp _ _ ->  case getThisReference s of
                                        Just (name,typ,line,r) -> [QField {fname=name,fline=line,ftype=typ,fexp=Just r}]
                                        Nothing -> []
                WhileExp w l -> let WhileLoop (cond,StatementBlockExp ss _) line = w in
                                    _getClassFieldDeclarations ss
                IfExp i l -> let IfBlockExp cond (StatementBlockExp ss _) elifs els line = i 
                                 gqfElif e = let ElifBlockExp cond (StatementBlockExp elifst _) _ = e in
                                              _getClassFieldDeclarations elifst
                                 elifFields = concat $ map gqfElif elifs
                                 ElseBlockExp (StatementBlockExp elss _) _ = els 
                             in
                                 foldl (++) [] [_getClassFieldDeclarations ss, elifFields, _getClassFieldDeclarations elss]
                _ -> [] 


{-
getReference (Assignment Exp a l) = case a of
                                      UntypedAssignExp l r _ -> case (l,r) of
                                                                   (IdRef name line, _) -> (name,Untyped,Untyped,line,r)
                                                                   (ObjIdRef obj name line,_) -> (name,getType obj,Untyped,line,r)
                                      TypedAssignExp l typ r _ -> case (l,r) of
                                                                   (IdRef name line,_) -> (name,Untyped,Untyped,line,r)
                                                                   (ObjIdRef obj name line,_) -> (name,getType obj,Type typ,line,r)
-}
getThisReference (AssignmentExp a l) = case a of
                                      UntypedAssignExp l r _ -> case (l,r) of
                                                                   (ObjIdRef (LeftExp (IdRef "this" _)_) name line,_) -> Just  (name,Untyped,line,r)
                                                                   _ -> Nothing
                                      TypedAssignExp l typ r _ -> case (l,r) of
                                                                   (ObjIdRef (LeftExp (IdRef "this" _)_) name line,_) -> Just  (name,Type typ,line,r)
                                                                   _ -> Nothing

{- AssignmentExp   (UntypedAssignExp     (ObjIdRef (LeftExp (IdRef "this" 3) 3) "ll" 3)      (LeftExp (IdRef "ll" 3) 3) 4)              4] [] 6) 6] -}

getClassFieldDeclarations ast = let filterNothings c = case c of
                                                      Just c' -> True
                                                      Nothing -> False
                                    mapQFields c = let Just c' = c in c'
                                in case ast of
                                  ASTStatementBlock sb -> let StatementBlockExp bodyStmts l = sb in
                                                            _getClassFieldDeclarations bodyStmts
                                  _ -> []



getClassSig :: ClassSig -> (QId,QId,FormalArgs,Int)
getClassSig s = case s of
                  ClassSigExp id args l -> (id,"Obj",args,l)
                  ExtendedClassSigExp id args ext l -> (id,ext,args,l)                 

extractClasses2 :: ASTNode -> [QClass]
extractClasses2 ast = case ast of
                    ASTProgram prog -> let ProgramExp cs ss l = prog in extractClasses2 (ASTClasses cs)
                    ASTClasses cs -> concat $ map (extractClasses2 . ASTClassExp) (reverse cs)

                    ASTClassExp cls -> let ClassDecExp sig classbody l = cls 
                                           ClassBodyExp body methods lb = classbody
                                           (name,super,args,line) = getClassSig sig
                                           argSyms = getArgs args
                                           consLocalFields = getFieldDeclarations . ASTStatementBlock $ StatementBlockExp body lb
                                           classFields = getClassFieldDeclarations . ASTStatementBlock $ StatementBlockExp body lb
                                           constructorMethod = QMethod {mname=name,syms=qFieldsToMap $ argSyms++consLocalFields,mline=line,mtype=Type name,mclass=name}
                                           ms = qMethodsToMap . map (getQMethod name) $ methods
                                       in
                                           [QClass {cname=name,csuper=super,cfields=qFieldsToMap classFields,
                                                    cline=l,cconstructor=constructorMethod,cmethods= ms  }]                       
    {-
in extractClasses (ASTClassSig sig)
                    ASTClassSig sig -> case sig of
                                        ClassSigExp id args l -> [(id,("Obj",l))]
                                        ExtendedClassSigExp id args ext l ->   [(id,(ext,l)) ]-}
{-                                           
                        WhileLoop (cond,StatementBlockExp ss _) line -> getQFields ss
                        
                        IfBlockExp cond body elifs els line -> let StatementBlockExp ss _  = body
                                                                   ElseBlockExp elifCond (StatementBlockExp elsess _) _ = elifs           
-}



{-
getVal :: RightExp -> QValue a
getVal (StrExp s l) = Val s
getVal (IntExp i l) = Val i
getVal _ = None
-}



symbols ast = case ast of
                  ASTFormalArgs args -> map (\arg -> let FormalArgExp n t l = arg in (n,Just t, [],l)) args
                  ASTStatementBlock sb -> let StatementBlockExp ss line = sb in symbols (ASTStatements ss) 
                  ASTStatements ss -> concat $ map (\s -> symbols (ASTStatement s))  ss
                  ASTStatement s -> case s of
                                      IfExp i l -> symbols $ ASTIfExp i
                                      WhileExp w l -> symbols $ ASTWhileExp w   
                                      ReturnExp r l -> symbols $ ASTReturnExp r
                                      BareExp b l -> symbols $ ASTBareExp b
                                      AssignmentExp a l -> symbols $ ASTAssignmentExp a
                  ASTIfExp i -> let IfBlockExp cond body elifs els line = i 
                                    elifSymbols = symbols (ASTElifs elifs)
                                    elsSymbols = symbols (ASTElseExp els)
                                in  (symbols $ ASTCondPair (cond,body)) ++ elifSymbols ++ elsSymbols
                  ASTElifs elifs -> concat $ map (\e -> symbols (ASTElifExp e))  elifs
                  ASTElifExp e -> let ElifBlockExp cond body line = e
                                  in symbols $ ASTCondPair (cond,body)
                  ASTElseExp e -> let ElseBlockExp body line = e
                                  in symbols . ASTStatementBlock $ body 
                  ASTWhileExp w -> let WhileLoop (cond,body) line = w in symbols $ ASTCondPair (cond,body)
                  ASTAssignmentExp a -> case a of
                                          UntypedAssignExp l r line -> [(getName l,Nothing,[],line)]
                                          TypedAssignExp l t r line -> [(getName l,Just t,[],line)]

                  ASTCondPair (cond,body) -> map (\(n,t,cs,l) -> (n,t,cond:cs,l))  (symbols (ASTStatementBlock body))
                  --no ASTReturn, BareExp,ASTRightExp
                  _ -> []


{-

-}

{-
getQField :: AssignmentExp -> Maybe QField
getQField (UntypedAssignExp l _ line) = case l of
                                          ObjIdRef r name _ = if 
getQField (TypedAssignExp l t r line) = 




getQFields :: Statements -> [QField]
getQFields ss = map (\(Just f) -> f) . filter (\f -> f /= Nothing) .  map (\s -> gqf s) $ ss where
                    gqf s = case s of
                        UntypedAssignExp (ObjIdRef obj name) r line -> [case getValue obj of
                                                                        Val "this" -> Just $ QField {fname::name,fline::line,ftype::Nothing}
                                                                        _ ->  Nothing]
                        TypedAssignExp (ObjIdRef obj name) typ _ line -> [case getValue obj of 
                                                                          Val "this" -> QField {fname::name,fline::line,ftype::typ}
                                                                          _ -> Nothing]
                        WhileLoop (cond,StatementBlockExp ss _) line -> getQFields ss
                        
                        IfBlockExp cond body elifs els line -> let StatementBlockExp ss _  = body
                                                                   ElseBlockExp elifCond (StatementBlockExp elsess _) _ = elifs                
  getQFields ss

-}

{-
getQClass :: ClassExp -> QClass
getQClass c = let ClassDecExp sig body line = c
                  (className,superClass,args) = case sig of
                                                  ClassSigExp name args' _ -> (name,"Obj",args')
                                                  ExtendedClassSigExp name args' super _ -> (name,super,args')
                  ClassBodyExp consBody methods _ = body
                  consSyms = (symbols $ ASTFormalArgs args) ++ (symbols $ ASTStatements consBody)
               --  fields = Map.fromList ()
                  methods' = Map.fromList  (map (\m -> let m' = getQMethod m in (mName m', m')) methods)
                  constructor = QMethod {mName=className,syms=consSyms,mtype=Just className,mLine=line}
              in
                QClass {cName=className,super=superClass,methods=methods',constructor=constructor,line=line}



getClasses :: ASTNode -> [QClass]
getClasses ast = case ast of
                    ASTProgram prog -> let ProgramExp cs ss l = prog in getClasses . ASTClasses $ cs
                    ASTClasses cs -> map getQClass . reverse $ cs
                    _ -> []

-}

{-
getQClass :: ClassExp -> QClass
getQClass c = let ClassDecExp sig body line = c in
                  let  cName = (name sig)
                       sName = (super sig)
                       cArgs = symbols (args sig)
                       constructorSyms = symbols (constructor body)
                       cMethods = methods body
                       methodSyms = symbols cMethods 
                  in
                      {name=cName, super=sName, constructor=...


getQClass c = let ClassDecExp sig body line = c in
                  case sig of
                    ClassSigExp name args line -> 
                          let cArgs = symbols args 
                              sName = "" 
                          in
                              let ClassBodyExp ss ms = body
                                  constructor = MethodExp "constructor" args ss line
                                  constructory
                                      
      

-}


