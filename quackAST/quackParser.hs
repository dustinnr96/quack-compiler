{-# OPTIONS_GHC -w #-}
module Main where
import Data.List
import qualified Data.Map.Strict as Map

import QuackLexer
import QuackAST
import Control.Applicative(Applicative(..))
import Control.Monad (ap)

-- parser produced by Happy Version 1.19.5

data HappyAbsSyn t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16 t17 t18 t19 t20 t21 t22 t23 t24 t25
	= HappyTerminal (Token)
	| HappyErrorToken Int
	| HappyAbsSyn4 t4
	| HappyAbsSyn5 t5
	| HappyAbsSyn6 t6
	| HappyAbsSyn7 t7
	| HappyAbsSyn8 t8
	| HappyAbsSyn9 t9
	| HappyAbsSyn10 t10
	| HappyAbsSyn11 t11
	| HappyAbsSyn12 t12
	| HappyAbsSyn13 t13
	| HappyAbsSyn14 t14
	| HappyAbsSyn15 t15
	| HappyAbsSyn16 t16
	| HappyAbsSyn17 t17
	| HappyAbsSyn18 t18
	| HappyAbsSyn19 t19
	| HappyAbsSyn20 t20
	| HappyAbsSyn21 t21
	| HappyAbsSyn22 t22
	| HappyAbsSyn23 t23
	| HappyAbsSyn24 t24
	| HappyAbsSyn25 t25

action_0 (26) = happyShift action_5
action_0 (29) = happyShift action_17
action_0 (32) = happyShift action_18
action_0 (33) = happyShift action_19
action_0 (46) = happyShift action_20
action_0 (49) = happyShift action_21
action_0 (55) = happyShift action_22
action_0 (56) = happyShift action_23
action_0 (57) = happyShift action_24
action_0 (4) = happyGoto action_6
action_0 (5) = happyGoto action_2
action_0 (6) = happyGoto action_7
action_0 (7) = happyGoto action_4
action_0 (12) = happyGoto action_8
action_0 (13) = happyGoto action_9
action_0 (15) = happyGoto action_10
action_0 (16) = happyGoto action_11
action_0 (20) = happyGoto action_12
action_0 (21) = happyGoto action_13
action_0 (22) = happyGoto action_14
action_0 (23) = happyGoto action_15
action_0 (24) = happyGoto action_16
action_0 _ = happyReduce_4

action_1 (26) = happyShift action_5
action_1 (5) = happyGoto action_2
action_1 (6) = happyGoto action_3
action_1 (7) = happyGoto action_4
action_1 _ = happyFail

action_2 _ = happyReduce_6

action_3 (26) = happyShift action_5
action_3 (29) = happyShift action_17
action_3 (32) = happyShift action_18
action_3 (33) = happyShift action_19
action_3 (46) = happyShift action_20
action_3 (49) = happyShift action_21
action_3 (55) = happyShift action_22
action_3 (56) = happyShift action_23
action_3 (57) = happyShift action_24
action_3 (5) = happyGoto action_49
action_3 (7) = happyGoto action_4
action_3 (12) = happyGoto action_8
action_3 (13) = happyGoto action_50
action_3 (15) = happyGoto action_10
action_3 (16) = happyGoto action_11
action_3 (20) = happyGoto action_12
action_3 (21) = happyGoto action_13
action_3 (22) = happyGoto action_14
action_3 (23) = happyGoto action_15
action_3 (24) = happyGoto action_16
action_3 _ = happyFail

action_4 (47) = happyShift action_53
action_4 (9) = happyGoto action_52
action_4 _ = happyFail

action_5 (55) = happyShift action_51
action_5 _ = happyFail

action_6 (58) = happyAccept
action_6 _ = happyFail

action_7 (26) = happyShift action_5
action_7 (29) = happyShift action_17
action_7 (32) = happyShift action_18
action_7 (33) = happyShift action_19
action_7 (46) = happyShift action_20
action_7 (49) = happyShift action_21
action_7 (55) = happyShift action_22
action_7 (56) = happyShift action_23
action_7 (57) = happyShift action_24
action_7 (5) = happyGoto action_49
action_7 (7) = happyGoto action_4
action_7 (12) = happyGoto action_8
action_7 (13) = happyGoto action_50
action_7 (15) = happyGoto action_10
action_7 (16) = happyGoto action_11
action_7 (20) = happyGoto action_12
action_7 (21) = happyGoto action_13
action_7 (22) = happyGoto action_14
action_7 (23) = happyGoto action_15
action_7 (24) = happyGoto action_16
action_7 _ = happyReduce_2

action_8 _ = happyReduce_26

action_9 (29) = happyShift action_17
action_9 (32) = happyShift action_18
action_9 (33) = happyShift action_19
action_9 (46) = happyShift action_20
action_9 (49) = happyShift action_21
action_9 (55) = happyShift action_22
action_9 (56) = happyShift action_23
action_9 (57) = happyShift action_24
action_9 (12) = happyGoto action_48
action_9 (15) = happyGoto action_10
action_9 (16) = happyGoto action_11
action_9 (20) = happyGoto action_12
action_9 (21) = happyGoto action_13
action_9 (22) = happyGoto action_14
action_9 (23) = happyGoto action_15
action_9 (24) = happyGoto action_16
action_9 _ = happyReduce_3

action_10 _ = happyReduce_22

action_11 _ = happyReduce_21

action_12 _ = happyReduce_23

action_13 _ = happyReduce_24

action_14 (38) = happyShift action_46
action_14 (54) = happyShift action_47
action_14 _ = happyReduce_47

action_15 _ = happyReduce_25

action_16 (34) = happyShift action_33
action_16 (35) = happyShift action_34
action_16 (36) = happyShift action_35
action_16 (37) = happyShift action_36
action_16 (39) = happyShift action_37
action_16 (40) = happyShift action_38
action_16 (41) = happyShift action_39
action_16 (42) = happyShift action_40
action_16 (43) = happyShift action_41
action_16 (44) = happyShift action_42
action_16 (45) = happyShift action_43
action_16 (52) = happyShift action_44
action_16 (53) = happyShift action_45
action_16 _ = happyFail

action_17 (46) = happyShift action_20
action_17 (49) = happyShift action_21
action_17 (55) = happyShift action_22
action_17 (56) = happyShift action_23
action_17 (57) = happyShift action_24
action_17 (22) = happyGoto action_26
action_17 (24) = happyGoto action_32
action_17 _ = happyFail

action_18 (46) = happyShift action_20
action_18 (49) = happyShift action_21
action_18 (55) = happyShift action_22
action_18 (56) = happyShift action_23
action_18 (57) = happyShift action_24
action_18 (22) = happyGoto action_26
action_18 (24) = happyGoto action_31
action_18 _ = happyFail

action_19 (46) = happyShift action_20
action_19 (49) = happyShift action_21
action_19 (52) = happyShift action_30
action_19 (55) = happyShift action_22
action_19 (56) = happyShift action_23
action_19 (57) = happyShift action_24
action_19 (22) = happyGoto action_26
action_19 (24) = happyGoto action_29
action_19 _ = happyFail

action_20 (46) = happyShift action_20
action_20 (49) = happyShift action_21
action_20 (55) = happyShift action_22
action_20 (56) = happyShift action_23
action_20 (57) = happyShift action_24
action_20 (22) = happyGoto action_26
action_20 (24) = happyGoto action_28
action_20 _ = happyFail

action_21 (46) = happyShift action_20
action_21 (49) = happyShift action_21
action_21 (55) = happyShift action_22
action_21 (56) = happyShift action_23
action_21 (57) = happyShift action_24
action_21 (22) = happyGoto action_26
action_21 (24) = happyGoto action_27
action_21 _ = happyFail

action_22 (49) = happyShift action_25
action_22 _ = happyReduce_42

action_23 _ = happyReduce_46

action_24 _ = happyReduce_45

action_25 (46) = happyShift action_20
action_25 (49) = happyShift action_21
action_25 (50) = happyShift action_81
action_25 (55) = happyShift action_22
action_25 (56) = happyShift action_23
action_25 (57) = happyShift action_24
action_25 (22) = happyGoto action_26
action_25 (24) = happyGoto action_79
action_25 (25) = happyGoto action_80
action_25 _ = happyFail

action_26 _ = happyReduce_47

action_27 (34) = happyShift action_33
action_27 (35) = happyShift action_34
action_27 (36) = happyShift action_35
action_27 (37) = happyShift action_36
action_27 (39) = happyShift action_37
action_27 (40) = happyShift action_38
action_27 (41) = happyShift action_39
action_27 (42) = happyShift action_40
action_27 (43) = happyShift action_41
action_27 (44) = happyShift action_42
action_27 (45) = happyShift action_43
action_27 (50) = happyShift action_78
action_27 (53) = happyShift action_45
action_27 _ = happyFail

action_28 (34) = happyShift action_33
action_28 (35) = happyShift action_34
action_28 (36) = happyShift action_35
action_28 (37) = happyShift action_36
action_28 (39) = happyShift action_37
action_28 (40) = happyShift action_38
action_28 (41) = happyShift action_39
action_28 (42) = happyShift action_40
action_28 (43) = happyShift action_41
action_28 (44) = happyShift action_42
action_28 (45) = happyShift action_43
action_28 (53) = happyShift action_45
action_28 _ = happyReduce_60

action_29 (34) = happyShift action_33
action_29 (35) = happyShift action_34
action_29 (36) = happyShift action_35
action_29 (37) = happyShift action_36
action_29 (39) = happyShift action_37
action_29 (40) = happyShift action_38
action_29 (41) = happyShift action_39
action_29 (42) = happyShift action_40
action_29 (43) = happyShift action_41
action_29 (44) = happyShift action_42
action_29 (45) = happyShift action_43
action_29 (52) = happyShift action_77
action_29 (53) = happyShift action_45
action_29 _ = happyFail

action_30 _ = happyReduce_38

action_31 (34) = happyShift action_33
action_31 (35) = happyShift action_34
action_31 (36) = happyShift action_35
action_31 (37) = happyShift action_36
action_31 (39) = happyShift action_37
action_31 (40) = happyShift action_38
action_31 (41) = happyShift action_39
action_31 (42) = happyShift action_40
action_31 (43) = happyShift action_41
action_31 (44) = happyShift action_42
action_31 (45) = happyShift action_43
action_31 (47) = happyShift action_75
action_31 (53) = happyShift action_45
action_31 (14) = happyGoto action_76
action_31 _ = happyFail

action_32 (34) = happyShift action_33
action_32 (35) = happyShift action_34
action_32 (36) = happyShift action_35
action_32 (37) = happyShift action_36
action_32 (39) = happyShift action_37
action_32 (40) = happyShift action_38
action_32 (41) = happyShift action_39
action_32 (42) = happyShift action_40
action_32 (43) = happyShift action_41
action_32 (44) = happyShift action_42
action_32 (45) = happyShift action_43
action_32 (47) = happyShift action_75
action_32 (53) = happyShift action_45
action_32 (14) = happyGoto action_74
action_32 _ = happyFail

action_33 (46) = happyShift action_20
action_33 (49) = happyShift action_21
action_33 (55) = happyShift action_22
action_33 (56) = happyShift action_23
action_33 (57) = happyShift action_24
action_33 (22) = happyGoto action_26
action_33 (24) = happyGoto action_73
action_33 _ = happyFail

action_34 (46) = happyShift action_20
action_34 (49) = happyShift action_21
action_34 (55) = happyShift action_22
action_34 (56) = happyShift action_23
action_34 (57) = happyShift action_24
action_34 (22) = happyGoto action_26
action_34 (24) = happyGoto action_72
action_34 _ = happyFail

action_35 (46) = happyShift action_20
action_35 (49) = happyShift action_21
action_35 (55) = happyShift action_22
action_35 (56) = happyShift action_23
action_35 (57) = happyShift action_24
action_35 (22) = happyGoto action_26
action_35 (24) = happyGoto action_71
action_35 _ = happyFail

action_36 (46) = happyShift action_20
action_36 (49) = happyShift action_21
action_36 (55) = happyShift action_22
action_36 (56) = happyShift action_23
action_36 (57) = happyShift action_24
action_36 (22) = happyGoto action_26
action_36 (24) = happyGoto action_70
action_36 _ = happyFail

action_37 (46) = happyShift action_20
action_37 (49) = happyShift action_21
action_37 (55) = happyShift action_22
action_37 (56) = happyShift action_23
action_37 (57) = happyShift action_24
action_37 (22) = happyGoto action_26
action_37 (24) = happyGoto action_69
action_37 _ = happyFail

action_38 (46) = happyShift action_20
action_38 (49) = happyShift action_21
action_38 (55) = happyShift action_22
action_38 (56) = happyShift action_23
action_38 (57) = happyShift action_24
action_38 (22) = happyGoto action_26
action_38 (24) = happyGoto action_68
action_38 _ = happyFail

action_39 (46) = happyShift action_20
action_39 (49) = happyShift action_21
action_39 (55) = happyShift action_22
action_39 (56) = happyShift action_23
action_39 (57) = happyShift action_24
action_39 (22) = happyGoto action_26
action_39 (24) = happyGoto action_67
action_39 _ = happyFail

action_40 (46) = happyShift action_20
action_40 (49) = happyShift action_21
action_40 (55) = happyShift action_22
action_40 (56) = happyShift action_23
action_40 (57) = happyShift action_24
action_40 (22) = happyGoto action_26
action_40 (24) = happyGoto action_66
action_40 _ = happyFail

action_41 (46) = happyShift action_20
action_41 (49) = happyShift action_21
action_41 (55) = happyShift action_22
action_41 (56) = happyShift action_23
action_41 (57) = happyShift action_24
action_41 (22) = happyGoto action_26
action_41 (24) = happyGoto action_65
action_41 _ = happyFail

action_42 (46) = happyShift action_20
action_42 (49) = happyShift action_21
action_42 (55) = happyShift action_22
action_42 (56) = happyShift action_23
action_42 (57) = happyShift action_24
action_42 (22) = happyGoto action_26
action_42 (24) = happyGoto action_64
action_42 _ = happyFail

action_43 (46) = happyShift action_20
action_43 (49) = happyShift action_21
action_43 (55) = happyShift action_22
action_43 (56) = happyShift action_23
action_43 (57) = happyShift action_24
action_43 (22) = happyGoto action_26
action_43 (24) = happyGoto action_63
action_43 _ = happyFail

action_44 _ = happyReduce_44

action_45 (55) = happyShift action_62
action_45 _ = happyFail

action_46 (46) = happyShift action_20
action_46 (49) = happyShift action_21
action_46 (55) = happyShift action_22
action_46 (56) = happyShift action_23
action_46 (57) = happyShift action_24
action_46 (22) = happyGoto action_26
action_46 (24) = happyGoto action_61
action_46 _ = happyFail

action_47 (55) = happyShift action_60
action_47 _ = happyFail

action_48 _ = happyReduce_27

action_49 _ = happyReduce_7

action_50 (29) = happyShift action_17
action_50 (32) = happyShift action_18
action_50 (33) = happyShift action_19
action_50 (46) = happyShift action_20
action_50 (49) = happyShift action_21
action_50 (55) = happyShift action_22
action_50 (56) = happyShift action_23
action_50 (57) = happyShift action_24
action_50 (12) = happyGoto action_48
action_50 (15) = happyGoto action_10
action_50 (16) = happyGoto action_11
action_50 (20) = happyGoto action_12
action_50 (21) = happyGoto action_13
action_50 (22) = happyGoto action_14
action_50 (23) = happyGoto action_15
action_50 (24) = happyGoto action_16
action_50 _ = happyReduce_1

action_51 (49) = happyShift action_59
action_51 _ = happyFail

action_52 _ = happyReduce_5

action_53 (27) = happyShift action_57
action_53 (29) = happyShift action_17
action_53 (32) = happyShift action_18
action_53 (33) = happyShift action_19
action_53 (46) = happyShift action_20
action_53 (48) = happyShift action_58
action_53 (49) = happyShift action_21
action_53 (55) = happyShift action_22
action_53 (56) = happyShift action_23
action_53 (57) = happyShift action_24
action_53 (10) = happyGoto action_54
action_53 (11) = happyGoto action_55
action_53 (12) = happyGoto action_8
action_53 (13) = happyGoto action_56
action_53 (15) = happyGoto action_10
action_53 (16) = happyGoto action_11
action_53 (20) = happyGoto action_12
action_53 (21) = happyGoto action_13
action_53 (22) = happyGoto action_14
action_53 (23) = happyGoto action_15
action_53 (24) = happyGoto action_16
action_53 _ = happyFail

action_54 _ = happyReduce_19

action_55 (27) = happyShift action_57
action_55 (48) = happyShift action_100
action_55 (10) = happyGoto action_99
action_55 _ = happyFail

action_56 (27) = happyShift action_57
action_56 (29) = happyShift action_17
action_56 (32) = happyShift action_18
action_56 (33) = happyShift action_19
action_56 (46) = happyShift action_20
action_56 (48) = happyShift action_98
action_56 (49) = happyShift action_21
action_56 (55) = happyShift action_22
action_56 (56) = happyShift action_23
action_56 (57) = happyShift action_24
action_56 (10) = happyGoto action_54
action_56 (11) = happyGoto action_97
action_56 (12) = happyGoto action_48
action_56 (15) = happyGoto action_10
action_56 (16) = happyGoto action_11
action_56 (20) = happyGoto action_12
action_56 (21) = happyGoto action_13
action_56 (22) = happyGoto action_14
action_56 (23) = happyGoto action_15
action_56 (24) = happyGoto action_16
action_56 _ = happyFail

action_57 (55) = happyShift action_96
action_57 _ = happyFail

action_58 _ = happyReduce_16

action_59 (55) = happyShift action_95
action_59 (8) = happyGoto action_94
action_59 _ = happyReduce_10

action_60 (38) = happyShift action_93
action_60 _ = happyFail

action_61 (34) = happyShift action_33
action_61 (35) = happyShift action_34
action_61 (36) = happyShift action_35
action_61 (37) = happyShift action_36
action_61 (39) = happyShift action_37
action_61 (40) = happyShift action_38
action_61 (41) = happyShift action_39
action_61 (42) = happyShift action_40
action_61 (43) = happyShift action_41
action_61 (44) = happyShift action_42
action_61 (45) = happyShift action_43
action_61 (52) = happyShift action_92
action_61 (53) = happyShift action_45
action_61 _ = happyFail

action_62 (49) = happyShift action_91
action_62 _ = happyReduce_43

action_63 (34) = happyShift action_33
action_63 (35) = happyShift action_34
action_63 (36) = happyShift action_35
action_63 (37) = happyShift action_36
action_63 (39) = happyShift action_37
action_63 (40) = happyShift action_38
action_63 (41) = happyShift action_39
action_63 (42) = happyShift action_40
action_63 (43) = happyShift action_41
action_63 (53) = happyShift action_45
action_63 _ = happyReduce_59

action_64 (34) = happyShift action_33
action_64 (35) = happyShift action_34
action_64 (36) = happyShift action_35
action_64 (37) = happyShift action_36
action_64 (39) = happyShift action_37
action_64 (40) = happyShift action_38
action_64 (41) = happyShift action_39
action_64 (42) = happyShift action_40
action_64 (43) = happyShift action_41
action_64 (53) = happyShift action_45
action_64 _ = happyReduce_58

action_65 (34) = happyShift action_33
action_65 (35) = happyShift action_34
action_65 (36) = happyShift action_35
action_65 (37) = happyShift action_36
action_65 (39) = happyFail
action_65 (40) = happyFail
action_65 (41) = happyFail
action_65 (42) = happyFail
action_65 (43) = happyFail
action_65 (53) = happyShift action_45
action_65 _ = happyReduce_57

action_66 (34) = happyShift action_33
action_66 (35) = happyShift action_34
action_66 (36) = happyShift action_35
action_66 (37) = happyShift action_36
action_66 (39) = happyFail
action_66 (40) = happyFail
action_66 (41) = happyFail
action_66 (42) = happyFail
action_66 (43) = happyFail
action_66 (53) = happyShift action_45
action_66 _ = happyReduce_56

action_67 (34) = happyShift action_33
action_67 (35) = happyShift action_34
action_67 (36) = happyShift action_35
action_67 (37) = happyShift action_36
action_67 (39) = happyFail
action_67 (40) = happyFail
action_67 (41) = happyFail
action_67 (42) = happyFail
action_67 (43) = happyFail
action_67 (53) = happyShift action_45
action_67 _ = happyReduce_55

action_68 (34) = happyShift action_33
action_68 (35) = happyShift action_34
action_68 (36) = happyShift action_35
action_68 (37) = happyShift action_36
action_68 (39) = happyFail
action_68 (40) = happyFail
action_68 (41) = happyFail
action_68 (42) = happyFail
action_68 (43) = happyFail
action_68 (53) = happyShift action_45
action_68 _ = happyReduce_54

action_69 (34) = happyShift action_33
action_69 (35) = happyShift action_34
action_69 (36) = happyShift action_35
action_69 (37) = happyShift action_36
action_69 (39) = happyFail
action_69 (40) = happyFail
action_69 (41) = happyFail
action_69 (42) = happyFail
action_69 (43) = happyFail
action_69 (53) = happyShift action_45
action_69 _ = happyReduce_53

action_70 (53) = happyShift action_45
action_70 _ = happyReduce_52

action_71 (53) = happyShift action_45
action_71 _ = happyReduce_51

action_72 (36) = happyShift action_35
action_72 (37) = happyShift action_36
action_72 (53) = happyShift action_45
action_72 _ = happyReduce_50

action_73 (36) = happyShift action_35
action_73 (37) = happyShift action_36
action_73 (53) = happyShift action_45
action_73 _ = happyReduce_49

action_74 (30) = happyShift action_89
action_74 (31) = happyShift action_90
action_74 (17) = happyGoto action_86
action_74 (18) = happyGoto action_87
action_74 (19) = happyGoto action_88
action_74 _ = happyReduce_37

action_75 (29) = happyShift action_17
action_75 (32) = happyShift action_18
action_75 (33) = happyShift action_19
action_75 (46) = happyShift action_20
action_75 (48) = happyShift action_85
action_75 (49) = happyShift action_21
action_75 (55) = happyShift action_22
action_75 (56) = happyShift action_23
action_75 (57) = happyShift action_24
action_75 (12) = happyGoto action_8
action_75 (13) = happyGoto action_84
action_75 (15) = happyGoto action_10
action_75 (16) = happyGoto action_11
action_75 (20) = happyGoto action_12
action_75 (21) = happyGoto action_13
action_75 (22) = happyGoto action_14
action_75 (23) = happyGoto action_15
action_75 (24) = happyGoto action_16
action_75 _ = happyFail

action_76 _ = happyReduce_30

action_77 _ = happyReduce_39

action_78 _ = happyReduce_48

action_79 (34) = happyShift action_33
action_79 (35) = happyShift action_34
action_79 (36) = happyShift action_35
action_79 (37) = happyShift action_36
action_79 (39) = happyShift action_37
action_79 (40) = happyShift action_38
action_79 (41) = happyShift action_39
action_79 (42) = happyShift action_40
action_79 (43) = happyShift action_41
action_79 (44) = happyShift action_42
action_79 (45) = happyShift action_43
action_79 (53) = happyShift action_45
action_79 _ = happyReduce_65

action_80 (50) = happyShift action_82
action_80 (51) = happyShift action_83
action_80 _ = happyFail

action_81 _ = happyReduce_64

action_82 _ = happyReduce_63

action_83 (46) = happyShift action_20
action_83 (49) = happyShift action_21
action_83 (55) = happyShift action_22
action_83 (56) = happyShift action_23
action_83 (57) = happyShift action_24
action_83 (22) = happyGoto action_26
action_83 (24) = happyGoto action_113
action_83 _ = happyFail

action_84 (29) = happyShift action_17
action_84 (32) = happyShift action_18
action_84 (33) = happyShift action_19
action_84 (46) = happyShift action_20
action_84 (48) = happyShift action_112
action_84 (49) = happyShift action_21
action_84 (55) = happyShift action_22
action_84 (56) = happyShift action_23
action_84 (57) = happyShift action_24
action_84 (12) = happyGoto action_48
action_84 (15) = happyGoto action_10
action_84 (16) = happyGoto action_11
action_84 (20) = happyGoto action_12
action_84 (21) = happyGoto action_13
action_84 (22) = happyGoto action_14
action_84 (23) = happyGoto action_15
action_84 (24) = happyGoto action_16
action_84 _ = happyFail

action_85 _ = happyReduce_28

action_86 _ = happyReduce_34

action_87 (30) = happyShift action_89
action_87 (31) = happyShift action_90
action_87 (17) = happyGoto action_110
action_87 (19) = happyGoto action_111
action_87 _ = happyReduce_37

action_88 _ = happyReduce_31

action_89 (46) = happyShift action_20
action_89 (49) = happyShift action_21
action_89 (55) = happyShift action_22
action_89 (56) = happyShift action_23
action_89 (57) = happyShift action_24
action_89 (22) = happyGoto action_26
action_89 (24) = happyGoto action_109
action_89 _ = happyFail

action_90 (47) = happyShift action_75
action_90 (14) = happyGoto action_108
action_90 _ = happyFail

action_91 (46) = happyShift action_20
action_91 (49) = happyShift action_21
action_91 (50) = happyShift action_107
action_91 (55) = happyShift action_22
action_91 (56) = happyShift action_23
action_91 (57) = happyShift action_24
action_91 (22) = happyGoto action_26
action_91 (24) = happyGoto action_79
action_91 (25) = happyGoto action_106
action_91 _ = happyFail

action_92 _ = happyReduce_40

action_93 (46) = happyShift action_20
action_93 (49) = happyShift action_21
action_93 (55) = happyShift action_22
action_93 (56) = happyShift action_23
action_93 (57) = happyShift action_24
action_93 (22) = happyGoto action_26
action_93 (24) = happyGoto action_105
action_93 _ = happyFail

action_94 (50) = happyShift action_104
action_94 _ = happyFail

action_95 (54) = happyShift action_103
action_95 _ = happyFail

action_96 (49) = happyShift action_102
action_96 _ = happyFail

action_97 (27) = happyShift action_57
action_97 (48) = happyShift action_101
action_97 (10) = happyGoto action_99
action_97 _ = happyFail

action_98 _ = happyReduce_13

action_99 _ = happyReduce_20

action_100 _ = happyReduce_14

action_101 _ = happyReduce_15

action_102 (55) = happyShift action_95
action_102 (8) = happyGoto action_119
action_102 _ = happyReduce_10

action_103 (55) = happyShift action_118
action_103 _ = happyFail

action_104 (28) = happyShift action_117
action_104 _ = happyReduce_8

action_105 (34) = happyShift action_33
action_105 (35) = happyShift action_34
action_105 (36) = happyShift action_35
action_105 (37) = happyShift action_36
action_105 (39) = happyShift action_37
action_105 (40) = happyShift action_38
action_105 (41) = happyShift action_39
action_105 (42) = happyShift action_40
action_105 (43) = happyShift action_41
action_105 (44) = happyShift action_42
action_105 (45) = happyShift action_43
action_105 (52) = happyShift action_116
action_105 (53) = happyShift action_45
action_105 _ = happyFail

action_106 (50) = happyShift action_115
action_106 (51) = happyShift action_83
action_106 _ = happyFail

action_107 _ = happyReduce_62

action_108 _ = happyReduce_36

action_109 (34) = happyShift action_33
action_109 (35) = happyShift action_34
action_109 (36) = happyShift action_35
action_109 (37) = happyShift action_36
action_109 (39) = happyShift action_37
action_109 (40) = happyShift action_38
action_109 (41) = happyShift action_39
action_109 (42) = happyShift action_40
action_109 (43) = happyShift action_41
action_109 (44) = happyShift action_42
action_109 (45) = happyShift action_43
action_109 (47) = happyShift action_75
action_109 (53) = happyShift action_45
action_109 (14) = happyGoto action_114
action_109 _ = happyFail

action_110 _ = happyReduce_35

action_111 _ = happyReduce_32

action_112 _ = happyReduce_29

action_113 (34) = happyShift action_33
action_113 (35) = happyShift action_34
action_113 (36) = happyShift action_35
action_113 (37) = happyShift action_36
action_113 (39) = happyShift action_37
action_113 (40) = happyShift action_38
action_113 (41) = happyShift action_39
action_113 (42) = happyShift action_40
action_113 (43) = happyShift action_41
action_113 (44) = happyShift action_42
action_113 (45) = happyShift action_43
action_113 (53) = happyShift action_45
action_113 _ = happyReduce_66

action_114 _ = happyReduce_33

action_115 _ = happyReduce_61

action_116 _ = happyReduce_41

action_117 (55) = happyShift action_122
action_117 _ = happyFail

action_118 (51) = happyShift action_121
action_118 _ = happyReduce_11

action_119 (50) = happyShift action_120
action_119 _ = happyFail

action_120 (47) = happyShift action_75
action_120 (54) = happyShift action_125
action_120 (14) = happyGoto action_124
action_120 _ = happyFail

action_121 (55) = happyShift action_95
action_121 (8) = happyGoto action_123
action_121 _ = happyReduce_10

action_122 _ = happyReduce_9

action_123 _ = happyReduce_12

action_124 _ = happyReduce_17

action_125 (55) = happyShift action_126
action_125 _ = happyFail

action_126 (47) = happyShift action_75
action_126 (14) = happyGoto action_127
action_126 _ = happyFail

action_127 _ = happyReduce_18

happyReduce_1 = happyMonadReduce 2 4 happyReduction_1
happyReduction_1 ((HappyAbsSyn13  happy_var_2) `HappyStk`
	(HappyAbsSyn6  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseProgram happy_var_1 happy_var_2  l))
	) (\r -> happyReturn (HappyAbsSyn4 r))

happyReduce_2 = happyMonadReduce 1 4 happyReduction_2
happyReduction_2 ((HappyAbsSyn6  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseProgram happy_var_1 (ASTStatements [])  l))
	) (\r -> happyReturn (HappyAbsSyn4 r))

happyReduce_3 = happyMonadReduce 1 4 happyReduction_3
happyReduction_3 ((HappyAbsSyn13  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseProgram (ASTClasses []) happy_var_1  l))
	) (\r -> happyReturn (HappyAbsSyn4 r))

happyReduce_4 = happyMonadReduce 0 4 happyReduction_4
happyReduction_4 (happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseProgram (ASTClasses []) (ASTStatements [])  l))
	) (\r -> happyReturn (HappyAbsSyn4 r))

happyReduce_5 = happyMonadReduce 2 5 happyReduction_5
happyReduction_5 ((HappyAbsSyn9  happy_var_2) `HappyStk`
	(HappyAbsSyn7  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassExp happy_var_1 happy_var_2 l))
	) (\r -> happyReturn (HappyAbsSyn5 r))

happyReduce_6 = happyMonadReduce 1 6 happyReduction_6
happyReduction_6 ((HappyAbsSyn5  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassExps happy_var_1 (ASTClasses []) l))
	) (\r -> happyReturn (HappyAbsSyn6 r))

happyReduce_7 = happyMonadReduce 2 6 happyReduction_7
happyReduction_7 ((HappyAbsSyn5  happy_var_2) `HappyStk`
	(HappyAbsSyn6  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassExps happy_var_2 happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn6 r))

happyReduce_8 = happyMonadReduce 5 7 happyReduction_8
happyReduction_8 (_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassSig happy_var_2 Nothing happy_var_4 l))
	) (\r -> happyReturn (HappyAbsSyn7 r))

happyReduce_9 = happyMonadReduce 7 7 happyReduction_9
happyReduction_9 ((HappyTerminal (Id happy_var_7)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassSig happy_var_2 (Just happy_var_7) happy_var_4 l))
	) (\r -> happyReturn (HappyAbsSyn7 r))

happyReduce_10 = happyMonadReduce 0 8 happyReduction_10
happyReduction_10 (happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ ASTFormalArgs []))
	) (\r -> happyReturn (HappyAbsSyn8 r))

happyReduce_11 = happyMonadReduce 3 8 happyReduction_11
happyReduction_11 ((HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseFormalArgs happy_var_1 happy_var_3  (ASTFormalArgs []) l))
	) (\r -> happyReturn (HappyAbsSyn8 r))

happyReduce_12 = happyMonadReduce 5 8 happyReduction_12
happyReduction_12 ((HappyAbsSyn8  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseFormalArgs happy_var_1 happy_var_3 happy_var_5 l))
	) (\r -> happyReturn (HappyAbsSyn8 r))

happyReduce_13 = happyMonadReduce 3 9 happyReduction_13
happyReduction_13 (_ `HappyStk`
	(HappyAbsSyn13  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassBody happy_var_2 (ASTMethods []) l))
	) (\r -> happyReturn (HappyAbsSyn9 r))

happyReduce_14 = happyMonadReduce 3 9 happyReduction_14
happyReduction_14 (_ `HappyStk`
	(HappyAbsSyn11  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassBody (ASTStatements []) happy_var_2 l))
	) (\r -> happyReturn (HappyAbsSyn9 r))

happyReduce_15 = happyMonadReduce 4 9 happyReduction_15
happyReduction_15 (_ `HappyStk`
	(HappyAbsSyn11  happy_var_3) `HappyStk`
	(HappyAbsSyn13  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassBody happy_var_2 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn9 r))

happyReduce_16 = happyMonadReduce 2 9 happyReduction_16
happyReduction_16 (_ `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseClassBody (ASTStatements []) (ASTMethods []) l))
	) (\r -> happyReturn (HappyAbsSyn9 r))

happyReduce_17 = happyMonadReduce 6 10 happyReduction_17
happyReduction_17 ((HappyAbsSyn14  happy_var_6) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseMethodExp happy_var_2 happy_var_4 Nothing happy_var_6 l))
	) (\r -> happyReturn (HappyAbsSyn10 r))

happyReduce_18 = happyMonadReduce 8 10 happyReduction_18
happyReduction_18 ((HappyAbsSyn14  happy_var_8) `HappyStk`
	(HappyTerminal (Id happy_var_7)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseMethodExp happy_var_2 happy_var_4 (Just happy_var_7) happy_var_8 l))
	) (\r -> happyReturn (HappyAbsSyn10 r))

happyReduce_19 = happyMonadReduce 1 11 happyReduction_19
happyReduction_19 ((HappyAbsSyn10  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseMethods happy_var_1 (ASTMethods []) l))
	) (\r -> happyReturn (HappyAbsSyn11 r))

happyReduce_20 = happyMonadReduce 2 11 happyReduction_20
happyReduction_20 ((HappyAbsSyn10  happy_var_2) `HappyStk`
	(HappyAbsSyn11  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseMethods happy_var_2 happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn11 r))

happyReduce_21 = happyMonadReduce 1 12 happyReduction_21
happyReduction_21 ((HappyAbsSyn16  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn12 r))

happyReduce_22 = happyMonadReduce 1 12 happyReduction_22
happyReduction_22 ((HappyAbsSyn15  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn12 r))

happyReduce_23 = happyMonadReduce 1 12 happyReduction_23
happyReduction_23 ((HappyAbsSyn20  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn12 r))

happyReduce_24 = happyMonadReduce 1 12 happyReduction_24
happyReduction_24 ((HappyAbsSyn21  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn12 r))

happyReduce_25 = happyMonadReduce 1 12 happyReduction_25
happyReduction_25 ((HappyAbsSyn23  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn12 r))

happyReduce_26 = happyMonadReduce 1 13 happyReduction_26
happyReduction_26 ((HappyAbsSyn12  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatements happy_var_1 (ASTStatements []) l))
	) (\r -> happyReturn (HappyAbsSyn13 r))

happyReduce_27 = happyMonadReduce 2 13 happyReduction_27
happyReduction_27 ((HappyAbsSyn12  happy_var_2) `HappyStk`
	(HappyAbsSyn13  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatements happy_var_2 happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn13 r))

happyReduce_28 = happyMonadReduce 2 14 happyReduction_28
happyReduction_28 (_ `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementBlock (ASTStatements []) l))
	) (\r -> happyReturn (HappyAbsSyn14 r))

happyReduce_29 = happyMonadReduce 3 14 happyReduction_29
happyReduction_29 (_ `HappyStk`
	(HappyAbsSyn13  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseStatementBlock happy_var_2  l))
	) (\r -> happyReturn (HappyAbsSyn14 r))

happyReduce_30 = happyMonadReduce 3 15 happyReduction_30
happyReduction_30 ((HappyAbsSyn14  happy_var_3) `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseWhileExp happy_var_2 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn15 r))

happyReduce_31 = happyMonadReduce 4 16 happyReduction_31
happyReduction_31 ((HappyAbsSyn19  happy_var_4) `HappyStk`
	(HappyAbsSyn14  happy_var_3) `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseIfExp happy_var_2 happy_var_3 (ASTElifs []) happy_var_4 l))
	) (\r -> happyReturn (HappyAbsSyn16 r))

happyReduce_32 = happyMonadReduce 5 16 happyReduction_32
happyReduction_32 ((HappyAbsSyn19  happy_var_5) `HappyStk`
	(HappyAbsSyn18  happy_var_4) `HappyStk`
	(HappyAbsSyn14  happy_var_3) `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseIfExp happy_var_2 happy_var_3 happy_var_4 happy_var_5 l))
	) (\r -> happyReturn (HappyAbsSyn16 r))

happyReduce_33 = happyMonadReduce 3 17 happyReduction_33
happyReduction_33 ((HappyAbsSyn14  happy_var_3) `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseElifExp happy_var_2 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn17 r))

happyReduce_34 = happyMonadReduce 1 18 happyReduction_34
happyReduction_34 ((HappyAbsSyn17  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseElifs happy_var_1 (ASTElifs []) l))
	) (\r -> happyReturn (HappyAbsSyn18 r))

happyReduce_35 = happyMonadReduce 2 18 happyReduction_35
happyReduction_35 ((HappyAbsSyn17  happy_var_2) `HappyStk`
	(HappyAbsSyn18  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseElifs happy_var_2 happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn18 r))

happyReduce_36 = happyMonadReduce 2 19 happyReduction_36
happyReduction_36 ((HappyAbsSyn14  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseElseExp (Just happy_var_2) l))
	) (\r -> happyReturn (HappyAbsSyn19 r))

happyReduce_37 = happyMonadReduce 0 19 happyReduction_37
happyReduction_37 (happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseElseExp Nothing l))
	) (\r -> happyReturn (HappyAbsSyn19 r))

happyReduce_38 = happyMonadReduce 2 20 happyReduction_38
happyReduction_38 (_ `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseReturnExp Nothing l))
	) (\r -> happyReturn (HappyAbsSyn20 r))

happyReduce_39 = happyMonadReduce 3 20 happyReduction_39
happyReduction_39 (_ `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseReturnExp (Just happy_var_2) l))
	) (\r -> happyReturn (HappyAbsSyn20 r))

happyReduce_40 = happyMonadReduce 4 21 happyReduction_40
happyReduction_40 (_ `HappyStk`
	(HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn22  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseAssignmentExp happy_var_1 Nothing happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn21 r))

happyReduce_41 = happyMonadReduce 6 21 happyReduction_41
happyReduction_41 (_ `HappyStk`
	(HappyAbsSyn24  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn22  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseAssignmentExp happy_var_1 (Just happy_var_3) happy_var_5 l))
	) (\r -> happyReturn (HappyAbsSyn21 r))

happyReduce_42 = happyMonadReduce 1 22 happyReduction_42
happyReduction_42 ((HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseLeftExp Nothing happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn22 r))

happyReduce_43 = happyMonadReduce 3 22 happyReduction_43
happyReduction_43 ((HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseLeftExp (Just happy_var_1) happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn22 r))

happyReduce_44 = happyMonadReduce 2 23 happyReduction_44
happyReduction_44 (_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseBareExp happy_var_1 l))
	) (\r -> happyReturn (HappyAbsSyn23 r))

happyReduce_45 = happyMonadReduce 1 24 happyReduction_45
happyReduction_45 ((HappyTerminal (QString happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ ASTRightExp (StrExp happy_var_1 l) ))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_46 = happyMonadReduce 1 24 happyReduction_46
happyReduction_46 ((HappyTerminal (Qint happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ ASTRightExp (IntExp (read happy_var_1) l) ))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_47 = happyMonadReduce 1 24 happyReduction_47
happyReduction_47 ((HappyAbsSyn22  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ ASTRightExp (LeftExp (getLeftExp happy_var_1) l) ))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_48 = happyMonadReduce 3 24 happyReduction_48
happyReduction_48 (_ `HappyStk`
	(HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseParExp happy_var_2 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_49 = happyMonadReduce 3 24 happyReduction_49
happyReduction_49 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "+" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_50 = happyMonadReduce 3 24 happyReduction_50
happyReduction_50 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "-" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_51 = happyMonadReduce 3 24 happyReduction_51
happyReduction_51 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "*" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_52 = happyMonadReduce 3 24 happyReduction_52
happyReduction_52 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "/" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_53 = happyMonadReduce 3 24 happyReduction_53
happyReduction_53 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "==" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_54 = happyMonadReduce 3 24 happyReduction_54
happyReduction_54 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "<=" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_55 = happyMonadReduce 3 24 happyReduction_55
happyReduction_55 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp ">=" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_56 = happyMonadReduce 3 24 happyReduction_56
happyReduction_56 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "<" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_57 = happyMonadReduce 3 24 happyReduction_57
happyReduction_57 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp ">" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_58 = happyMonadReduce 3 24 happyReduction_58
happyReduction_58 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "and" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_59 = happyMonadReduce 3 24 happyReduction_59
happyReduction_59 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen (( getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "or" happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_60 = happyMonadReduce 2 24 happyReduction_60
happyReduction_60 ((HappyAbsSyn24  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseNotExp happy_var_2 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_61 = happyMonadReduce 6 24 happyReduction_61
happyReduction_61 (_ `HappyStk`
	(HappyAbsSyn25  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseFuncCallExp happy_var_1 happy_var_3 happy_var_5 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_62 = happyMonadReduce 5 24 happyReduction_62
happyReduction_62 (_ `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseFuncCallExp happy_var_1 happy_var_3 (ASTActualArgs []) l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_63 = happyMonadReduce 4 24 happyReduction_63
happyReduction_63 (_ `HappyStk`
	(HappyAbsSyn25  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseConstructorExp happy_var_1 happy_var_3 l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_64 = happyMonadReduce 3 24 happyReduction_64
happyReduction_64 (_ `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseConstructorExp happy_var_1 (ASTActualArgs []) l))
	) (\r -> happyReturn (HappyAbsSyn24 r))

happyReduce_65 = happyMonadReduce 1 25 happyReduction_65
happyReduction_65 ((HappyAbsSyn24  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseActualArgs happy_var_1 (ASTActualArgs []) ))
	) (\r -> happyReturn (HappyAbsSyn25 r))

happyReduce_66 = happyMonadReduce 3 25 happyReduction_66
happyReduction_66 ((HappyAbsSyn24  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn25  happy_var_1) `HappyStk`
	happyRest) tk
	 = happyThen ((getLineNo `thenP` (\l a b -> Ok $ parseActualArgs happy_var_3 happy_var_1 ))
	) (\r -> happyReturn (HappyAbsSyn25 r))

happyNewToken action sts stk
	= happyLexer(\tk -> 
	let cont i = action i i tk (HappyState action) sts stk in
	case tk of {
	EOF -> action 58 58 tk (HappyState action) sts stk;
	Class happy_dollar_dollar -> cont 26;
	Def happy_dollar_dollar -> cont 27;
	Extends happy_dollar_dollar -> cont 28;
	If happy_dollar_dollar -> cont 29;
	Elif happy_dollar_dollar -> cont 30;
	Else happy_dollar_dollar -> cont 31;
	While happy_dollar_dollar -> cont 32;
	Return happy_dollar_dollar -> cont 33;
	Plus happy_dollar_dollar -> cont 34;
	Minus happy_dollar_dollar -> cont 35;
	Times happy_dollar_dollar -> cont 36;
	Divide happy_dollar_dollar -> cont 37;
	Gets happy_dollar_dollar -> cont 38;
	Equals happy_dollar_dollar -> cont 39;
	Atmost happy_dollar_dollar -> cont 40;
	Atleast happy_dollar_dollar -> cont 41;
	Less happy_dollar_dollar -> cont 42;
	More happy_dollar_dollar -> cont 43;
	And happy_dollar_dollar -> cont 44;
	Or happy_dollar_dollar -> cont 45;
	Not happy_dollar_dollar -> cont 46;
	LeftCurly happy_dollar_dollar -> cont 47;
	RightCurly happy_dollar_dollar -> cont 48;
	LeftPar happy_dollar_dollar -> cont 49;
	RightPar happy_dollar_dollar -> cont 50;
	Comma happy_dollar_dollar -> cont 51;
	Semi happy_dollar_dollar -> cont 52;
	Period happy_dollar_dollar -> cont 53;
	Colon happy_dollar_dollar -> cont 54;
	Id happy_dollar_dollar -> cont 55;
	Qint happy_dollar_dollar -> cont 56;
	QString happy_dollar_dollar -> cont 57;
	_ -> happyError' tk
	})

happyError_ 58 tk = happyError' tk
happyError_ _ tk = happyError' tk

happyThen :: () => P a -> (a -> P b) -> P b
happyThen = (thenP)
happyReturn :: () => a -> P a
happyReturn = (returnP)
happyThen1 = happyThen
happyReturn1 :: () => a -> P a
happyReturn1 = happyReturn
happyError' :: () => (Token) -> P a
happyError' tk = parseErrorP tk

quackParse = happySomeParser where
  happySomeParser = happyThen (happyParse action_0) (\x -> case x of {HappyAbsSyn4 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


-- parse ASTNode functions
parseProgram :: ASTNode -> ASTNode -> Int -> ASTNode
parseProgram cs ss line = let ASTClasses cs'  = cs
                              ASTStatements ss'  = ss
                           in
                              ASTProgram (ProgramExp cs' ss' line) 

parseClassExp :: ASTNode -> ASTNode -> Int -> ASTNode
parseClassExp s b line = let ASTClassSig s'  = s
                             ASTClassBody b'  = b
                          in 
                             ASTClassExp (ClassDecExp s' b' line)  

parseClassExps :: ASTNode -> ASTNode -> Int -> ASTNode
parseClassExps c cs line = let ASTClassExp c'  = c 
                               ASTClasses cs'   = cs 
                            in ASTClasses (c':cs') 







parseClassSig :: String -> Maybe String -> ASTNode -> Int -> ASTNode
parseClassSig n e as line = let ASTFormalArgs as'  = as
                            in case e of
                             Just e' -> ASTClassSig (ExtendedClassSigExp n as' e' line) 
                             Nothing -> ASTClassSig (ClassSigExp n as' line) 



parseClassBody :: ASTNode -> ASTNode -> Int -> ASTNode
parseClassBody ss ms line = let ASTStatements ss'  = ss
                                ASTMethods ms'  = ms
                             in
                                ASTClassBody (ClassBodyExp ss' ms' line) 


parseFormalArgs :: String -> String -> ASTNode -> Int -> ASTNode
parseFormalArgs a t as line = let a' = FormalArgExp a t line
                                  ASTFormalArgs as'  = as 
                              in ASTFormalArgs (a':as') 


parseMethodExp :: String -> ASTNode -> Maybe String -> ASTNode -> Int -> ASTNode
parseMethodExp i as t sb line = let ASTFormalArgs as'  = as
                                    ASTStatementBlock sb'  = sb
                                 in case t of
                                      Just t' -> ASTMethod (TypedMethodExp i as' t' sb' line) 
                                      Nothing -> ASTMethod (MethodExp i as' sb' line) 



parseMethods :: ASTNode -> ASTNode -> Int -> ASTNode
parseMethods m ms line = let  ASTMethod m'  = m 
                              ASTMethods ms'   = ms 
                            in ASTMethods (m':ms') 


parseStatementExp :: ASTNode -> Int -> ASTNode
parseStatementExp e line = case e of
                        ASTReturnExp e'  -> ASTStatement (ReturnExp e' line)  
                        ASTBareExp e' -> ASTStatement (BareExp e' line)  
                        ASTAssignmentExp e'  -> ASTStatement (AssignmentExp e' line)  
                        ASTWhileExp e' ->  ASTStatement (WhileExp e' line)
                        ASTIfExp e' -> ASTStatement (IfExp e' line)




parseStatements :: ASTNode -> ASTNode -> Int -> ASTNode
parseStatements s ss line = let ASTStatement s'  = s
                                ASTStatements ss' = ss
                            in
                             case ss' of
                                [] -> ASTStatements [s'] 
                                _ -> ASTStatements (s':ss') 


parseStatementBlock :: ASTNode -> Int -> ASTNode
parseStatementBlock ss line = let ASTStatements ss'  = ss in
                                ASTStatementBlock (StatementBlockExp ss' line)  

parseWhileExp :: ASTNode -> ASTNode -> Int -> ASTNode
parseWhileExp c b line = let ASTRightExp c'  = c
                             ASTStatementBlock b'  = b
                          in
                              ASTWhileExp (WhileLoop (c',b') line) 



parseIfExp :: ASTNode -> ASTNode -> ASTNode -> ASTNode -> Int -> ASTNode
parseIfExp c sb el e line = let ASTRightExp c'  = c
                                ASTStatementBlock sb'  = sb
                                ASTElifs el'  = el
                                ASTElseExp e'  = e
                              in
                                 ASTIfExp (IfBlockExp c' sb' el' e' line) 


parseElifExp :: ASTNode -> ASTNode -> Int -> ASTNode
parseElifExp e sb line = let ASTRightExp e'  = e
                             ASTStatementBlock sb'  = sb 
                          in 
                            ASTElifExp (ElifBlockExp e' sb' line) 


parseElifs :: ASTNode -> ASTNode -> Int -> ASTNode
parseElifs e es line = let ASTElifExp e'  = e 
                           ASTElifs es'   = es 
                            in ASTElifs (e':es') 



parseElseExp :: Maybe ASTNode -> Int -> ASTNode
parseElseExp sb line = case sb of
                          Just sb' -> let ASTStatementBlock sb''  = sb' in
                                          ASTElseExp (ElseBlockExp sb'' line) 
                          Nothing -> ASTElseExp (ElseBlockExp (StatementBlockExp [] line) line) 



parseReturnExp :: Maybe ASTNode -> Int -> ASTNode
parseReturnExp r line = case r of
                      Just r' -> ASTReturnExp (RetExp (getRightExp r') line) 
                      Nothing -> ASTReturnExp $ NoReturnExp line


parseAssignmentExp :: ASTNode -> Maybe String -> ASTNode -> Int -> ASTNode
parseAssignmentExp l t r line = let l' = getLeftExp l
                                    r' = getRightExp r
                                 in
                                    case t of
                                        Just t' -> ASTAssignmentExp (TypedAssignExp l' t' r' line) 
                                        Nothing -> ASTAssignmentExp (UntypedAssignExp l'  r' line) 
                                                 




parseLeftExp :: Maybe ASTNode -> String -> Int -> ASTNode
parseLeftExp ast id line = case ast of
                              Just e -> let e' = getRightExp e in
                                                        ASTLeftExp (ObjIdRef e' id line) 
                              Nothing -> ASTLeftExp (IdRef id line) 







parseBareExp e line = ASTBareExp (RightExp (getRightExp e) line) 


parseActualArgs :: ASTNode -> ASTNode  -> ASTNode
parseActualArgs a as  = let ASTRightExp a' = a
                            ASTActualArgs as' = as
                          in
                           case as' of
                              [] -> ASTActualArgs $  [a'] 
                              _ -> ASTActualArgs $  (a':as') 


parseFuncCallExp e id args line = let ASTActualArgs as  = args 
                                    in case e of
                                        ASTRightExp e'  -> ASTRightExp (FuncCallExp e' id as  line) 
                                        ASTLeftExp l  -> case l of
                                                          IdRef s line' -> ASTRightExp (NullExp (show l) line) 
                                                          ObjIdRef  r s  line' -> ASTRightExp r 
                                        _ -> ASTRightExp (NullExp "" line) 


 
parseConstructorExp id args line = let ASTActualArgs as  = args in
                                ASTRightExp (ConstructorExp id as line)  




parseNotExp e line = let ASTRightExp e'  = e in
                        ASTRightExp (NotExp e' line) 


parseBinExp op l r line = let ASTRightExp l'  = l
                              ASTRightExp r'  = r
                          in
                              ASTRightExp (BinaryExp op l' r' line) 

parseParExp e line = let ASTRightExp e' = e in
                        ASTRightExp $ ParExp e' line


data Result a = Ok a | Failed String
type P a =  String -> Int -> Result a



thenP :: P a -> (a -> P b) -> P b
m `thenP` k = \s l -> case m s l of
                Ok a -> k a s l
                Failed e -> Failed e


returnP :: a -> P a
returnP a = \s l -> Ok a


failP :: String -> P a
failP err = \s l -> Failed (err)



catchP :: P a -> (String -> P a) -> P a
catchP m k = \s l -> case m s l of
                        Ok a -> Ok a
                        Failed e -> k e s l


getLineNo :: P Int
getLineNo = \s l -> Ok l


happyLexer :: (Token -> P a) -> P a
happyLexer cont s  = case s of 
                        ('\n':[]) -> (\line -> happyLexer cont [] (line))
                        ('\n':ss) ->  (\line -> happyLexer cont ss (line+1))
                        _ -> (\l -> case happyScanner s l of
                                Left lexErr -> Failed (lexErr ++ " at line " ++ (show l))
                                Right (tok,s',l') -> case tok of 
                                                    Comment _ -> happyLexer cont s' l'
                                                    BlockComment _ -> happyLexer cont s' l'
                                                    _ -> cont tok s' l')
            

printHappyLexer ::  String -> IO ()
printHappyLexer  s  = do
                        putStrLn $ s                      
                        putStrLn $ show (happyScanner s 1) 

parseErrorP :: Token -> P a 
parseErrorP t = \s l -> case t of
                        _ -> Failed ("Parse error at line " ++ (show l))






cycleResult  (cls,  line, (err, path)) = 
          let pathStr = cls ++ (concat $ map ( ((++) " -> ")) path)
            in case err of
              NoErr -> ""
              Cycle -> "Cycle in class heirarchy for " ++ (show cls) ++ " at line " ++ (show line) ++ " (" ++ pathStr ++ ")\n"
              DeadEnd c -> "Dead end in class heirarchy for " ++ (show cls) ++ " at line " ++ (show line)  ++ ", extends unknown class " ++ (show c)  
                              ++ " (" ++ pathStr ++ "->" ++ c ++ ")\n"



--getInheritanceErrors ::  [(String,(Int,(Bool,[QId])))] -> [String]
getInheritanceErrors classes = let m (k, (line, (res,path) ) ) = cycleResult (k,line,(res,path)) in map m classes 

printInheritanceErrors ast = let inheritances = getInheritanceErrors $ classInheritances ast
                              in
                                  mapM_ (putStr) inheritances    

printUndeclaredConsErrors ast = let errors = badConstructors ast in
                                    case errors of
                                     [] -> putStr ""
                                     _ -> mapM_ (\(i,l) -> putStrLn $ "Undeclared constructor \""++i++"\" at line " ++ (show l)) errors

main = do
        p <- getContents
        ast <- return $ quackParse p 1
        
        case ast of
          Ok x -> do
                   putStrLn (show x)
                   print $ extractClasses2 x
                 {-  putStrLn "Finished parse with no syntax errors." 
                   printInheritanceErrors x                
                   printUndeclaredConsErrors x -}

                          
          Failed w -> print w
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "<built-in>" #-}
{-# LINE 1 "<command-line>" #-}
{-# LINE 8 "<command-line>" #-}
# 1 "/usr/include/stdc-predef.h" 1 3 4

# 17 "/usr/include/stdc-predef.h" 3 4










































{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "/usr/lib/ghc/include/ghcversion.h" #-}

















{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
-- Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp 

{-# LINE 13 "templates/GenericTemplate.hs" #-}

{-# LINE 46 "templates/GenericTemplate.hs" #-}








{-# LINE 67 "templates/GenericTemplate.hs" #-}

{-# LINE 77 "templates/GenericTemplate.hs" #-}

{-# LINE 86 "templates/GenericTemplate.hs" #-}

infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is (1), it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
        happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
         (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action

{-# LINE 155 "templates/GenericTemplate.hs" #-}

-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k - ((1) :: Int)) sts of
         sts1@(((st1@(HappyState (action))):(_))) ->
                let r = fn stk in  -- it doesn't hurt to always seq here...
                happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction

{-# LINE 256 "templates/GenericTemplate.hs" #-}
happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery ((1) is the error token)

-- parse error if we are in recovery and we fail again
happyFail (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--      trace "failing" $ 
        happyError_ i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  (1) tk old_st (((HappyState (action))):(sts)) 
                                                (saved_tok `HappyStk` _ `HappyStk` stk) =
--      trace ("discarding state, depth " ++ show (length stk))  $
        action (1) (1) tk (HappyState (action)) sts ((saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail  i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
        action (1) (1) tk (HappyState (action)) sts ( (HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--      happySeq = happyDoSeq
-- otherwise it emits
--      happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.

{-# LINE 322 "templates/GenericTemplate.hs" #-}
{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
