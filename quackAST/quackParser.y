{
module Main where
import Data.List
import qualified Data.Map.Strict as Map

import QuackLexer
import QuackAST
}

%name quackParse
%tokentype { Token }
%error { parseErrorP }
%monad { P } { thenP } { returnP }
%lexer { happyLexer } { EOF }
%token
        --list tokens
    class           { Class $$ }
    def             { Def $$ }    
    extends         { Extends $$ }
    if              { If $$ }
    elif            { Elif $$ }
    else            { Else $$ }
    while           { While $$ }
    return          { Return $$ }
    --  PUNCTUATION 
    '+'             { Plus $$ }
    '-'             { Minus $$ }
    '*'             { Times $$ }
    '/'             { Divide $$ }
    '='             { Gets $$ }
    eq              { Equals $$ }
    le              { Atmost $$ }
    ge              { Atleast $$ }
    '<'             { Less $$ }
    '>'             { More $$ }
    and             { And $$ }
    or              { Or $$ }
    not             { Not $$ }
    '{'             { LeftCurly $$ }
    '}'             { RightCurly $$ }
    '('             { LeftPar $$ }
    ')'             { RightPar $$ }
    ','             { Comma $$ }
    ';'             { Semi $$}
    '.'             { Period $$ }
    ':'             { Colon $$ }
    --  IDENTIFIERS 
    id              { Id $$ }
    -- INT  LITS 
    int             { Qint $$ }
        
    -- STRING  LITS 
  
    str             { QString $$ }


%right not
%left and or
%nonassoc eq '>' '<' le ge
%left '+' '-'
%left '*' '/'
%left '.'
%%


Program : ClassExps Statements                                           { %getLineNo `thenP` (\l a b -> Ok $ parseProgram $1 $2  l) } 
      | ClassExps                                                           { %getLineNo `thenP` (\l a b -> Ok $ parseProgram $1 (ASTStatements [])  l) } 
       | Statements                                                          { %getLineNo `thenP` (\l a b -> Ok $ parseProgram (ASTClasses []) $1  l) } 
      | {- empty -}                                                        { %getLineNo `thenP` (\l a b -> Ok $ parseProgram (ASTClasses []) (ASTStatements [])  l) } 

ClassExp :  ClassSig ClassBody                                         { %getLineNo `thenP` (\l a b -> Ok $ parseClassExp $1 $2 l) } --ClassDecExp $1 {-$2-} }  


ClassExps : ClassExp                                                         { %getLineNo `thenP` (\l a b -> Ok $ parseClassExps $1 (ASTClasses []) l) }-- [$1] }  
          | ClassExps ClassExp                                               { %getLineNo `thenP` (\l a b -> Ok $ parseClassExps $2 $1 l) } --($2:$1) }  


ClassSig : class id '(' FormalArgs ')'                                        { %getLineNo `thenP` (\l a b -> Ok $ parseClassSig $2 Nothing $4 l) } 
         | class id '(' FormalArgs ')' extends id                             { %getLineNo `thenP` (\l a b -> Ok $ parseClassSig $2 (Just $7) $4 l) } 

FormalArgs : {- empty -}                                                      { %getLineNo `thenP` (\l a b -> Ok $ ASTFormalArgs []) } 
             | id ':' id                                                       { %getLineNo `thenP` (\l a b -> Ok $ parseFormalArgs $1 $3  (ASTFormalArgs []) l) }
             | id ':' id ','  FormalArgs                                       { %getLineNo `thenP` (\l a b -> Ok $ parseFormalArgs $1 $3 $5 l) }  
    
ClassBody :'{' Statements '}'                                               { %getLineNo `thenP` (\l a b -> Ok $ parseClassBody $2 (ASTMethods []) l) }  
          | '{' Methods '}'                                                  { %getLineNo `thenP` (\l a b -> Ok $ parseClassBody (ASTStatements []) $2 l) }  
          | '{' Statements Methods '}'                                       { %getLineNo `thenP` (\l a b -> Ok $ parseClassBody $2 $3 l) } 
          | '{' '}'                                                          { %getLineNo `thenP` (\l a b -> Ok $ parseClassBody (ASTStatements []) (ASTMethods []) l) }  
  


Method : def id '(' FormalArgs ')' StatementBlock                            { %getLineNo `thenP` (\l a b -> Ok $ parseMethodExp $2 $4 Nothing $6 l)  }  
       | def id '(' FormalArgs ')' ':' id StatementBlock                     { %getLineNo `thenP` (\l a b -> Ok $ parseMethodExp $2 $4 (Just $7) $8 l)  }  

Methods : Method                                                             { %getLineNo `thenP` (\l a b -> Ok $ parseMethods $1 (ASTMethods []) l) }  
        | Methods Method                                                     { %getLineNo `thenP` (\l a b -> Ok $ parseMethods $2 $1 l) }  

Statement : IfExp                                                             { %getLineNo `thenP` (\l a b -> Ok $ parseStatementExp $1 l) }  
          | WhileExp                                                           { %getLineNo `thenP` (\l a b -> Ok $ parseStatementExp $1 l) }  
          | ReturnExp                                                         { %getLineNo `thenP` (\l a b -> Ok $ parseStatementExp $1 l) }  
          | Assignment                                                       { %getLineNo `thenP` (\l a b -> Ok $ parseStatementExp $1 l) }  
          | BareExp                                                          { %getLineNo `thenP` (\l a b -> Ok $ parseStatementExp $1 l) }
   
Statements : Statement                                                       { %getLineNo `thenP` (\l a b -> Ok $ parseStatements $1 (ASTStatements []) l)  }  
           | Statements Statement                                            { %getLineNo `thenP` (\l a b -> Ok $ parseStatements $2 $1 l) }  
    
StatementBlock : '{' '}'                                                     { %getLineNo `thenP` (\l a b -> Ok $ parseStatementBlock (ASTStatements []) l)  }   
               | '{' Statements '}'                                          { %getLineNo `thenP` (\l a b -> Ok $ parseStatementBlock $2  l)  }


WhileExp : while RExp StatementBlock                                         { %getLineNo `thenP` (\l a b -> Ok $ parseWhileExp $2 $3 l) }  


IfExp : if RExp StatementBlock  Else                                          { %getLineNo `thenP` (\l a b -> Ok $ parseIfExp $2 $3 (ASTElifs []) $4 l) }
      | if RExp StatementBlock Elifs Else                                     { %getLineNo `thenP` (\l a b -> Ok $ parseIfExp $2 $3 $4 $5 l) }
  

Elif : elif RExp StatementBlock                                               { %getLineNo `thenP` (\l a b -> Ok $ parseElifExp $2 $3 l) }
 
Elifs : Elif                                                                  { %getLineNo `thenP` (\l a b -> Ok $ parseElifs $1 (ASTElifs []) l) }
      | Elifs Elif                                                            { %getLineNo `thenP` (\l a b -> Ok $ parseElifs $2 $1 l) }
      -- | {- empty -}                                                           { %getLineNo `thenP` (\l a b -> Ok $ emptyElifs l)  }


Else : else StatementBlock                                                    { %getLineNo `thenP` (\l a b -> Ok $ parseElseExp (Just $2) l) }
     | {- empty -}                                                            { %getLineNo `thenP` (\l a b -> Ok $ parseElseExp Nothing l) }


ReturnExp : return ';'                                                       { %getLineNo `thenP` (\l a b -> Ok $ parseReturnExp Nothing l) }  
          | return RExp ';'                                                  { %getLineNo `thenP` (\l a b -> Ok $ parseReturnExp (Just $2) l) }  

Assignment : LExp '=' RExp ';'                                               { %getLineNo `thenP` (\l a b -> Ok $ parseAssignmentExp $1 Nothing $3 l) }  
           | LExp ':' id '=' RExp ';'                                        { %getLineNo `thenP` (\l a b -> Ok $ parseAssignmentExp $1 (Just $3) $5 l) }  

LExp : id                                                                    { %getLineNo `thenP` (\l a b -> Ok $ parseLeftExp Nothing $1 l) }  
     | RExp '.' id                                                           { %getLineNo `thenP` (\l a b -> Ok $ parseLeftExp (Just $1) $3 l) }  

BareExp : RExp ';'                                                           { %getLineNo `thenP` (\l a b -> Ok $ parseBareExp $1 l) }   


RExp : str                                                                   { % getLineNo `thenP` (\l _ _ -> Ok $ ASTRightExp (StrExp $1 l) ) }  
     | int                                                                   { % getLineNo `thenP` (\l _ _ -> Ok $ ASTRightExp (IntExp (read $1) l) )  }  
     | LExp                                                                  { %getLineNo `thenP` (\l a b -> Ok $ ASTRightExp (LeftExp (getLeftExp $1) l) ) }  
     | '(' RExp ')'                                                          { % getLineNo `thenP` (\l _ _ -> Ok $ parseParExp $2 l) }  
     | RExp '+' RExp                                                         { % getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "+" $1 $3 l) }  
     | RExp '-' RExp                                                         { % getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "-" $1 $3 l) }  
     | RExp '*' RExp                                                         { % getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "*" $1 $3 l) }  
     | RExp '/' RExp                                                         { % getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "/" $1 $3 l) }  
     | RExp eq RExp                                                          { % getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "==" $1 $3 l) }  
     | RExp le RExp                                                          { % getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "<=" $1 $3 l) }  
     | RExp ge RExp                                                          { % getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp ">=" $1 $3 l) }  
     | RExp '<' RExp                                                         { % getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "<" $1 $3 l) }  
     | RExp '>' RExp                                                         { % getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp ">" $1 $3 l) }   
     | RExp and RExp                                                         { % getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "and" $1 $3 l) }  
     | RExp or RExp                                                          { % getLineNo `thenP` (\l _ _ -> Ok $ parseBinExp "or" $1 $3 l) }      
     | not RExp                                                              { %getLineNo `thenP` (\l a b -> Ok $ parseNotExp $2 l) }  
     | RExp '.' id '(' ActualArgs ')'                                        { %getLineNo `thenP` (\l a b -> Ok $ parseFuncCallExp $1 $3 $5 l)  }  
     | RExp '.' id '('  ')'                                                  { %getLineNo `thenP` (\l a b -> Ok $ parseFuncCallExp $1 $3 (ASTActualArgs []) l)  } 
     | id '(' ActualArgs ')'                                                 { %getLineNo `thenP` (\l a b -> Ok $ parseConstructorExp $1 $3 l)  }  
     | id '(' ')'                                                            { %getLineNo `thenP` (\l a b -> Ok $ parseConstructorExp $1 (ASTActualArgs []) l)  } 

ActualArgs : RExp                                                            { %getLineNo `thenP` (\l a b -> Ok $ parseActualArgs $1 (ASTActualArgs []) )  }  
           | ActualArgs ',' RExp                                             { %getLineNo `thenP` (\l a b -> Ok $ parseActualArgs $3 $1 )  } 



{




-- parse ASTNode functions
parseProgram :: ASTNode -> ASTNode -> Int -> ASTNode
parseProgram cs ss line = let ASTClasses cs'  = cs
                              ASTStatements ss'  = ss
                           in
                              ASTProgram (ProgramExp cs' ss' line) 

parseClassExp :: ASTNode -> ASTNode -> Int -> ASTNode
parseClassExp s b line = let ASTClassSig s'  = s
                             ASTClassBody b'  = b
                          in 
                             ASTClassExp (ClassDecExp s' b' line)  

parseClassExps :: ASTNode -> ASTNode -> Int -> ASTNode
parseClassExps c cs line = let ASTClassExp c'  = c 
                               ASTClasses cs'   = cs 
                            in ASTClasses (c':cs') 







parseClassSig :: String -> Maybe String -> ASTNode -> Int -> ASTNode
parseClassSig n e as line = let ASTFormalArgs as'  = as
                            in case e of
                             Just e' -> ASTClassSig (ExtendedClassSigExp n as' e' line) 
                             Nothing -> ASTClassSig (ClassSigExp n as' line) 



parseClassBody :: ASTNode -> ASTNode -> Int -> ASTNode
parseClassBody ss ms line = let ASTStatements ss'  = ss
                                ASTMethods ms'  = ms
                             in
                                ASTClassBody (ClassBodyExp ss' ms' line) 


parseFormalArgs :: String -> String -> ASTNode -> Int -> ASTNode
parseFormalArgs a t as line = let a' = FormalArgExp a t line
                                  ASTFormalArgs as'  = as 
                              in ASTFormalArgs (a':as') 


parseMethodExp :: String -> ASTNode -> Maybe String -> ASTNode -> Int -> ASTNode
parseMethodExp i as t sb line = let ASTFormalArgs as'  = as
                                    ASTStatementBlock sb'  = sb
                                 in case t of
                                      Just t' -> ASTMethod (TypedMethodExp i as' t' sb' line) 
                                      Nothing -> ASTMethod (MethodExp i as' sb' line) 



parseMethods :: ASTNode -> ASTNode -> Int -> ASTNode
parseMethods m ms line = let  ASTMethod m'  = m 
                              ASTMethods ms'   = ms 
                            in ASTMethods (m':ms') 


parseStatementExp :: ASTNode -> Int -> ASTNode
parseStatementExp e line = case e of
                        ASTReturnExp e'  -> ASTStatement (ReturnExp e' line)  
                        ASTBareExp e' -> ASTStatement (BareExp e' line)  
                        ASTAssignmentExp e'  -> ASTStatement (AssignmentExp e' line)  
                        ASTWhileExp e' ->  ASTStatement (WhileExp e' line)
                        ASTIfExp e' -> ASTStatement (IfExp e' line)




parseStatements :: ASTNode -> ASTNode -> Int -> ASTNode
parseStatements s ss line = let ASTStatement s'  = s
                                ASTStatements ss' = ss
                            in
                             case ss' of
                                [] -> ASTStatements [s'] 
                                _ -> ASTStatements (s':ss') 


parseStatementBlock :: ASTNode -> Int -> ASTNode
parseStatementBlock ss line = let ASTStatements ss'  = ss in
                                ASTStatementBlock (StatementBlockExp ss' line)  

parseWhileExp :: ASTNode -> ASTNode -> Int -> ASTNode
parseWhileExp c b line = let ASTRightExp c'  = c
                             ASTStatementBlock b'  = b
                          in
                              ASTWhileExp (WhileLoop (c',b') line) 



parseIfExp :: ASTNode -> ASTNode -> ASTNode -> ASTNode -> Int -> ASTNode
parseIfExp c sb el e line = let ASTRightExp c'  = c
                                ASTStatementBlock sb'  = sb
                                ASTElifs el'  = el
                                ASTElseExp e'  = e
                              in
                                 ASTIfExp (IfBlockExp c' sb' el' e' line) 


parseElifExp :: ASTNode -> ASTNode -> Int -> ASTNode
parseElifExp e sb line = let ASTRightExp e'  = e
                             ASTStatementBlock sb'  = sb 
                          in 
                            ASTElifExp (ElifBlockExp e' sb' line) 


parseElifs :: ASTNode -> ASTNode -> Int -> ASTNode
parseElifs e es line = let ASTElifExp e'  = e 
                           ASTElifs es'   = es 
                            in ASTElifs (e':es') 



parseElseExp :: Maybe ASTNode -> Int -> ASTNode
parseElseExp sb line = case sb of
                          Just sb' -> let ASTStatementBlock sb''  = sb' in
                                          ASTElseExp (ElseBlockExp sb'' line) 
                          Nothing -> ASTElseExp (ElseBlockExp (StatementBlockExp [] line) line) 



parseReturnExp :: Maybe ASTNode -> Int -> ASTNode
parseReturnExp r line = case r of
                      Just r' -> ASTReturnExp (RetExp (getRightExp r') line) 
                      Nothing -> ASTReturnExp $ NoReturnExp line


parseAssignmentExp :: ASTNode -> Maybe String -> ASTNode -> Int -> ASTNode
parseAssignmentExp l t r line = let l' = getLeftExp l
                                    r' = getRightExp r
                                 in
                                    case t of
                                        Just t' -> ASTAssignmentExp (TypedAssignExp l' t' r' line) 
                                        Nothing -> ASTAssignmentExp (UntypedAssignExp l'  r' line) 
                                                 




parseLeftExp :: Maybe ASTNode -> String -> Int -> ASTNode
parseLeftExp ast id line = case ast of
                              Just e -> let e' = getRightExp e in
                                                        ASTLeftExp (ObjIdRef e' id line) 
                              Nothing -> ASTLeftExp (IdRef id line) 







parseBareExp e line = ASTBareExp (RightExp (getRightExp e) line) 


parseActualArgs :: ASTNode -> ASTNode  -> ASTNode
parseActualArgs a as  = let ASTRightExp a' = a
                            ASTActualArgs as' = as
                          in
                           case as' of
                              [] -> ASTActualArgs $  [a'] 
                              _ -> ASTActualArgs $  (a':as') 


parseFuncCallExp e id args line = let ASTActualArgs as  = args 
                                    in case e of
                                        ASTRightExp e'  -> ASTRightExp (FuncCallExp e' id as  line) 
                                        ASTLeftExp l  -> case l of
                                                          IdRef s line' -> ASTRightExp (NullExp (show l) line) 
                                                          ObjIdRef  r s  line' -> ASTRightExp r 
                                        _ -> ASTRightExp (NullExp "" line) 


 
parseConstructorExp id args line = let ASTActualArgs as  = args in
                                ASTRightExp (ConstructorExp id as line)  




parseNotExp e line = let ASTRightExp e'  = e in
                        ASTRightExp (NotExp e' line) 


parseBinExp op l r line = let ASTRightExp l'  = l
                              ASTRightExp r'  = r
                          in
                              ASTRightExp (BinaryExp op l' r' line) 

parseParExp e line = let ASTRightExp e' = e in
                        ASTRightExp $ ParExp e' line


data Result a = Ok a | Failed String
type P a =  String -> Int -> Result a



thenP :: P a -> (a -> P b) -> P b
m `thenP` k = \s l -> case m s l of
                Ok a -> k a s l
                Failed e -> Failed e


returnP :: a -> P a
returnP a = \s l -> Ok a


failP :: String -> P a
failP err = \s l -> Failed (err)



catchP :: P a -> (String -> P a) -> P a
catchP m k = \s l -> case m s l of
                        Ok a -> Ok a
                        Failed e -> k e s l


getLineNo :: P Int
getLineNo = \s l -> Ok l


happyLexer :: (Token -> P a) -> P a
happyLexer cont s  = case s of 
                        ('\n':[]) -> (\line -> happyLexer cont [] (line))
                        ('\n':ss) ->  (\line -> happyLexer cont ss (line+1))
                        _ -> (\l -> case happyScanner s l of
                                Left lexErr -> Failed (lexErr ++ " at line " ++ (show l))
                                Right (tok,s',l') -> case tok of 
                                                    Comment _ -> happyLexer cont s' l'
                                                    BlockComment _ -> happyLexer cont s' l'
                                                    _ -> cont tok s' l')
            

printHappyLexer ::  String -> IO ()
printHappyLexer  s  = do
                        putStrLn $ s                      
                        putStrLn $ show (happyScanner s 1) 

parseErrorP :: Token -> P a 
parseErrorP t = \s l -> case t of
                        _ -> Failed ("Parse error at line " ++ (show l))






cycleResult  (cls,  line, (err, path)) = 
          let pathStr = cls ++ (concat $ map ( ((++) " -> ")) path)
            in case err of
              NoErr -> ""
              Cycle -> "Cycle in class heirarchy for " ++ (show cls) ++ " at line " ++ (show line) ++ " (" ++ pathStr ++ ")\n"
              DeadEnd c -> "Dead end in class heirarchy for " ++ (show cls) ++ " at line " ++ (show line)  ++ ", extends unknown class " ++ (show c)  
                              ++ " (" ++ pathStr ++ "->" ++ c ++ ")\n"



--getInheritanceErrors ::  [(String,(Int,(Bool,[QId])))] -> [String]
getInheritanceErrors classes = let m (k, (line, (res,path) ) ) = cycleResult (k,line,(res,path)) in map m classes 

printInheritanceErrors ast = let inheritances = getInheritanceErrors $ classInheritances ast
                              in
                                  mapM_ (putStr) inheritances    

printUndeclaredConsErrors ast = let errors = badConstructors ast in
                                    case errors of
                                     [] -> putStr ""
                                     _ -> mapM_ (\(i,l) -> putStrLn $ "Undeclared constructor \""++i++"\" at line " ++ (show l)) errors

main = do
        p <- getContents
        ast <- return $ quackParse p 1
        
        case ast of
          Ok x -> do
                   putStrLn (show x)
                   print $ extractClasses2 x
                 {-  putStrLn "Finished parse with no syntax errors." 
                   printInheritanceErrors x                
                   printUndeclaredConsErrors x -}

                          
          Failed w -> print w
}

