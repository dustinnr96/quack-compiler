{
module Main where
import Lexer (Token(..), lex)
}

%name calc
%tokentype { Token }
%error { parseError }

%token
        --list tokens
    class           { Class $$ }
    def             { Def $$ }    
    extends         { Extends $$ }
  {-  if              { If $$ }
    elif            { Elif $$ }
    else            { Else }
    while           { While }
    return          { Return }-}
    --  PUNCTUATION 
    '+'             { Plus }
    '-'             { Minus }
    '*'             { Times }
    '/'             { Divide }
   -- '='             { Gets }
    eq              { Equals }
    le              { Atmost }
    ge              { Atleast }
    '<'             { Less }
    '>'             { More }
    and             { And }
    or              { Or }
    not             { Not }
    '{'             { LeftCurly $$ }
    '}'             { RightCurly $$ }
    '('             { LeftPar $$ }
    ')'             { RightPar $$ }
    ','             { Comma $$ }
    ';'             { Semi } 
    '.'             { Period } 
    ':'             { Colon $$ } 
    --  IDENTIFIERS 
    id              { Id $$ }
    -- INT  LITS 
   int             { Qint $$ }
        
    -- STRING  LITS 
  
    str             { QString $$ } 
%%


-- Grammar prod rules ex



Program : ClassExps Statements { 0 } 

ClassExp : ClassSig ClassBody { 0 } 

ClassExps : ClassExp  { 0 } 
          | ClassExps ClassExp { 0 } 




ClassSig : class id '(' FormalArgs ')' { 0 } 
         | class id '(' FormalArgs extends id { 0 } 

FormalArgs : {- empty -}  { 0 } 
           | id ':' id { 0 } 
           | id ':' id ','  FormalArgs { 0 } 
      
ClassBody : '{' '}' { 0 } 
          | '{' Statements Methods '}' { 0 } 

Statement :  id { 0 }

Method : def id '(' FormalArgs ')' StatementBlock { 0 } 
       | def id '(' FormalArgs ')' ':' id StatementBlock  { 0 } 

Methods : Method  { 0 } 
        | Methods Method { 0 } 
{-
Statement : IfExp { 0 } 
          | WhileExp { 0 } 
          | ReturnExp { 0 } 
          | Assignment { 0 } 
          | BareExp     { 0 } 
-}
Statements : Statement { 0 } 
           | Statements Statement { 0 } 

StatementBlock : '{' '}'       { 0 }  
               | '{' Statements '}'   { 0 } 
{-
WhileExp : while RExp StatementBlock           { 0 } 

IfExp : if RExp StatementBlock                 { 0 } 
       | if RExp StatementBlock elif RExp StatementBlock { 0 } 
       | if RExp StatementBlock else StatementBlock     { 0 } 
       | if RExp StatementBlock elif RExp StatementBlock else StatementBlock { 0 } 

ReturnExp : return ';'                  { 0 } 
          | return RExp ';'             { 0 } 

Assignment : LExp '=' RExp ';'          { 0 } 
           | LExp ':' id '=' RExp ';'   { 0 } 
 -}
LExp : id                               { 0 } 
     | RExp '.' id                      { 0 } 

BareExp : RExp ';'                      { 0 }

 RExp : str                              { 0 } 
     | int                              { 0 } 
     --| LExp                             { 0 } 
     | RExp '+' RExp                    { 0 } 
     | RExp '-' RExp                    { 0 } 
     | RExp '*' RExp                    { 0 } 
     | RExp '/' RExp                    { 0 } 
     | '(' RExp ')'                     { 0 } 
     | RExp eq RExp                     { 0 } 
     | RExp le RExp                     { 0 } 
     | RExp ge RExp                     { 0 } 
     | RExp '<' RExp                    { 0 } 
     | RExp '>' RExp                    { 0 } 
     | RExp and RExp                    { 0 } 
     | RExp or RExp                     { 0 }      
     | not RExp                         { 0 } 
     | RExp '.' id '(' ActualArgs ')'   { 0 } 
     | id '(' ActualArgs ')'            { 0 } 
    
 ActualArgs : RExp                       { 0 } 
           | ActualArgs ',' RExp         { 0 } 
     
{

parseError :: [Token] -> a
parseError _ = error "Parse error"


--data type for parsed expr ex
--data ActualArgs = RExp | ActualArgs Exp


--data struct for tokens   (already in lexer)

--main = getContents >>= print . calc . lexer

parse = getContents >>= print . calc . Lexer.lex

main = parse
}

