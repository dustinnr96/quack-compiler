{-# OPTIONS_GHC -w #-}
module Main where
import Lexer (Token(..), lex)
import Control.Applicative(Applicative(..))
import Control.Monad (ap)

-- parser produced by Happy Version 1.19.5

data HappyAbsSyn t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16 t17 t18
	= HappyTerminal (Token)
	| HappyErrorToken Int
	| HappyAbsSyn4 t4
	| HappyAbsSyn5 t5
	| HappyAbsSyn6 t6
	| HappyAbsSyn7 t7
	| HappyAbsSyn8 t8
	| HappyAbsSyn9 t9
	| HappyAbsSyn10 t10
	| HappyAbsSyn11 t11
	| HappyAbsSyn12 t12
	| HappyAbsSyn13 t13
	| HappyAbsSyn14 t14
	| HappyAbsSyn15 t15
	| HappyAbsSyn16 t16
	| HappyAbsSyn17 t17
	| HappyAbsSyn18 t18

action_0 (19) = happyShift action_5
action_0 (4) = happyGoto action_6
action_0 (5) = happyGoto action_2
action_0 (6) = happyGoto action_3
action_0 (7) = happyGoto action_4
action_0 _ = happyFail

action_1 (19) = happyShift action_5
action_1 (5) = happyGoto action_2
action_1 (6) = happyGoto action_3
action_1 (7) = happyGoto action_4
action_1 _ = happyFail

action_2 _ = happyReduce_3

action_3 (19) = happyShift action_5
action_3 (42) = happyShift action_13
action_3 (5) = happyGoto action_10
action_3 (7) = happyGoto action_4
action_3 (10) = happyGoto action_11
action_3 (13) = happyGoto action_12
action_3 _ = happyFail

action_4 (34) = happyShift action_9
action_4 (9) = happyGoto action_8
action_4 _ = happyFail

action_5 (42) = happyShift action_7
action_5 _ = happyFail

action_6 (45) = happyAccept
action_6 _ = happyFail

action_7 (36) = happyShift action_17
action_7 _ = happyFail

action_8 _ = happyReduce_2

action_9 (35) = happyShift action_16
action_9 (42) = happyShift action_13
action_9 (10) = happyGoto action_11
action_9 (13) = happyGoto action_15
action_9 _ = happyFail

action_10 _ = happyReduce_4

action_11 _ = happyReduce_17

action_12 (42) = happyShift action_13
action_12 (10) = happyGoto action_14
action_12 _ = happyReduce_1

action_13 _ = happyReduce_12

action_14 _ = happyReduce_18

action_15 (20) = happyShift action_22
action_15 (42) = happyShift action_13
action_15 (10) = happyGoto action_14
action_15 (11) = happyGoto action_20
action_15 (12) = happyGoto action_21
action_15 _ = happyFail

action_16 _ = happyReduce_10

action_17 (42) = happyShift action_19
action_17 (8) = happyGoto action_18
action_17 _ = happyReduce_7

action_18 (21) = happyShift action_27
action_18 (37) = happyShift action_28
action_18 _ = happyFail

action_19 (41) = happyShift action_26
action_19 _ = happyFail

action_20 _ = happyReduce_15

action_21 (20) = happyShift action_22
action_21 (35) = happyShift action_25
action_21 (11) = happyGoto action_24
action_21 _ = happyFail

action_22 (42) = happyShift action_23
action_22 _ = happyFail

action_23 (36) = happyShift action_31
action_23 _ = happyFail

action_24 _ = happyReduce_16

action_25 _ = happyReduce_11

action_26 (42) = happyShift action_30
action_26 _ = happyFail

action_27 (42) = happyShift action_29
action_27 _ = happyFail

action_28 _ = happyReduce_5

action_29 _ = happyReduce_6

action_30 (38) = happyShift action_33
action_30 _ = happyReduce_8

action_31 (42) = happyShift action_19
action_31 (8) = happyGoto action_32
action_31 _ = happyReduce_7

action_32 (37) = happyShift action_35
action_32 _ = happyFail

action_33 (42) = happyShift action_19
action_33 (8) = happyGoto action_34
action_33 _ = happyReduce_7

action_34 _ = happyReduce_9

action_35 (34) = happyShift action_37
action_35 (41) = happyShift action_38
action_35 (14) = happyGoto action_36
action_35 _ = happyFail

action_36 _ = happyReduce_13

action_37 (35) = happyShift action_41
action_37 (42) = happyShift action_13
action_37 (10) = happyGoto action_11
action_37 (13) = happyGoto action_40
action_37 _ = happyFail

action_38 (42) = happyShift action_39
action_38 _ = happyFail

action_39 (34) = happyShift action_37
action_39 (14) = happyGoto action_43
action_39 _ = happyFail

action_40 (35) = happyShift action_42
action_40 (42) = happyShift action_13
action_40 (10) = happyGoto action_14
action_40 _ = happyFail

action_41 _ = happyReduce_19

action_42 _ = happyReduce_20

action_43 _ = happyReduce_14

happyReduce_1 = happySpecReduce_2  4 happyReduction_1
happyReduction_1 _
	_
	 =  HappyAbsSyn4
		 (0
	)

happyReduce_2 = happySpecReduce_2  5 happyReduction_2
happyReduction_2 _
	_
	 =  HappyAbsSyn5
		 (0
	)

happyReduce_3 = happySpecReduce_1  6 happyReduction_3
happyReduction_3 _
	 =  HappyAbsSyn6
		 (0
	)

happyReduce_4 = happySpecReduce_2  6 happyReduction_4
happyReduction_4 _
	_
	 =  HappyAbsSyn6
		 (0
	)

happyReduce_5 = happyReduce 5 7 happyReduction_5
happyReduction_5 (_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn7
		 (0
	) `HappyStk` happyRest

happyReduce_6 = happyReduce 6 7 happyReduction_6
happyReduction_6 (_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn7
		 (0
	) `HappyStk` happyRest

happyReduce_7 = happySpecReduce_0  8 happyReduction_7
happyReduction_7  =  HappyAbsSyn8
		 (0
	)

happyReduce_8 = happySpecReduce_3  8 happyReduction_8
happyReduction_8 _
	_
	_
	 =  HappyAbsSyn8
		 (0
	)

happyReduce_9 = happyReduce 5 8 happyReduction_9
happyReduction_9 (_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn8
		 (0
	) `HappyStk` happyRest

happyReduce_10 = happySpecReduce_2  9 happyReduction_10
happyReduction_10 _
	_
	 =  HappyAbsSyn9
		 (0
	)

happyReduce_11 = happyReduce 4 9 happyReduction_11
happyReduction_11 (_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn9
		 (0
	) `HappyStk` happyRest

happyReduce_12 = happySpecReduce_1  10 happyReduction_12
happyReduction_12 _
	 =  HappyAbsSyn10
		 (0
	)

happyReduce_13 = happyReduce 6 11 happyReduction_13
happyReduction_13 (_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn11
		 (0
	) `HappyStk` happyRest

happyReduce_14 = happyReduce 8 11 happyReduction_14
happyReduction_14 (_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn11
		 (0
	) `HappyStk` happyRest

happyReduce_15 = happySpecReduce_1  12 happyReduction_15
happyReduction_15 _
	 =  HappyAbsSyn12
		 (0
	)

happyReduce_16 = happySpecReduce_2  12 happyReduction_16
happyReduction_16 _
	_
	 =  HappyAbsSyn12
		 (0
	)

happyReduce_17 = happySpecReduce_1  13 happyReduction_17
happyReduction_17 _
	 =  HappyAbsSyn13
		 (0
	)

happyReduce_18 = happySpecReduce_2  13 happyReduction_18
happyReduction_18 _
	_
	 =  HappyAbsSyn13
		 (0
	)

happyReduce_19 = happySpecReduce_2  14 happyReduction_19
happyReduction_19 _
	_
	 =  HappyAbsSyn14
		 (0
	)

happyReduce_20 = happySpecReduce_3  14 happyReduction_20
happyReduction_20 _
	_
	_
	 =  HappyAbsSyn14
		 (0
	)

happyReduce_21 = happySpecReduce_1  15 happyReduction_21
happyReduction_21 _
	 =  HappyAbsSyn15
		 (0
	)

happyReduce_22 = happySpecReduce_3  15 happyReduction_22
happyReduction_22 _
	_
	_
	 =  HappyAbsSyn15
		 (0
	)

happyReduce_23 = happySpecReduce_2  16 happyReduction_23
happyReduction_23 _
	_
	 =  HappyAbsSyn16
		 (0
	)

happyReduce_24 = happySpecReduce_1  17 happyReduction_24
happyReduction_24 _
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_25 = happySpecReduce_1  17 happyReduction_25
happyReduction_25 _
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_26 = happySpecReduce_3  17 happyReduction_26
happyReduction_26 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_27 = happySpecReduce_3  17 happyReduction_27
happyReduction_27 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_28 = happySpecReduce_3  17 happyReduction_28
happyReduction_28 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_29 = happySpecReduce_3  17 happyReduction_29
happyReduction_29 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_30 = happySpecReduce_3  17 happyReduction_30
happyReduction_30 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_31 = happySpecReduce_3  17 happyReduction_31
happyReduction_31 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_32 = happySpecReduce_3  17 happyReduction_32
happyReduction_32 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_33 = happySpecReduce_3  17 happyReduction_33
happyReduction_33 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_34 = happySpecReduce_3  17 happyReduction_34
happyReduction_34 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_35 = happySpecReduce_3  17 happyReduction_35
happyReduction_35 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_36 = happySpecReduce_3  17 happyReduction_36
happyReduction_36 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_37 = happySpecReduce_3  17 happyReduction_37
happyReduction_37 _
	_
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_38 = happySpecReduce_2  17 happyReduction_38
happyReduction_38 _
	_
	 =  HappyAbsSyn17
		 (0
	)

happyReduce_39 = happyReduce 6 17 happyReduction_39
happyReduction_39 (_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn17
		 (0
	) `HappyStk` happyRest

happyReduce_40 = happyReduce 4 17 happyReduction_40
happyReduction_40 (_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn17
		 (0
	) `HappyStk` happyRest

happyReduce_41 = happySpecReduce_1  18 happyReduction_41
happyReduction_41 _
	 =  HappyAbsSyn18
		 (0
	)

happyReduce_42 = happySpecReduce_3  18 happyReduction_42
happyReduction_42 _
	_
	_
	 =  HappyAbsSyn18
		 (0
	)

happyNewToken action sts stk [] =
	action 45 45 notHappyAtAll (HappyState action) sts stk []

happyNewToken action sts stk (tk:tks) =
	let cont i = action i i tk (HappyState action) sts stk tks in
	case tk of {
	Class happy_dollar_dollar -> cont 19;
	Def happy_dollar_dollar -> cont 20;
	Extends happy_dollar_dollar -> cont 21;
	Plus -> cont 22;
	Minus -> cont 23;
	Times -> cont 24;
	Divide -> cont 25;
	Equals -> cont 26;
	Atmost -> cont 27;
	Atleast -> cont 28;
	Less -> cont 29;
	More -> cont 30;
	And -> cont 31;
	Or -> cont 32;
	Not -> cont 33;
	LeftCurly happy_dollar_dollar -> cont 34;
	RightCurly happy_dollar_dollar -> cont 35;
	LeftPar happy_dollar_dollar -> cont 36;
	RightPar happy_dollar_dollar -> cont 37;
	Comma happy_dollar_dollar -> cont 38;
	Semi -> cont 39;
	Period -> cont 40;
	Colon happy_dollar_dollar -> cont 41;
	Id happy_dollar_dollar -> cont 42;
	Qint happy_dollar_dollar -> cont 43;
	QString happy_dollar_dollar -> cont 44;
	_ -> happyError' (tk:tks)
	}

happyError_ 45 tk tks = happyError' tks
happyError_ _ tk tks = happyError' (tk:tks)

newtype HappyIdentity a = HappyIdentity a
happyIdentity = HappyIdentity
happyRunIdentity (HappyIdentity a) = a

instance Functor HappyIdentity where
    fmap f (HappyIdentity a) = HappyIdentity (f a)

instance Applicative HappyIdentity where
    pure  = return
    (<*>) = ap
instance Monad HappyIdentity where
    return = HappyIdentity
    (HappyIdentity p) >>= q = q p

happyThen :: () => HappyIdentity a -> (a -> HappyIdentity b) -> HappyIdentity b
happyThen = (>>=)
happyReturn :: () => a -> HappyIdentity a
happyReturn = (return)
happyThen1 m k tks = (>>=) m (\a -> k a tks)
happyReturn1 :: () => a -> b -> HappyIdentity a
happyReturn1 = \a tks -> (return) a
happyError' :: () => [(Token)] -> HappyIdentity a
happyError' = HappyIdentity . parseError

calc tks = happyRunIdentity happySomeParser where
  happySomeParser = happyThen (happyParse action_0 tks) (\x -> case x of {HappyAbsSyn4 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


parseError :: [Token] -> a
parseError _ = error "Parse error"


--data type for parsed expr ex
--data ActualArgs = RExp | ActualArgs Exp


--data struct for tokens   (already in lexer)

--main = getContents >>= print . calc . lexer

parse = getContents >>= print . calc . Lexer.lex

main = parse
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "<built-in>" #-}
{-# LINE 1 "<command-line>" #-}
{-# LINE 8 "<command-line>" #-}
# 1 "/usr/include/stdc-predef.h" 1 3 4

# 17 "/usr/include/stdc-predef.h" 3 4










































{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "/usr/lib/ghc/include/ghcversion.h" #-}

















{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
-- Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp 

{-# LINE 13 "templates/GenericTemplate.hs" #-}

{-# LINE 46 "templates/GenericTemplate.hs" #-}








{-# LINE 67 "templates/GenericTemplate.hs" #-}

{-# LINE 77 "templates/GenericTemplate.hs" #-}

{-# LINE 86 "templates/GenericTemplate.hs" #-}

infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is (1), it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
        happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
         (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action

{-# LINE 155 "templates/GenericTemplate.hs" #-}

-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k - ((1) :: Int)) sts of
         sts1@(((st1@(HappyState (action))):(_))) ->
                let r = fn stk in  -- it doesn't hurt to always seq here...
                happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction

{-# LINE 256 "templates/GenericTemplate.hs" #-}
happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery ((1) is the error token)

-- parse error if we are in recovery and we fail again
happyFail (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--      trace "failing" $ 
        happyError_ i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  (1) tk old_st (((HappyState (action))):(sts)) 
                                                (saved_tok `HappyStk` _ `HappyStk` stk) =
--      trace ("discarding state, depth " ++ show (length stk))  $
        action (1) (1) tk (HappyState (action)) sts ((saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail  i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
        action (1) (1) tk (HappyState (action)) sts ( (HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--      happySeq = happyDoSeq
-- otherwise it emits
--      happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.

{-# LINE 322 "templates/GenericTemplate.hs" #-}
{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
