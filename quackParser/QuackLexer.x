{
module QuackLexer (QuackLexer.lex,Token(..),filterTokens,happyScanner) where
--module Main where

import Control.Monad
import qualified Data.Text as Text
import Data.String 

}

%wrapper "monadUserState"

$digit = 0-9 
$alpha = [a-zA-Z]

$innerQuote = [^\n"]

$eQuote = \"
$quotable = $printable  # [\n " \\]

$escapables = [0 b t n r f " \\]
$unescapables = ~ [0 b t n r f " \\]

tokens :-
    <0> \n                              ;
    <0>  $white                         ;
    <0> "class"                         { getValToken  $ \s -> Class s  }
    <0> "def"                           { getValToken  $ \s -> Def s }
    <0>  "extends"                      { getValToken  $ \s -> Extends s }
    <0>  "if"                           { getValToken  $ \s -> If s }
    <0>  "elif"                         { getValToken  $ \s -> Elif s }
    <0>  "else"                         { getValToken  $ \s -> Else s }
    <0>  "while"                        { getValToken  $ \s -> While s }
    <0>  "return"                       { getValToken  $ \s -> Return s }

    <0>  "="                            { getValToken $ \s -> Gets s}
    <0>  "+"                            { getValToken  $ \s -> Plus s }
    <0>  "-"                            { getValToken  $ \s -> Minus s }
    <0>  "*"                            { getValToken  $ \s -> Times s }
    <0>  "/"                            { getValToken  $ \s -> Divide s }
    <0> "=="                            { getValToken  $ \s -> Equals s }
    <0>  "<="                           { getValToken  $ \s -> Atmost s }
    <0>  ">="                           { getValToken  $ \s -> Atleast s }
    <0> "<"                             { getValToken  $ \s -> Less s }
    <0>  ">"                            { getValToken  $ \s -> More s }
    <0>  "or"                           { getValToken  $ \s -> Or s }
    <0>  "and"                          { getValToken  $ \s -> And s }
    <0>  "not"                          { getValToken  $ \s -> Not s }
    <0>  "{"                            { getValToken  $ \s -> LeftCurly s }
    <0>  "}"                            { getValToken  $ \s -> RightCurly s }
    <0>  "("                            { getValToken  $ \s -> LeftPar s }
    <0>  ")"                            { getValToken  $ \s -> RightPar s }
    <0>  ","                            { getValToken  $ \s -> Comma s }
    <0>  ";"                            { getValToken  $ \s -> Semi s }
    <0>  "."                            { getValToken  $ \s -> Period s }
    <0>  ":"                            { getValToken  $ \s -> Colon s }

    <0>  [_$alpha]+[_$alpha$digit]*     { getValToken $ \s -> Id s  }
    <0>   \-?$digit+                       { getValToken $ \s -> Qint s  }


    <0>  "//"                           { begin linecomment }
    <linecomment>  [^\n]*               { getValToken $ \s -> Comment s }
    <linecomment> \n                    { begin 0 }

    <0>  "/*"                           { begin blockcomment }
    <blockcomment> "*/"                 { returnComment `andBegin` 0 }
    <blockcomment> \n                   { readStrChar }
    <blockcomment> $printable           { readStrChar }

    <0> "                               { begin string }
    <string> "                          { returnString `andBegin` 0 }
    <string>  $quotable                 { readStrChar }
    <string>  \\ $escapables            { readEscapeCode }



    <0> "{3}                            { begin doublestring }
    <doublestring> \n                   { readStrChar }
    <doublestring> $printable           { readStrChar }
    <doublestring> "{3}                 { returnString `andBegin` 0 }
{


data Token = 
    -- KEYWORDS
    Class String       |
    Def String         |
    Extends String     |
    If String          |
    Elif String        |
    Else String        |
    While String       |
    Return String      |

    -- PUNCTUATION
    Gets String        |
    Plus String        |
    Minus String       |
    Times String       |
    Divide String      |
    Equals String      |
    Atmost String      |
    Atleast String     |
    Less String        |
    More String        |
    And String         |
    Or String          |
    Not String         |
    LeftCurly String   |
    RightCurly String  |
    LeftPar String     |
    RightPar String    |
    Comma String       |
    Semi String        |
    Period String      |
    Colon String       |

    -- IDENTIFIERS  
    Id String          |

    -- INT LITS
    Qint String        |

    -- COMMENTS
    Comment String     |
    BlockComment String|
    -- STRING LITS 
    SingleQuote        |
    
    QString String     |
    EscapeCode  String |

    EOF                |
    IGNORE             |
    Newline Int
    deriving (Eq,Show)


alexEOF :: Alex Token
alexEOF = return EOF

data AlexUserState  = AlexUserState { lineNo :: Int, stateStack :: [Int],
                                        curString :: String, curComment :: String }

alexInitUserState :: AlexUserState
alexInitUserState = AlexUserState { lineNo = 1, stateStack = [], curString = "", curComment = "" }


getNewLineCount :: Alex Int
getNewLineCount = Alex $ \s@AlexState{alex_ust=ust} -> Right (s, lineNo ust)

setNewLineCount :: Int -> Alex ()
setNewLineCount ss = Alex $ \s -> Right (s{alex_ust=(alex_ust s){lineNo=ss}}, ()) 



getStrChar ::  Alex ()
getStrChar = Alex $ \s -> Right $ let ust = alex_ust s in
                                        let cur = curString ust in
                                            case cur of
                                                "" -> ( s{alex_ust=ust{curString= [alex_chr s]}}, () )
                                                _ -> ( s{alex_ust=ust{curString= cur++[(alex_chr s)]}}, () )
                                            
saveStrChar :: Alex String
saveStrChar = Alex $ \s@AlexState{alex_ust=ust} -> Right (s, curString ust)

clearStrChar :: Alex ()
clearStrChar = Alex $ \s -> Right $ let ust = alex_ust s in
                                        let cur = curString ust in
                                            ( s{alex_ust=ust{curString=""}}, () )

--Need a way to call read... without returning IGNORE token
-- String -> Int -> Alex Ignore ?
readStrChar input len = do 
                            getStrChar
                            return $ IGNORE

-- String -> Int -> Alex QString ?
returnString input len  = do
                            str <- saveStrChar
                            clearStrChar
                            return $ QString str





getRestOfInput :: Alex String
getRestOfInput = Alex $ \s@AlexState{alex_inp=inp} -> Right (s, inp)


getAlexLineNo :: Alex Int
getAlexLineNo = Alex $ \s@AlexState{alex_pos=pos} -> Right (s, let AlexPn off lineno colno = pos in lineno)

getEscapeCode :: String -> Alex ()
getEscapeCode esc = Alex $ \s -> Right $ let ust = alex_ust s in
                                        let cur = curString ust in
                                            ( s{alex_ust=ust{curString=cur ++ esc}}, () )










-- String -> Int -> Alex Ignore ?
readEscapeCode input len = let (_,_,_,s) = input in 
                            do
                                getEscapeCode (take len s) 
                                return $ IGNORE



-- String -> Int -> Alex Comment ?
returnComment input len  = do
                            str <- saveStrChar
                            clearStrChar
                            return $ Comment str



incNewlineCount ::   Alex ()
incNewlineCount   = Alex $ \s -> Right ( s{alex_ust=(alex_ust s){lineNo=((lineNo (alex_ust s))+1)}}, () )


countNewline input len =
    do incNewlineCount
       lineCount <- getNewLineCount
       return $ Newline lineCount

blockStrNewLine input len =
    do incNewlineCount
       getStrChar
       return $ IGNORE

getToken f  = \input -> \len -> return $  f

getValToken f = \input -> \len -> let (_,_,_,s) = input in return $ f (take len s)



scanner :: String -> Either String [Token]
scanner str =
    let loop = do
        tok <- alexMonadScan
        if tok == EOF
            then return []
        else 
            do 
                toks <- loop; 
                return (tok:toks)
    in runAlex str loop


happyScanner :: String -> Either String (Token, String, Int)
happyScanner  str = 
    let scan = do
                tok <- alexMonadScan
                inp <- getRestOfInput
                lineNo <- getAlexLineNo
                if tok == IGNORE
                then scan
                else
                    return $ (case tok of
                                    EOF ->  (EOF,"",lineNo)
                                    _ ->   (tok,inp,lineNo))
    in runAlex str scan
{-                if tok == EOF
                  then return (EOF,"",lineNo)
                else
                  return $ (tok,inp,lineNo)-}


filtercomm x = let (_,b) = x in 
                    case b of
                        Comment c -> False
                        BlockComment c -> False
                        _ -> True





filterComments s = filter  filtercomm (s) 



mapTups s = map (\x -> (0,x)) s 
 

cnl :: [(Int, Token)] -> Int -> [(Int, Token)]
cnl toks curLine  = case toks of
                [] -> []
                (t:ts) -> let (n,token) = t in
                            case token of
                                Newline m -> (cnl ts m)
                                thing -> ((curLine,thing):(cnl ts curLine))

countNewlines s = cnl (s) 1


stripIgnores :: [(Int, Token)] -> [(Int, Token)]
stripIgnores toks = case toks of
                        [] -> []
                        (t:ts) -> let (n,token) = t in
                                    case token of
                                        IGNORE -> stripIgnores ts
                                        thing -> t:(stripIgnores ts)

formatPrint s =  case s of
                    t:ts -> [putStr "\n", print t] ++ (formatPrint ts)
                    [] -> []


--printTokens = mapM_ putStrLn

{-printStuff :: Either String [(Token)] -> IO()
printStuff s = case s of 
                Left x -> print x
                Right toks -> printTokens $  stringify (   ( filterComments (countNewlines ( stripIgnores ( mapTups  toks)))))-}


filterTokens = map (\s -> let (a,b) = s in b)

lex :: String -> [Token] 
lex s = let i = scanner s in 
                    case i of
                        Left x -> []
                        Right toks -> filterTokens( filterComments  ( countNewlines ( stripIgnores ( mapTups  toks) ) )  )

{- fixBlockTags s = map (\a b -> case b of
                        QString s -> (a,QString $  s)
                        BlockComment s -> (a,BlockComment $ "/*" ++ s ++ "*/")
                        thing -> (a,thing)) s -}

stringify s = map (\a b -> (show a) ++ "\t" ++ (show b)) s



main = do
    s <- getContents
    --return s
    print $ happyScanner s
}

