{-# OPTIONS_GHC -w #-}
module Main where
import QuackLexer
import Control.Applicative(Applicative(..))
import Control.Monad (ap)

-- parser produced by Happy Version 1.19.5

data HappyAbsSyn t4 t5 t6 t7 t8 t9 t10 t11 t12 t13 t14 t15 t16 t17 t18 t19 t20 t21 t22
	= HappyTerminal (Token)
	| HappyErrorToken Int
	| HappyAbsSyn4 t4
	| HappyAbsSyn5 t5
	| HappyAbsSyn6 t6
	| HappyAbsSyn7 t7
	| HappyAbsSyn8 t8
	| HappyAbsSyn9 t9
	| HappyAbsSyn10 t10
	| HappyAbsSyn11 t11
	| HappyAbsSyn12 t12
	| HappyAbsSyn13 t13
	| HappyAbsSyn14 t14
	| HappyAbsSyn15 t15
	| HappyAbsSyn16 t16
	| HappyAbsSyn17 t17
	| HappyAbsSyn18 t18
	| HappyAbsSyn19 t19
	| HappyAbsSyn20 t20
	| HappyAbsSyn21 t21
	| HappyAbsSyn22 t22

action_0 (23) = happyShift action_5
action_0 (26) = happyShift action_17
action_0 (29) = happyShift action_18
action_0 (30) = happyShift action_19
action_0 (43) = happyShift action_20
action_0 (46) = happyShift action_21
action_0 (52) = happyShift action_22
action_0 (53) = happyShift action_23
action_0 (54) = happyShift action_24
action_0 (4) = happyGoto action_6
action_0 (5) = happyGoto action_2
action_0 (6) = happyGoto action_7
action_0 (7) = happyGoto action_4
action_0 (12) = happyGoto action_8
action_0 (13) = happyGoto action_9
action_0 (15) = happyGoto action_10
action_0 (16) = happyGoto action_11
action_0 (17) = happyGoto action_12
action_0 (18) = happyGoto action_13
action_0 (19) = happyGoto action_14
action_0 (20) = happyGoto action_15
action_0 (21) = happyGoto action_16
action_0 _ = happyReduce_4

action_1 (23) = happyShift action_5
action_1 (5) = happyGoto action_2
action_1 (6) = happyGoto action_3
action_1 (7) = happyGoto action_4
action_1 _ = happyFail

action_2 _ = happyReduce_6

action_3 (23) = happyShift action_5
action_3 (26) = happyShift action_17
action_3 (29) = happyShift action_18
action_3 (30) = happyShift action_19
action_3 (43) = happyShift action_20
action_3 (46) = happyShift action_21
action_3 (52) = happyShift action_22
action_3 (53) = happyShift action_23
action_3 (54) = happyShift action_24
action_3 (5) = happyGoto action_49
action_3 (7) = happyGoto action_4
action_3 (12) = happyGoto action_8
action_3 (13) = happyGoto action_50
action_3 (15) = happyGoto action_10
action_3 (16) = happyGoto action_11
action_3 (17) = happyGoto action_12
action_3 (18) = happyGoto action_13
action_3 (19) = happyGoto action_14
action_3 (20) = happyGoto action_15
action_3 (21) = happyGoto action_16
action_3 _ = happyFail

action_4 (44) = happyShift action_53
action_4 (9) = happyGoto action_52
action_4 _ = happyFail

action_5 (52) = happyShift action_51
action_5 _ = happyFail

action_6 (55) = happyAccept
action_6 _ = happyFail

action_7 (23) = happyShift action_5
action_7 (26) = happyShift action_17
action_7 (29) = happyShift action_18
action_7 (30) = happyShift action_19
action_7 (43) = happyShift action_20
action_7 (46) = happyShift action_21
action_7 (52) = happyShift action_22
action_7 (53) = happyShift action_23
action_7 (54) = happyShift action_24
action_7 (5) = happyGoto action_49
action_7 (7) = happyGoto action_4
action_7 (12) = happyGoto action_8
action_7 (13) = happyGoto action_50
action_7 (15) = happyGoto action_10
action_7 (16) = happyGoto action_11
action_7 (17) = happyGoto action_12
action_7 (18) = happyGoto action_13
action_7 (19) = happyGoto action_14
action_7 (20) = happyGoto action_15
action_7 (21) = happyGoto action_16
action_7 _ = happyReduce_2

action_8 _ = happyReduce_26

action_9 (26) = happyShift action_17
action_9 (29) = happyShift action_18
action_9 (30) = happyShift action_19
action_9 (43) = happyShift action_20
action_9 (46) = happyShift action_21
action_9 (52) = happyShift action_22
action_9 (53) = happyShift action_23
action_9 (54) = happyShift action_24
action_9 (12) = happyGoto action_48
action_9 (15) = happyGoto action_10
action_9 (16) = happyGoto action_11
action_9 (17) = happyGoto action_12
action_9 (18) = happyGoto action_13
action_9 (19) = happyGoto action_14
action_9 (20) = happyGoto action_15
action_9 (21) = happyGoto action_16
action_9 _ = happyReduce_3

action_10 _ = happyReduce_22

action_11 _ = happyReduce_21

action_12 _ = happyReduce_23

action_13 _ = happyReduce_24

action_14 (35) = happyShift action_46
action_14 (51) = happyShift action_47
action_14 _ = happyReduce_44

action_15 _ = happyReduce_25

action_16 (31) = happyShift action_33
action_16 (32) = happyShift action_34
action_16 (33) = happyShift action_35
action_16 (34) = happyShift action_36
action_16 (36) = happyShift action_37
action_16 (37) = happyShift action_38
action_16 (38) = happyShift action_39
action_16 (39) = happyShift action_40
action_16 (40) = happyShift action_41
action_16 (41) = happyShift action_42
action_16 (42) = happyShift action_43
action_16 (49) = happyShift action_44
action_16 (50) = happyShift action_45
action_16 _ = happyFail

action_17 (43) = happyShift action_20
action_17 (46) = happyShift action_21
action_17 (52) = happyShift action_22
action_17 (53) = happyShift action_23
action_17 (54) = happyShift action_24
action_17 (19) = happyGoto action_26
action_17 (21) = happyGoto action_32
action_17 _ = happyFail

action_18 (43) = happyShift action_20
action_18 (46) = happyShift action_21
action_18 (52) = happyShift action_22
action_18 (53) = happyShift action_23
action_18 (54) = happyShift action_24
action_18 (19) = happyGoto action_26
action_18 (21) = happyGoto action_31
action_18 _ = happyFail

action_19 (43) = happyShift action_20
action_19 (46) = happyShift action_21
action_19 (49) = happyShift action_30
action_19 (52) = happyShift action_22
action_19 (53) = happyShift action_23
action_19 (54) = happyShift action_24
action_19 (19) = happyGoto action_26
action_19 (21) = happyGoto action_29
action_19 _ = happyFail

action_20 (43) = happyShift action_20
action_20 (46) = happyShift action_21
action_20 (52) = happyShift action_22
action_20 (53) = happyShift action_23
action_20 (54) = happyShift action_24
action_20 (19) = happyGoto action_26
action_20 (21) = happyGoto action_28
action_20 _ = happyFail

action_21 (43) = happyShift action_20
action_21 (46) = happyShift action_21
action_21 (52) = happyShift action_22
action_21 (53) = happyShift action_23
action_21 (54) = happyShift action_24
action_21 (19) = happyGoto action_26
action_21 (21) = happyGoto action_27
action_21 _ = happyFail

action_22 (46) = happyShift action_25
action_22 _ = happyReduce_39

action_23 _ = happyReduce_43

action_24 _ = happyReduce_42

action_25 (43) = happyShift action_20
action_25 (46) = happyShift action_21
action_25 (47) = happyShift action_81
action_25 (52) = happyShift action_22
action_25 (53) = happyShift action_23
action_25 (54) = happyShift action_24
action_25 (19) = happyGoto action_26
action_25 (21) = happyGoto action_79
action_25 (22) = happyGoto action_80
action_25 _ = happyFail

action_26 _ = happyReduce_44

action_27 (31) = happyShift action_33
action_27 (32) = happyShift action_34
action_27 (33) = happyShift action_35
action_27 (34) = happyShift action_36
action_27 (36) = happyShift action_37
action_27 (37) = happyShift action_38
action_27 (38) = happyShift action_39
action_27 (39) = happyShift action_40
action_27 (40) = happyShift action_41
action_27 (41) = happyShift action_42
action_27 (42) = happyShift action_43
action_27 (47) = happyShift action_78
action_27 (50) = happyShift action_45
action_27 _ = happyFail

action_28 (31) = happyShift action_33
action_28 (32) = happyShift action_34
action_28 (33) = happyShift action_35
action_28 (34) = happyShift action_36
action_28 (36) = happyShift action_37
action_28 (37) = happyShift action_38
action_28 (38) = happyShift action_39
action_28 (39) = happyShift action_40
action_28 (40) = happyShift action_41
action_28 (41) = happyShift action_42
action_28 (42) = happyShift action_43
action_28 (50) = happyShift action_45
action_28 _ = happyReduce_57

action_29 (31) = happyShift action_33
action_29 (32) = happyShift action_34
action_29 (33) = happyShift action_35
action_29 (34) = happyShift action_36
action_29 (36) = happyShift action_37
action_29 (37) = happyShift action_38
action_29 (38) = happyShift action_39
action_29 (39) = happyShift action_40
action_29 (40) = happyShift action_41
action_29 (41) = happyShift action_42
action_29 (42) = happyShift action_43
action_29 (49) = happyShift action_77
action_29 (50) = happyShift action_45
action_29 _ = happyFail

action_30 _ = happyReduce_35

action_31 (31) = happyShift action_33
action_31 (32) = happyShift action_34
action_31 (33) = happyShift action_35
action_31 (34) = happyShift action_36
action_31 (36) = happyShift action_37
action_31 (37) = happyShift action_38
action_31 (38) = happyShift action_39
action_31 (39) = happyShift action_40
action_31 (40) = happyShift action_41
action_31 (41) = happyShift action_42
action_31 (42) = happyShift action_43
action_31 (44) = happyShift action_75
action_31 (50) = happyShift action_45
action_31 (14) = happyGoto action_76
action_31 _ = happyFail

action_32 (31) = happyShift action_33
action_32 (32) = happyShift action_34
action_32 (33) = happyShift action_35
action_32 (34) = happyShift action_36
action_32 (36) = happyShift action_37
action_32 (37) = happyShift action_38
action_32 (38) = happyShift action_39
action_32 (39) = happyShift action_40
action_32 (40) = happyShift action_41
action_32 (41) = happyShift action_42
action_32 (42) = happyShift action_43
action_32 (44) = happyShift action_75
action_32 (50) = happyShift action_45
action_32 (14) = happyGoto action_74
action_32 _ = happyFail

action_33 (43) = happyShift action_20
action_33 (46) = happyShift action_21
action_33 (52) = happyShift action_22
action_33 (53) = happyShift action_23
action_33 (54) = happyShift action_24
action_33 (19) = happyGoto action_26
action_33 (21) = happyGoto action_73
action_33 _ = happyFail

action_34 (43) = happyShift action_20
action_34 (46) = happyShift action_21
action_34 (52) = happyShift action_22
action_34 (53) = happyShift action_23
action_34 (54) = happyShift action_24
action_34 (19) = happyGoto action_26
action_34 (21) = happyGoto action_72
action_34 _ = happyFail

action_35 (43) = happyShift action_20
action_35 (46) = happyShift action_21
action_35 (52) = happyShift action_22
action_35 (53) = happyShift action_23
action_35 (54) = happyShift action_24
action_35 (19) = happyGoto action_26
action_35 (21) = happyGoto action_71
action_35 _ = happyFail

action_36 (43) = happyShift action_20
action_36 (46) = happyShift action_21
action_36 (52) = happyShift action_22
action_36 (53) = happyShift action_23
action_36 (54) = happyShift action_24
action_36 (19) = happyGoto action_26
action_36 (21) = happyGoto action_70
action_36 _ = happyFail

action_37 (43) = happyShift action_20
action_37 (46) = happyShift action_21
action_37 (52) = happyShift action_22
action_37 (53) = happyShift action_23
action_37 (54) = happyShift action_24
action_37 (19) = happyGoto action_26
action_37 (21) = happyGoto action_69
action_37 _ = happyFail

action_38 (43) = happyShift action_20
action_38 (46) = happyShift action_21
action_38 (52) = happyShift action_22
action_38 (53) = happyShift action_23
action_38 (54) = happyShift action_24
action_38 (19) = happyGoto action_26
action_38 (21) = happyGoto action_68
action_38 _ = happyFail

action_39 (43) = happyShift action_20
action_39 (46) = happyShift action_21
action_39 (52) = happyShift action_22
action_39 (53) = happyShift action_23
action_39 (54) = happyShift action_24
action_39 (19) = happyGoto action_26
action_39 (21) = happyGoto action_67
action_39 _ = happyFail

action_40 (43) = happyShift action_20
action_40 (46) = happyShift action_21
action_40 (52) = happyShift action_22
action_40 (53) = happyShift action_23
action_40 (54) = happyShift action_24
action_40 (19) = happyGoto action_26
action_40 (21) = happyGoto action_66
action_40 _ = happyFail

action_41 (43) = happyShift action_20
action_41 (46) = happyShift action_21
action_41 (52) = happyShift action_22
action_41 (53) = happyShift action_23
action_41 (54) = happyShift action_24
action_41 (19) = happyGoto action_26
action_41 (21) = happyGoto action_65
action_41 _ = happyFail

action_42 (43) = happyShift action_20
action_42 (46) = happyShift action_21
action_42 (52) = happyShift action_22
action_42 (53) = happyShift action_23
action_42 (54) = happyShift action_24
action_42 (19) = happyGoto action_26
action_42 (21) = happyGoto action_64
action_42 _ = happyFail

action_43 (43) = happyShift action_20
action_43 (46) = happyShift action_21
action_43 (52) = happyShift action_22
action_43 (53) = happyShift action_23
action_43 (54) = happyShift action_24
action_43 (19) = happyGoto action_26
action_43 (21) = happyGoto action_63
action_43 _ = happyFail

action_44 _ = happyReduce_41

action_45 (52) = happyShift action_62
action_45 _ = happyFail

action_46 (43) = happyShift action_20
action_46 (46) = happyShift action_21
action_46 (52) = happyShift action_22
action_46 (53) = happyShift action_23
action_46 (54) = happyShift action_24
action_46 (19) = happyGoto action_26
action_46 (21) = happyGoto action_61
action_46 _ = happyFail

action_47 (52) = happyShift action_60
action_47 _ = happyFail

action_48 _ = happyReduce_27

action_49 _ = happyReduce_7

action_50 (26) = happyShift action_17
action_50 (29) = happyShift action_18
action_50 (30) = happyShift action_19
action_50 (43) = happyShift action_20
action_50 (46) = happyShift action_21
action_50 (52) = happyShift action_22
action_50 (53) = happyShift action_23
action_50 (54) = happyShift action_24
action_50 (12) = happyGoto action_48
action_50 (15) = happyGoto action_10
action_50 (16) = happyGoto action_11
action_50 (17) = happyGoto action_12
action_50 (18) = happyGoto action_13
action_50 (19) = happyGoto action_14
action_50 (20) = happyGoto action_15
action_50 (21) = happyGoto action_16
action_50 _ = happyReduce_1

action_51 (46) = happyShift action_59
action_51 _ = happyFail

action_52 _ = happyReduce_5

action_53 (24) = happyShift action_57
action_53 (26) = happyShift action_17
action_53 (29) = happyShift action_18
action_53 (30) = happyShift action_19
action_53 (43) = happyShift action_20
action_53 (45) = happyShift action_58
action_53 (46) = happyShift action_21
action_53 (52) = happyShift action_22
action_53 (53) = happyShift action_23
action_53 (54) = happyShift action_24
action_53 (10) = happyGoto action_54
action_53 (11) = happyGoto action_55
action_53 (12) = happyGoto action_8
action_53 (13) = happyGoto action_56
action_53 (15) = happyGoto action_10
action_53 (16) = happyGoto action_11
action_53 (17) = happyGoto action_12
action_53 (18) = happyGoto action_13
action_53 (19) = happyGoto action_14
action_53 (20) = happyGoto action_15
action_53 (21) = happyGoto action_16
action_53 _ = happyFail

action_54 _ = happyReduce_19

action_55 (24) = happyShift action_57
action_55 (45) = happyShift action_97
action_55 (10) = happyGoto action_96
action_55 _ = happyFail

action_56 (24) = happyShift action_57
action_56 (26) = happyShift action_17
action_56 (29) = happyShift action_18
action_56 (30) = happyShift action_19
action_56 (43) = happyShift action_20
action_56 (45) = happyShift action_95
action_56 (46) = happyShift action_21
action_56 (52) = happyShift action_22
action_56 (53) = happyShift action_23
action_56 (54) = happyShift action_24
action_56 (10) = happyGoto action_54
action_56 (11) = happyGoto action_94
action_56 (12) = happyGoto action_48
action_56 (15) = happyGoto action_10
action_56 (16) = happyGoto action_11
action_56 (17) = happyGoto action_12
action_56 (18) = happyGoto action_13
action_56 (19) = happyGoto action_14
action_56 (20) = happyGoto action_15
action_56 (21) = happyGoto action_16
action_56 _ = happyFail

action_57 (52) = happyShift action_93
action_57 _ = happyFail

action_58 _ = happyReduce_16

action_59 (52) = happyShift action_92
action_59 (8) = happyGoto action_91
action_59 _ = happyReduce_10

action_60 (35) = happyShift action_90
action_60 _ = happyFail

action_61 (31) = happyShift action_33
action_61 (32) = happyShift action_34
action_61 (33) = happyShift action_35
action_61 (34) = happyShift action_36
action_61 (36) = happyShift action_37
action_61 (37) = happyShift action_38
action_61 (38) = happyShift action_39
action_61 (39) = happyShift action_40
action_61 (40) = happyShift action_41
action_61 (41) = happyShift action_42
action_61 (42) = happyShift action_43
action_61 (49) = happyShift action_89
action_61 (50) = happyShift action_45
action_61 _ = happyFail

action_62 (46) = happyShift action_88
action_62 _ = happyReduce_40

action_63 (31) = happyShift action_33
action_63 (32) = happyShift action_34
action_63 (33) = happyShift action_35
action_63 (34) = happyShift action_36
action_63 (36) = happyShift action_37
action_63 (37) = happyShift action_38
action_63 (38) = happyShift action_39
action_63 (39) = happyShift action_40
action_63 (40) = happyShift action_41
action_63 (50) = happyShift action_45
action_63 _ = happyReduce_56

action_64 (31) = happyShift action_33
action_64 (32) = happyShift action_34
action_64 (33) = happyShift action_35
action_64 (34) = happyShift action_36
action_64 (36) = happyShift action_37
action_64 (37) = happyShift action_38
action_64 (38) = happyShift action_39
action_64 (39) = happyShift action_40
action_64 (40) = happyShift action_41
action_64 (50) = happyShift action_45
action_64 _ = happyReduce_55

action_65 (31) = happyShift action_33
action_65 (32) = happyShift action_34
action_65 (33) = happyShift action_35
action_65 (34) = happyShift action_36
action_65 (36) = happyFail
action_65 (37) = happyFail
action_65 (38) = happyFail
action_65 (39) = happyFail
action_65 (40) = happyFail
action_65 (50) = happyShift action_45
action_65 _ = happyReduce_54

action_66 (31) = happyShift action_33
action_66 (32) = happyShift action_34
action_66 (33) = happyShift action_35
action_66 (34) = happyShift action_36
action_66 (36) = happyFail
action_66 (37) = happyFail
action_66 (38) = happyFail
action_66 (39) = happyFail
action_66 (40) = happyFail
action_66 (50) = happyShift action_45
action_66 _ = happyReduce_53

action_67 (31) = happyShift action_33
action_67 (32) = happyShift action_34
action_67 (33) = happyShift action_35
action_67 (34) = happyShift action_36
action_67 (36) = happyFail
action_67 (37) = happyFail
action_67 (38) = happyFail
action_67 (39) = happyFail
action_67 (40) = happyFail
action_67 (50) = happyShift action_45
action_67 _ = happyReduce_52

action_68 (31) = happyShift action_33
action_68 (32) = happyShift action_34
action_68 (33) = happyShift action_35
action_68 (34) = happyShift action_36
action_68 (36) = happyFail
action_68 (37) = happyFail
action_68 (38) = happyFail
action_68 (39) = happyFail
action_68 (40) = happyFail
action_68 (50) = happyShift action_45
action_68 _ = happyReduce_51

action_69 (31) = happyShift action_33
action_69 (32) = happyShift action_34
action_69 (33) = happyShift action_35
action_69 (34) = happyShift action_36
action_69 (36) = happyFail
action_69 (37) = happyFail
action_69 (38) = happyFail
action_69 (39) = happyFail
action_69 (40) = happyFail
action_69 (50) = happyShift action_45
action_69 _ = happyReduce_50

action_70 (50) = happyShift action_45
action_70 _ = happyReduce_49

action_71 (50) = happyShift action_45
action_71 _ = happyReduce_48

action_72 (33) = happyShift action_35
action_72 (34) = happyShift action_36
action_72 (50) = happyShift action_45
action_72 _ = happyReduce_47

action_73 (33) = happyShift action_35
action_73 (34) = happyShift action_36
action_73 (50) = happyShift action_45
action_73 _ = happyReduce_46

action_74 (27) = happyShift action_86
action_74 (28) = happyShift action_87
action_74 _ = happyReduce_31

action_75 (26) = happyShift action_17
action_75 (29) = happyShift action_18
action_75 (30) = happyShift action_19
action_75 (43) = happyShift action_20
action_75 (45) = happyShift action_85
action_75 (46) = happyShift action_21
action_75 (52) = happyShift action_22
action_75 (53) = happyShift action_23
action_75 (54) = happyShift action_24
action_75 (12) = happyGoto action_8
action_75 (13) = happyGoto action_84
action_75 (15) = happyGoto action_10
action_75 (16) = happyGoto action_11
action_75 (17) = happyGoto action_12
action_75 (18) = happyGoto action_13
action_75 (19) = happyGoto action_14
action_75 (20) = happyGoto action_15
action_75 (21) = happyGoto action_16
action_75 _ = happyFail

action_76 _ = happyReduce_30

action_77 _ = happyReduce_36

action_78 _ = happyReduce_45

action_79 (31) = happyShift action_33
action_79 (32) = happyShift action_34
action_79 (33) = happyShift action_35
action_79 (34) = happyShift action_36
action_79 (36) = happyShift action_37
action_79 (37) = happyShift action_38
action_79 (38) = happyShift action_39
action_79 (39) = happyShift action_40
action_79 (40) = happyShift action_41
action_79 (41) = happyShift action_42
action_79 (42) = happyShift action_43
action_79 (50) = happyShift action_45
action_79 _ = happyReduce_62

action_80 (47) = happyShift action_82
action_80 (48) = happyShift action_83
action_80 _ = happyFail

action_81 _ = happyReduce_61

action_82 _ = happyReduce_60

action_83 (43) = happyShift action_20
action_83 (46) = happyShift action_21
action_83 (52) = happyShift action_22
action_83 (53) = happyShift action_23
action_83 (54) = happyShift action_24
action_83 (19) = happyGoto action_26
action_83 (21) = happyGoto action_108
action_83 _ = happyFail

action_84 (26) = happyShift action_17
action_84 (29) = happyShift action_18
action_84 (30) = happyShift action_19
action_84 (43) = happyShift action_20
action_84 (45) = happyShift action_107
action_84 (46) = happyShift action_21
action_84 (52) = happyShift action_22
action_84 (53) = happyShift action_23
action_84 (54) = happyShift action_24
action_84 (12) = happyGoto action_48
action_84 (15) = happyGoto action_10
action_84 (16) = happyGoto action_11
action_84 (17) = happyGoto action_12
action_84 (18) = happyGoto action_13
action_84 (19) = happyGoto action_14
action_84 (20) = happyGoto action_15
action_84 (21) = happyGoto action_16
action_84 _ = happyFail

action_85 _ = happyReduce_28

action_86 (43) = happyShift action_20
action_86 (46) = happyShift action_21
action_86 (52) = happyShift action_22
action_86 (53) = happyShift action_23
action_86 (54) = happyShift action_24
action_86 (19) = happyGoto action_26
action_86 (21) = happyGoto action_106
action_86 _ = happyFail

action_87 (44) = happyShift action_75
action_87 (14) = happyGoto action_105
action_87 _ = happyFail

action_88 (43) = happyShift action_20
action_88 (46) = happyShift action_21
action_88 (47) = happyShift action_104
action_88 (52) = happyShift action_22
action_88 (53) = happyShift action_23
action_88 (54) = happyShift action_24
action_88 (19) = happyGoto action_26
action_88 (21) = happyGoto action_79
action_88 (22) = happyGoto action_103
action_88 _ = happyFail

action_89 _ = happyReduce_37

action_90 (43) = happyShift action_20
action_90 (46) = happyShift action_21
action_90 (52) = happyShift action_22
action_90 (53) = happyShift action_23
action_90 (54) = happyShift action_24
action_90 (19) = happyGoto action_26
action_90 (21) = happyGoto action_102
action_90 _ = happyFail

action_91 (47) = happyShift action_101
action_91 _ = happyFail

action_92 (51) = happyShift action_100
action_92 _ = happyFail

action_93 (46) = happyShift action_99
action_93 _ = happyFail

action_94 (24) = happyShift action_57
action_94 (45) = happyShift action_98
action_94 (10) = happyGoto action_96
action_94 _ = happyFail

action_95 _ = happyReduce_13

action_96 _ = happyReduce_20

action_97 _ = happyReduce_14

action_98 _ = happyReduce_15

action_99 (52) = happyShift action_92
action_99 (8) = happyGoto action_114
action_99 _ = happyReduce_10

action_100 (52) = happyShift action_113
action_100 _ = happyFail

action_101 (25) = happyShift action_112
action_101 _ = happyReduce_8

action_102 (31) = happyShift action_33
action_102 (32) = happyShift action_34
action_102 (33) = happyShift action_35
action_102 (34) = happyShift action_36
action_102 (36) = happyShift action_37
action_102 (37) = happyShift action_38
action_102 (38) = happyShift action_39
action_102 (39) = happyShift action_40
action_102 (40) = happyShift action_41
action_102 (41) = happyShift action_42
action_102 (42) = happyShift action_43
action_102 (49) = happyShift action_111
action_102 (50) = happyShift action_45
action_102 _ = happyFail

action_103 (47) = happyShift action_110
action_103 (48) = happyShift action_83
action_103 _ = happyFail

action_104 _ = happyReduce_59

action_105 _ = happyReduce_33

action_106 (31) = happyShift action_33
action_106 (32) = happyShift action_34
action_106 (33) = happyShift action_35
action_106 (34) = happyShift action_36
action_106 (36) = happyShift action_37
action_106 (37) = happyShift action_38
action_106 (38) = happyShift action_39
action_106 (39) = happyShift action_40
action_106 (40) = happyShift action_41
action_106 (41) = happyShift action_42
action_106 (42) = happyShift action_43
action_106 (44) = happyShift action_75
action_106 (50) = happyShift action_45
action_106 (14) = happyGoto action_109
action_106 _ = happyFail

action_107 _ = happyReduce_29

action_108 (31) = happyShift action_33
action_108 (32) = happyShift action_34
action_108 (33) = happyShift action_35
action_108 (34) = happyShift action_36
action_108 (36) = happyShift action_37
action_108 (37) = happyShift action_38
action_108 (38) = happyShift action_39
action_108 (39) = happyShift action_40
action_108 (40) = happyShift action_41
action_108 (41) = happyShift action_42
action_108 (42) = happyShift action_43
action_108 (50) = happyShift action_45
action_108 _ = happyReduce_63

action_109 (28) = happyShift action_118
action_109 _ = happyReduce_32

action_110 _ = happyReduce_58

action_111 _ = happyReduce_38

action_112 (52) = happyShift action_117
action_112 _ = happyFail

action_113 (48) = happyShift action_116
action_113 _ = happyReduce_11

action_114 (47) = happyShift action_115
action_114 _ = happyFail

action_115 (44) = happyShift action_75
action_115 (51) = happyShift action_122
action_115 (14) = happyGoto action_121
action_115 _ = happyFail

action_116 (52) = happyShift action_92
action_116 (8) = happyGoto action_120
action_116 _ = happyReduce_10

action_117 _ = happyReduce_9

action_118 (44) = happyShift action_75
action_118 (14) = happyGoto action_119
action_118 _ = happyFail

action_119 _ = happyReduce_34

action_120 _ = happyReduce_12

action_121 _ = happyReduce_17

action_122 (52) = happyShift action_123
action_122 _ = happyFail

action_123 (44) = happyShift action_75
action_123 (14) = happyGoto action_124
action_123 _ = happyFail

action_124 _ = happyReduce_18

happyReduce_1 = happySpecReduce_2  4 happyReduction_1
happyReduction_1 (HappyAbsSyn13  happy_var_2)
	(HappyAbsSyn6  happy_var_1)
	 =  HappyAbsSyn4
		 (ProgramExp happy_var_1 happy_var_2
	)
happyReduction_1 _ _  = notHappyAtAll 

happyReduce_2 = happySpecReduce_1  4 happyReduction_2
happyReduction_2 (HappyAbsSyn6  happy_var_1)
	 =  HappyAbsSyn4
		 (ProgramExp happy_var_1 []
	)
happyReduction_2 _  = notHappyAtAll 

happyReduce_3 = happySpecReduce_1  4 happyReduction_3
happyReduction_3 (HappyAbsSyn13  happy_var_1)
	 =  HappyAbsSyn4
		 (ProgramExp [] happy_var_1
	)
happyReduction_3 _  = notHappyAtAll 

happyReduce_4 = happySpecReduce_0  4 happyReduction_4
happyReduction_4  =  HappyAbsSyn4
		 (ProgramExp [] []
	)

happyReduce_5 = happySpecReduce_2  5 happyReduction_5
happyReduction_5 (HappyAbsSyn9  happy_var_2)
	(HappyAbsSyn7  happy_var_1)
	 =  HappyAbsSyn5
		 (ClassDecExp happy_var_1 happy_var_2
	)
happyReduction_5 _ _  = notHappyAtAll 

happyReduce_6 = happySpecReduce_1  6 happyReduction_6
happyReduction_6 (HappyAbsSyn5  happy_var_1)
	 =  HappyAbsSyn6
		 ([happy_var_1]
	)
happyReduction_6 _  = notHappyAtAll 

happyReduce_7 = happySpecReduce_2  6 happyReduction_7
happyReduction_7 (HappyAbsSyn5  happy_var_2)
	(HappyAbsSyn6  happy_var_1)
	 =  HappyAbsSyn6
		 ((happy_var_2:happy_var_1)
	)
happyReduction_7 _ _  = notHappyAtAll 

happyReduce_8 = happyReduce 5 7 happyReduction_8
happyReduction_8 (_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn7
		 (ClassSigExp happy_var_2 happy_var_4
	) `HappyStk` happyRest

happyReduce_9 = happyReduce 7 7 happyReduction_9
happyReduction_9 ((HappyTerminal (Id happy_var_7)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn7
		 (ExtendedClassSigExp happy_var_2 happy_var_4 happy_var_7
	) `HappyStk` happyRest

happyReduce_10 = happySpecReduce_0  8 happyReduction_10
happyReduction_10  =  HappyAbsSyn8
		 ([]
	)

happyReduce_11 = happySpecReduce_3  8 happyReduction_11
happyReduction_11 (HappyTerminal (Id happy_var_3))
	_
	(HappyTerminal (Id happy_var_1))
	 =  HappyAbsSyn8
		 ([FormalArgExp happy_var_1 happy_var_3]
	)
happyReduction_11 _ _ _  = notHappyAtAll 

happyReduce_12 = happyReduce 5 8 happyReduction_12
happyReduction_12 ((HappyAbsSyn8  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest)
	 = HappyAbsSyn8
		 (((FormalArgExp happy_var_1 happy_var_3):happy_var_5)
	) `HappyStk` happyRest

happyReduce_13 = happySpecReduce_3  9 happyReduction_13
happyReduction_13 _
	(HappyAbsSyn13  happy_var_2)
	_
	 =  HappyAbsSyn9
		 (ClassBodyExp happy_var_2 []
	)
happyReduction_13 _ _ _  = notHappyAtAll 

happyReduce_14 = happySpecReduce_3  9 happyReduction_14
happyReduction_14 _
	(HappyAbsSyn11  happy_var_2)
	_
	 =  HappyAbsSyn9
		 (ClassBodyExp [] happy_var_2
	)
happyReduction_14 _ _ _  = notHappyAtAll 

happyReduce_15 = happyReduce 4 9 happyReduction_15
happyReduction_15 (_ `HappyStk`
	(HappyAbsSyn11  happy_var_3) `HappyStk`
	(HappyAbsSyn13  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn9
		 (ClassBodyExp happy_var_2 happy_var_3
	) `HappyStk` happyRest

happyReduce_16 = happySpecReduce_2  9 happyReduction_16
happyReduction_16 _
	_
	 =  HappyAbsSyn9
		 (ClassBodyExp [] []
	)

happyReduce_17 = happyReduce 6 10 happyReduction_17
happyReduction_17 ((HappyAbsSyn14  happy_var_6) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn10
		 (MethodExp happy_var_2 happy_var_4 happy_var_6
	) `HappyStk` happyRest

happyReduce_18 = happyReduce 8 10 happyReduction_18
happyReduction_18 ((HappyAbsSyn14  happy_var_8) `HappyStk`
	(HappyTerminal (Id happy_var_7)) `HappyStk`
	_ `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn8  happy_var_4) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_2)) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn10
		 (TypedMethodExp happy_var_2 happy_var_4 happy_var_7 happy_var_8
	) `HappyStk` happyRest

happyReduce_19 = happySpecReduce_1  11 happyReduction_19
happyReduction_19 (HappyAbsSyn10  happy_var_1)
	 =  HappyAbsSyn11
		 ([happy_var_1]
	)
happyReduction_19 _  = notHappyAtAll 

happyReduce_20 = happySpecReduce_2  11 happyReduction_20
happyReduction_20 (HappyAbsSyn10  happy_var_2)
	(HappyAbsSyn11  happy_var_1)
	 =  HappyAbsSyn11
		 ((happy_var_2:happy_var_1)
	)
happyReduction_20 _ _  = notHappyAtAll 

happyReduce_21 = happySpecReduce_1  12 happyReduction_21
happyReduction_21 (HappyAbsSyn16  happy_var_1)
	 =  HappyAbsSyn12
		 (IfExp happy_var_1
	)
happyReduction_21 _  = notHappyAtAll 

happyReduce_22 = happySpecReduce_1  12 happyReduction_22
happyReduction_22 (HappyAbsSyn15  happy_var_1)
	 =  HappyAbsSyn12
		 (WhileExp happy_var_1
	)
happyReduction_22 _  = notHappyAtAll 

happyReduce_23 = happySpecReduce_1  12 happyReduction_23
happyReduction_23 (HappyAbsSyn17  happy_var_1)
	 =  HappyAbsSyn12
		 (ReturnExp happy_var_1
	)
happyReduction_23 _  = notHappyAtAll 

happyReduce_24 = happySpecReduce_1  12 happyReduction_24
happyReduction_24 (HappyAbsSyn18  happy_var_1)
	 =  HappyAbsSyn12
		 (AssignmentExp happy_var_1
	)
happyReduction_24 _  = notHappyAtAll 

happyReduce_25 = happySpecReduce_1  12 happyReduction_25
happyReduction_25 (HappyAbsSyn20  happy_var_1)
	 =  HappyAbsSyn12
		 (BareExp happy_var_1
	)
happyReduction_25 _  = notHappyAtAll 

happyReduce_26 = happySpecReduce_1  13 happyReduction_26
happyReduction_26 (HappyAbsSyn12  happy_var_1)
	 =  HappyAbsSyn13
		 ([happy_var_1]
	)
happyReduction_26 _  = notHappyAtAll 

happyReduce_27 = happySpecReduce_2  13 happyReduction_27
happyReduction_27 (HappyAbsSyn12  happy_var_2)
	(HappyAbsSyn13  happy_var_1)
	 =  HappyAbsSyn13
		 ((happy_var_2:happy_var_1)
	)
happyReduction_27 _ _  = notHappyAtAll 

happyReduce_28 = happySpecReduce_2  14 happyReduction_28
happyReduction_28 _
	_
	 =  HappyAbsSyn14
		 (StatementBlockExp []
	)

happyReduce_29 = happySpecReduce_3  14 happyReduction_29
happyReduction_29 _
	(HappyAbsSyn13  happy_var_2)
	_
	 =  HappyAbsSyn14
		 (StatementBlockExp happy_var_2
	)
happyReduction_29 _ _ _  = notHappyAtAll 

happyReduce_30 = happySpecReduce_3  15 happyReduction_30
happyReduction_30 (HappyAbsSyn14  happy_var_3)
	(HappyAbsSyn21  happy_var_2)
	_
	 =  HappyAbsSyn15
		 (WhileLoop (happy_var_2,happy_var_3)
	)
happyReduction_30 _ _ _  = notHappyAtAll 

happyReduce_31 = happySpecReduce_3  16 happyReduction_31
happyReduction_31 (HappyAbsSyn14  happy_var_3)
	(HappyAbsSyn21  happy_var_2)
	_
	 =  HappyAbsSyn16
		 (OnlyIf (happy_var_2,happy_var_3)
	)
happyReduction_31 _ _ _  = notHappyAtAll 

happyReduce_32 = happyReduce 6 16 happyReduction_32
happyReduction_32 ((HappyAbsSyn14  happy_var_6) `HappyStk`
	(HappyAbsSyn21  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn14  happy_var_3) `HappyStk`
	(HappyAbsSyn21  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn16
		 (IfElif (happy_var_2,happy_var_3) (happy_var_5,happy_var_6)
	) `HappyStk` happyRest

happyReduce_33 = happyReduce 5 16 happyReduction_33
happyReduction_33 ((HappyAbsSyn14  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn14  happy_var_3) `HappyStk`
	(HappyAbsSyn21  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn16
		 (IfElse (happy_var_2,happy_var_3) happy_var_5
	) `HappyStk` happyRest

happyReduce_34 = happyReduce 8 16 happyReduction_34
happyReduction_34 ((HappyAbsSyn14  happy_var_8) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn14  happy_var_6) `HappyStk`
	(HappyAbsSyn21  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn14  happy_var_3) `HappyStk`
	(HappyAbsSyn21  happy_var_2) `HappyStk`
	_ `HappyStk`
	happyRest)
	 = HappyAbsSyn16
		 (IfElifElse (happy_var_2,happy_var_3) (happy_var_5,happy_var_6) happy_var_8
	) `HappyStk` happyRest

happyReduce_35 = happySpecReduce_2  17 happyReduction_35
happyReduction_35 _
	_
	 =  HappyAbsSyn17
		 (NoReturnExp
	)

happyReduce_36 = happySpecReduce_3  17 happyReduction_36
happyReduction_36 _
	(HappyAbsSyn21  happy_var_2)
	_
	 =  HappyAbsSyn17
		 (RetExp happy_var_2
	)
happyReduction_36 _ _ _  = notHappyAtAll 

happyReduce_37 = happyReduce 4 18 happyReduction_37
happyReduction_37 (_ `HappyStk`
	(HappyAbsSyn21  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn19  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn18
		 (UntypedAssignExp happy_var_1 happy_var_3
	) `HappyStk` happyRest

happyReduce_38 = happyReduce 6 18 happyReduction_38
happyReduction_38 (_ `HappyStk`
	(HappyAbsSyn21  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn19  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn18
		 (TypedAssignExp happy_var_1 happy_var_3 happy_var_5
	) `HappyStk` happyRest

happyReduce_39 = happySpecReduce_1  19 happyReduction_39
happyReduction_39 (HappyTerminal (Id happy_var_1))
	 =  HappyAbsSyn19
		 (IdRef happy_var_1
	)
happyReduction_39 _  = notHappyAtAll 

happyReduce_40 = happySpecReduce_3  19 happyReduction_40
happyReduction_40 (HappyTerminal (Id happy_var_3))
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn19
		 (ObjIdRef happy_var_1 happy_var_3
	)
happyReduction_40 _ _ _  = notHappyAtAll 

happyReduce_41 = happySpecReduce_2  20 happyReduction_41
happyReduction_41 _
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn20
		 (RightExp happy_var_1
	)
happyReduction_41 _ _  = notHappyAtAll 

happyReduce_42 = happySpecReduce_1  21 happyReduction_42
happyReduction_42 (HappyTerminal (QString happy_var_1))
	 =  HappyAbsSyn21
		 (StrExp happy_var_1
	)
happyReduction_42 _  = notHappyAtAll 

happyReduce_43 = happySpecReduce_1  21 happyReduction_43
happyReduction_43 (HappyTerminal (Qint happy_var_1))
	 =  HappyAbsSyn21
		 (IntExp (read happy_var_1)
	)
happyReduction_43 _  = notHappyAtAll 

happyReduce_44 = happySpecReduce_1  21 happyReduction_44
happyReduction_44 (HappyAbsSyn19  happy_var_1)
	 =  HappyAbsSyn21
		 (LeftExp happy_var_1
	)
happyReduction_44 _  = notHappyAtAll 

happyReduce_45 = happySpecReduce_3  21 happyReduction_45
happyReduction_45 _
	(HappyAbsSyn21  happy_var_2)
	_
	 =  HappyAbsSyn21
		 (ParExp happy_var_2
	)
happyReduction_45 _ _ _  = notHappyAtAll 

happyReduce_46 = happySpecReduce_3  21 happyReduction_46
happyReduction_46 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (BinaryExp "+" happy_var_1 happy_var_3
	)
happyReduction_46 _ _ _  = notHappyAtAll 

happyReduce_47 = happySpecReduce_3  21 happyReduction_47
happyReduction_47 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (BinaryExp "-" happy_var_1 happy_var_3
	)
happyReduction_47 _ _ _  = notHappyAtAll 

happyReduce_48 = happySpecReduce_3  21 happyReduction_48
happyReduction_48 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (BinaryExp "*" happy_var_1 happy_var_3
	)
happyReduction_48 _ _ _  = notHappyAtAll 

happyReduce_49 = happySpecReduce_3  21 happyReduction_49
happyReduction_49 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (BinaryExp "/" happy_var_1 happy_var_3
	)
happyReduction_49 _ _ _  = notHappyAtAll 

happyReduce_50 = happySpecReduce_3  21 happyReduction_50
happyReduction_50 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (BinaryExp "==" happy_var_1 happy_var_3
	)
happyReduction_50 _ _ _  = notHappyAtAll 

happyReduce_51 = happySpecReduce_3  21 happyReduction_51
happyReduction_51 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (BinaryExp "<=" happy_var_1 happy_var_3
	)
happyReduction_51 _ _ _  = notHappyAtAll 

happyReduce_52 = happySpecReduce_3  21 happyReduction_52
happyReduction_52 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (BinaryExp ">=" happy_var_1 happy_var_3
	)
happyReduction_52 _ _ _  = notHappyAtAll 

happyReduce_53 = happySpecReduce_3  21 happyReduction_53
happyReduction_53 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (BinaryExp "<" happy_var_1 happy_var_3
	)
happyReduction_53 _ _ _  = notHappyAtAll 

happyReduce_54 = happySpecReduce_3  21 happyReduction_54
happyReduction_54 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (BinaryExp ">" happy_var_1 happy_var_3
	)
happyReduction_54 _ _ _  = notHappyAtAll 

happyReduce_55 = happySpecReduce_3  21 happyReduction_55
happyReduction_55 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (BinaryExp "and" happy_var_1 happy_var_3
	)
happyReduction_55 _ _ _  = notHappyAtAll 

happyReduce_56 = happySpecReduce_3  21 happyReduction_56
happyReduction_56 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn21
		 (BinaryExp "or" happy_var_1 happy_var_3
	)
happyReduction_56 _ _ _  = notHappyAtAll 

happyReduce_57 = happySpecReduce_2  21 happyReduction_57
happyReduction_57 (HappyAbsSyn21  happy_var_2)
	_
	 =  HappyAbsSyn21
		 (NotExp happy_var_2
	)
happyReduction_57 _ _  = notHappyAtAll 

happyReduce_58 = happyReduce 6 21 happyReduction_58
happyReduction_58 (_ `HappyStk`
	(HappyAbsSyn22  happy_var_5) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn21  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn21
		 (ObjFuncCallExp happy_var_1 happy_var_3 happy_var_5
	) `HappyStk` happyRest

happyReduce_59 = happyReduce 5 21 happyReduction_59
happyReduction_59 (_ `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_3)) `HappyStk`
	_ `HappyStk`
	(HappyAbsSyn21  happy_var_1) `HappyStk`
	happyRest)
	 = HappyAbsSyn21
		 (ObjFuncCallExp happy_var_1 happy_var_3 []
	) `HappyStk` happyRest

happyReduce_60 = happyReduce 4 21 happyReduction_60
happyReduction_60 (_ `HappyStk`
	(HappyAbsSyn22  happy_var_3) `HappyStk`
	_ `HappyStk`
	(HappyTerminal (Id happy_var_1)) `HappyStk`
	happyRest)
	 = HappyAbsSyn21
		 (FuncCallExp happy_var_1 happy_var_3
	) `HappyStk` happyRest

happyReduce_61 = happySpecReduce_3  21 happyReduction_61
happyReduction_61 _
	_
	(HappyTerminal (Id happy_var_1))
	 =  HappyAbsSyn21
		 (FuncCallExp happy_var_1 []
	)
happyReduction_61 _ _ _  = notHappyAtAll 

happyReduce_62 = happySpecReduce_1  22 happyReduction_62
happyReduction_62 (HappyAbsSyn21  happy_var_1)
	 =  HappyAbsSyn22
		 ([happy_var_1]
	)
happyReduction_62 _  = notHappyAtAll 

happyReduce_63 = happySpecReduce_3  22 happyReduction_63
happyReduction_63 (HappyAbsSyn21  happy_var_3)
	_
	(HappyAbsSyn22  happy_var_1)
	 =  HappyAbsSyn22
		 ((happy_var_3:happy_var_1)
	)
happyReduction_63 _ _ _  = notHappyAtAll 

happyNewToken action sts stk
	= happyLexer(\tk -> 
	let cont i = action i i tk (HappyState action) sts stk in
	case tk of {
	EOF -> action 55 55 tk (HappyState action) sts stk;
	Class happy_dollar_dollar -> cont 23;
	Def happy_dollar_dollar -> cont 24;
	Extends happy_dollar_dollar -> cont 25;
	If happy_dollar_dollar -> cont 26;
	Elif happy_dollar_dollar -> cont 27;
	Else happy_dollar_dollar -> cont 28;
	While happy_dollar_dollar -> cont 29;
	Return happy_dollar_dollar -> cont 30;
	Plus happy_dollar_dollar -> cont 31;
	Minus happy_dollar_dollar -> cont 32;
	Times happy_dollar_dollar -> cont 33;
	Divide happy_dollar_dollar -> cont 34;
	Gets happy_dollar_dollar -> cont 35;
	Equals happy_dollar_dollar -> cont 36;
	Atmost happy_dollar_dollar -> cont 37;
	Atleast happy_dollar_dollar -> cont 38;
	Less happy_dollar_dollar -> cont 39;
	More happy_dollar_dollar -> cont 40;
	And happy_dollar_dollar -> cont 41;
	Or happy_dollar_dollar -> cont 42;
	Not happy_dollar_dollar -> cont 43;
	LeftCurly happy_dollar_dollar -> cont 44;
	RightCurly happy_dollar_dollar -> cont 45;
	LeftPar happy_dollar_dollar -> cont 46;
	RightPar happy_dollar_dollar -> cont 47;
	Comma happy_dollar_dollar -> cont 48;
	Semi happy_dollar_dollar -> cont 49;
	Period happy_dollar_dollar -> cont 50;
	Colon happy_dollar_dollar -> cont 51;
	Id happy_dollar_dollar -> cont 52;
	Qint happy_dollar_dollar -> cont 53;
	QString happy_dollar_dollar -> cont 54;
	_ -> happyError' tk
	})

happyError_ 55 tk = happyError' tk
happyError_ _ tk = happyError' tk

happyThen :: () => P a -> (a -> P b) -> P b
happyThen = (thenP)
happyReturn :: () => a -> P a
happyReturn = (returnP)
happyThen1 = happyThen
happyReturn1 :: () => a -> P a
happyReturn1 = happyReturn
happyError' :: () => (Token) -> P a
happyError' tk = parseErrorP tk

quackParse = happySomeParser where
  happySomeParser = happyThen (happyParse action_0) (\x -> case x of {HappyAbsSyn4 z -> happyReturn z; _other -> notHappyAtAll })

happySeq = happyDontSeq


{- AST Definition -}
type QStr  = String 
type QId = String
type QInt  = Int   
type ActualArgs = [RightExp]
type Statements = [Statement]
type CondPair = (RightExp,StatementBlock)
type Methods = [Method]
type FormalArgs = [FormalArg]
type Classes = [ClassExp]

data Program = ProgramExp Classes Statements
             | ForPrinting deriving Show
      
data ClassExp = ClassDecExp ClassSig ClassBody deriving Show

data ClassSig = ClassSigExp QId FormalArgs
              | ExtendedClassSigExp QId FormalArgs QId
              deriving Show

data ClassBody = ClassBodyExp Statements Methods deriving Show

data FormalArg = FormalArgExp QId QId deriving Show
    
data Method = MethodExp QId FormalArgs StatementBlock
            | TypedMethodExp QId FormalArgs QId StatementBlock 
            deriving Show

data Statement  
             = IfExp IfExp
             | WhileExp WhileExp
             | ReturnExp ReturnExp
             | AssignmentExp AssignmentExp
             | BareExp BareExp
             deriving Show

data StatementBlock = StatementBlockExp Statements deriving Show

data WhileExp = WhileLoop CondPair deriving Show

data IfExp = OnlyIf CondPair
           | IfElif CondPair CondPair 
           | IfElse CondPair StatementBlock
           | IfElifElse CondPair CondPair StatementBlock
           deriving Show

data ReturnExp = NoReturnExp | RetExp RightExp deriving Show

data AssignmentExp = UntypedAssignExp LeftExp RightExp
                   | TypedAssignExp LeftExp QStr RightExp
                   deriving Show

data BareExp = RightExp RightExp deriving Show

--Left Expressions
data LeftExp = IdRef QId
          | ObjIdRef RightExp QId
          deriving Show

--Right Expressions
data RightExp
        = StrExp QStr 
        | IntExp QInt
        | BinaryExp String RightExp RightExp
        | NotExp RightExp
        | ParExp RightExp
        | ObjFuncCallExp RightExp QId ActualArgs
        | FuncCallExp QId ActualArgs 
        | LeftExp LeftExp
        deriving Show



{-
type QClass = (String,String)

gc :: ClassSig -> QClass
gc cl = case cl of 
                  ClassSigExp cn -> (cn,"")
                  ExtendedClassSigExp cn ec -> (cn, ec)

getClasses :: Program -> [QClass]
getClasses p = let ProgramExp cls stmts = p in
                    map (\cd -> let s b = cd in gc s) cls  


data ClassTree = ClassNode String [ClassTree]-}


data Result a = Ok a | Failed String
type P a =  String -> Int -> Result a



thenP :: P a -> (a -> P b) -> P b
m `thenP` k = \s l -> case m s l of
                Ok a -> k a s l
                Failed e -> Failed e


returnP :: a -> P a
returnP a = \s l -> Ok a


failP :: String -> P a
failP err = \s l -> Failed (err)



catchP :: P a -> (String -> P a) -> P a
catchP m k = \s l -> case m s l of
                        Ok a -> Ok a
                        Failed e -> k e s l


happyLexer :: (Token -> P a) -> P a
happyLexer cont s  = case s of 
                        '\n':s -> \line -> happyLexer cont s (line + 1)
                        _ -> (\l -> case happyScanner s of
                                Left lexErr -> Failed (lexErr ++ " at line " ++ (show l))
                                Right (tok,s',l') -> case tok of 
                                                    Comment _ -> happyLexer cont s' l
                                                    BlockComment _ -> happyLexer cont s' l
                                                    _ -> cont tok s' l)
            

printHappyLexer ::  String -> IO ()
printHappyLexer  s  = do
                        putStrLn $ s                      
                        putStrLn $ show (happyScanner s) 

parseErrorP :: Token -> P a 
parseErrorP t = \s l -> case t of
                        _ -> Failed ("Parse error at line " ++ (show l))





main = do
        p <- getContents
        putStrLn $ (case   (quackParse p) 1 of
                  Ok x -> "Finished parse with no errors"
                  Failed w -> (w) )
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
{-# LINE 1 "<built-in>" #-}
{-# LINE 1 "<command-line>" #-}
{-# LINE 8 "<command-line>" #-}
# 1 "/usr/include/stdc-predef.h" 1 3 4

# 17 "/usr/include/stdc-predef.h" 3 4










































{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "/usr/lib/ghc/include/ghcversion.h" #-}

















{-# LINE 8 "<command-line>" #-}
{-# LINE 1 "templates/GenericTemplate.hs" #-}
-- Id: GenericTemplate.hs,v 1.26 2005/01/14 14:47:22 simonmar Exp 

{-# LINE 13 "templates/GenericTemplate.hs" #-}

{-# LINE 46 "templates/GenericTemplate.hs" #-}








{-# LINE 67 "templates/GenericTemplate.hs" #-}

{-# LINE 77 "templates/GenericTemplate.hs" #-}

{-# LINE 86 "templates/GenericTemplate.hs" #-}

infixr 9 `HappyStk`
data HappyStk a = HappyStk a (HappyStk a)

-----------------------------------------------------------------------------
-- starting the parse

happyParse start_state = happyNewToken start_state notHappyAtAll notHappyAtAll

-----------------------------------------------------------------------------
-- Accepting the parse

-- If the current token is (1), it means we've just accepted a partial
-- parse (a %partial parser).  We must ignore the saved token on the top of
-- the stack in this case.
happyAccept (1) tk st sts (_ `HappyStk` ans `HappyStk` _) =
        happyReturn1 ans
happyAccept j tk st sts (HappyStk ans _) = 
         (happyReturn1 ans)

-----------------------------------------------------------------------------
-- Arrays only: do the next action

{-# LINE 155 "templates/GenericTemplate.hs" #-}

-----------------------------------------------------------------------------
-- HappyState data type (not arrays)



newtype HappyState b c = HappyState
        (Int ->                    -- token number
         Int ->                    -- token number (yes, again)
         b ->                           -- token semantic value
         HappyState b c ->              -- current state
         [HappyState b c] ->            -- state stack
         c)



-----------------------------------------------------------------------------
-- Shifting a token

happyShift new_state (1) tk st sts stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--     trace "shifting the error token" $
     new_state i i tk (HappyState (new_state)) ((st):(sts)) (stk)

happyShift new_state i tk st sts stk =
     happyNewToken new_state ((st):(sts)) ((HappyTerminal (tk))`HappyStk`stk)

-- happyReduce is specialised for the common cases.

happySpecReduce_0 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_0 nt fn j tk st@((HappyState (action))) sts stk
     = action nt j tk st ((st):(sts)) (fn `HappyStk` stk)

happySpecReduce_1 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_1 nt fn j tk _ sts@(((st@(HappyState (action))):(_))) (v1`HappyStk`stk')
     = let r = fn v1 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_2 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_2 nt fn j tk _ ((_):(sts@(((st@(HappyState (action))):(_))))) (v1`HappyStk`v2`HappyStk`stk')
     = let r = fn v1 v2 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happySpecReduce_3 i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happySpecReduce_3 nt fn j tk _ ((_):(((_):(sts@(((st@(HappyState (action))):(_))))))) (v1`HappyStk`v2`HappyStk`v3`HappyStk`stk')
     = let r = fn v1 v2 v3 in
       happySeq r (action nt j tk st sts (r `HappyStk` stk'))

happyReduce k i fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyReduce k nt fn j tk st sts stk
     = case happyDrop (k - ((1) :: Int)) sts of
         sts1@(((st1@(HappyState (action))):(_))) ->
                let r = fn stk in  -- it doesn't hurt to always seq here...
                happyDoSeq r (action nt j tk st1 sts1 r)

happyMonadReduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonadReduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
          let drop_stk = happyDropStk k stk in
          happyThen1 (fn stk tk) (\r -> action nt j tk st1 sts1 (r `HappyStk` drop_stk))

happyMonad2Reduce k nt fn (1) tk st sts stk
     = happyFail (1) tk st sts stk
happyMonad2Reduce k nt fn j tk st sts stk =
      case happyDrop k ((st):(sts)) of
        sts1@(((st1@(HappyState (action))):(_))) ->
         let drop_stk = happyDropStk k stk





             new_state = action

          in
          happyThen1 (fn stk tk) (\r -> happyNewToken new_state sts1 (r `HappyStk` drop_stk))

happyDrop (0) l = l
happyDrop n ((_):(t)) = happyDrop (n - ((1) :: Int)) t

happyDropStk (0) l = l
happyDropStk n (x `HappyStk` xs) = happyDropStk (n - ((1)::Int)) xs

-----------------------------------------------------------------------------
-- Moving to a new state after a reduction

{-# LINE 256 "templates/GenericTemplate.hs" #-}
happyGoto action j tk st = action j j tk (HappyState action)


-----------------------------------------------------------------------------
-- Error recovery ((1) is the error token)

-- parse error if we are in recovery and we fail again
happyFail (1) tk old_st _ stk@(x `HappyStk` _) =
     let i = (case x of { HappyErrorToken (i) -> i }) in
--      trace "failing" $ 
        happyError_ i tk

{-  We don't need state discarding for our restricted implementation of
    "error".  In fact, it can cause some bogus parses, so I've disabled it
    for now --SDM

-- discard a state
happyFail  (1) tk old_st (((HappyState (action))):(sts)) 
                                                (saved_tok `HappyStk` _ `HappyStk` stk) =
--      trace ("discarding state, depth " ++ show (length stk))  $
        action (1) (1) tk (HappyState (action)) sts ((saved_tok`HappyStk`stk))
-}

-- Enter error recovery: generate an error token,
--                       save the old token and carry on.
happyFail  i tk (HappyState (action)) sts stk =
--      trace "entering error recovery" $
        action (1) (1) tk (HappyState (action)) sts ( (HappyErrorToken (i)) `HappyStk` stk)

-- Internal happy errors:

notHappyAtAll :: a
notHappyAtAll = error "Internal Happy error\n"

-----------------------------------------------------------------------------
-- Hack to get the typechecker to accept our action functions







-----------------------------------------------------------------------------
-- Seq-ing.  If the --strict flag is given, then Happy emits 
--      happySeq = happyDoSeq
-- otherwise it emits
--      happySeq = happyDontSeq

happyDoSeq, happyDontSeq :: a -> b -> b
happyDoSeq   a b = a `seq` b
happyDontSeq a b = b

-----------------------------------------------------------------------------
-- Don't inline any functions from the template.  GHC has a nasty habit
-- of deciding to inline happyGoto everywhere, which increases the size of
-- the generated parser quite a bit.

{-# LINE 322 "templates/GenericTemplate.hs" #-}
{-# NOINLINE happyShift #-}
{-# NOINLINE happySpecReduce_0 #-}
{-# NOINLINE happySpecReduce_1 #-}
{-# NOINLINE happySpecReduce_2 #-}
{-# NOINLINE happySpecReduce_3 #-}
{-# NOINLINE happyReduce #-}
{-# NOINLINE happyMonadReduce #-}
{-# NOINLINE happyGoto #-}
{-# NOINLINE happyFail #-}

-- end of Happy Template.
