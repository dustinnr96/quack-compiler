{
module Main where
import QuackLexer
}

%name quackParse
%tokentype { Token }
%error { parseErrorP }
%monad { P } { thenP } { returnP }
%lexer { happyLexer } { EOF }
%token
        --list tokens
    class           { Class $$ }
    def             { Def $$ }    
    extends         { Extends $$ }
    if              { If $$ }
    elif            { Elif $$ }
    else            { Else $$ }
    while           { While $$ }
    return          { Return $$ }
    --  PUNCTUATION 
    '+'             { Plus $$ }
    '-'             { Minus $$ }
    '*'             { Times $$ }
    '/'             { Divide $$ }
    '='             { Gets $$ }
    eq              { Equals $$ }
    le              { Atmost $$ }
    ge              { Atleast $$ }
    '<'             { Less $$ }
    '>'             { More $$ }
    and             { And $$ }
    or              { Or $$ }
    not             { Not $$ }
    '{'             { LeftCurly $$ }
    '}'             { RightCurly $$ }
    '('             { LeftPar $$ }
    ')'             { RightPar $$ }
    ','             { Comma $$ }
    ';'             { Semi $$}
    '.'             { Period $$ }
    ':'             { Colon $$ }
    --  IDENTIFIERS 
    id              { Id $$ }
    -- INT  LITS 
    int             { Qint $$ }
        
    -- STRING  LITS 
  
    str             { QString $$ }


%right not
%left and or
%nonassoc eq '>' '<' le ge
%left '+' '-'
%left '*' '/'
%left '.'
%%

Program : ClassExps Statements                                               { ProgramExp $1 $2 }  
        | ClassExps                                                           { ProgramExp $1 [] } 
        | Statements                                                          { ProgramExp [] $1 } 
        | {- empty -}                                                        { ProgramExp [] [] } 


ClassExp : ClassSig ClassBody                                                { ClassDecExp $1 $2 }  

ClassExps : ClassExp                                                         { [$1] }  
          | ClassExps ClassExp                                               { ($2:$1) }  

ClassSig : class id '(' FormalArgs ')'                                       { ClassSigExp $2 $4 }  
         | class id '(' FormalArgs ')' extends id                            { ExtendedClassSigExp $2 $4 $7 }  

FormalArgs : {- empty -}                                                     { [] }  
           | id ':' id                                                       { [FormalArgExp $1 $3] }  
           | id ':' id ','  FormalArgs                                       { ((FormalArgExp $1 $3):$5) }  
     
ClassBody : '{' Statements '}'                                               { ClassBodyExp $2 [] }  
          | '{' Methods '}'                                                  { ClassBodyExp [] $2 }  
          | '{' Statements Methods '}'                                       { ClassBodyExp $2 $3 } 
          | '{' '}'                                                          { ClassBodyExp [] [] }  

Method : def id '(' FormalArgs ')' StatementBlock                            { MethodExp $2 $4 $6 }  
       | def id '(' FormalArgs ')' ':' id StatementBlock                     { TypedMethodExp $2 $4 $7 $8 }  
 
Methods : Method                                                             { [$1] }  
        | Methods Method                                                     { ($2:$1) }  
 
Statement : IfExp                                                            { IfExp $1 }  
          | WhileExp                                                         { WhileExp $1 }  
          | ReturnExp                                                        { ReturnExp $1 }  
          | Assignment                                                       { AssignmentExp $1 }  
          | BareExp                                                          { BareExp $1 }
       
Statements : Statement                                                       { [$1] }  
           | Statements Statement                                            { ($2:$1) }  

StatementBlock : '{' '}'                                                     { StatementBlockExp [] }   
               | '{' Statements '}'                                          { StatementBlockExp $2 }  

WhileExp : while RExp StatementBlock                                         { WhileLoop ($2,$3) }  

IfExp : if RExp StatementBlock                                               { OnlyIf ($2,$3) }  
       | if RExp StatementBlock elif RExp StatementBlock                     { IfElif ($2,$3) ($5,$6) }  
       | if RExp StatementBlock else StatementBlock                          { IfElse ($2,$3) $5 }  
       | if RExp StatementBlock elif RExp StatementBlock else StatementBlock { IfElifElse ($2,$3) ($5,$6) $8 }  
   
ReturnExp : return ';'                                                       { NoReturnExp }  
          | return RExp ';'                                                  { RetExp $2 }  

Assignment : LExp '=' RExp ';'                                               { UntypedAssignExp $1 $3 }  
           | LExp ':' id '=' RExp ';'                                        { TypedAssignExp $1 $3 $5 }  

LExp : id                                                                    { IdRef $1 }  
     | RExp '.' id                                                           { ObjIdRef $1 $3 }  

BareExp : RExp ';'                                                           { RightExp $1 }   


RExp : str                                                                   { StrExp $1 }  
     | int                                                                   { IntExp (read $1) }  
     | LExp                                                                  { LeftExp $1 }  
     | '(' RExp ')'                                                          { ParExp $2 }  
     | RExp '+' RExp                                                         { BinaryExp "+" $1 $3 }  
     | RExp '-' RExp                                                         { BinaryExp "-" $1 $3 }  
     | RExp '*' RExp                                                         { BinaryExp "*" $1 $3 }  
     | RExp '/' RExp                                                         { BinaryExp "/" $1 $3 }  
     | RExp eq RExp                                                          { BinaryExp "==" $1 $3 }  
     | RExp le RExp                                                          { BinaryExp "<=" $1 $3 }  
     | RExp ge RExp                                                          { BinaryExp ">=" $1 $3 }  
     | RExp '<' RExp                                                         { BinaryExp "<" $1 $3 }  
     | RExp '>' RExp                                                         { BinaryExp ">" $1 $3 }   
     | RExp and RExp                                                         { BinaryExp "and" $1 $3 }  
     | RExp or RExp                                                          { BinaryExp "or" $1 $3 }      
     | not RExp                                                              { NotExp $2 }  
     | RExp '.' id '(' ActualArgs ')'                                        { ObjFuncCallExp $1 $3 $5 }  
     | RExp '.' id '('  ')'                                                  { ObjFuncCallExp $1 $3 [] } 
     | id '(' ActualArgs ')'                                                 { FuncCallExp $1 $3 }  
     | id '(' ')'                                                            { FuncCallExp $1 [] } 

ActualArgs : RExp                                                            { [$1] }  
           | ActualArgs ',' RExp                                             { ($3:$1) }  

{


{- AST Definition -}
type QStr  = String 
type QId = String
type QInt  = Int   
type ActualArgs = [RightExp]
type Statements = [Statement]
type CondPair = (RightExp,StatementBlock)
type Methods = [Method]
type FormalArgs = [FormalArg]
type Classes = [ClassExp]

data Program = ProgramExp Classes Statements
             | ForPrinting deriving Show
      
data ClassExp = ClassDecExp ClassSig ClassBody deriving Show

data ClassSig = ClassSigExp QId FormalArgs
              | ExtendedClassSigExp QId FormalArgs QId
              deriving Show

data ClassBody = ClassBodyExp Statements Methods deriving Show

data FormalArg = FormalArgExp QId QId deriving Show
    
data Method = MethodExp QId FormalArgs StatementBlock
            | TypedMethodExp QId FormalArgs QId StatementBlock 
            deriving Show

data Statement  
             = IfExp IfExp
             | WhileExp WhileExp
             | ReturnExp ReturnExp
             | AssignmentExp AssignmentExp
             | BareExp BareExp
             deriving Show

data StatementBlock = StatementBlockExp Statements deriving Show

data WhileExp = WhileLoop CondPair deriving Show

data IfExp = OnlyIf CondPair
           | IfElif CondPair CondPair 
           | IfElse CondPair StatementBlock
           | IfElifElse CondPair CondPair StatementBlock
           deriving Show

data ReturnExp = NoReturnExp | RetExp RightExp deriving Show

data AssignmentExp = UntypedAssignExp LeftExp RightExp
                   | TypedAssignExp LeftExp QStr RightExp
                   deriving Show

data BareExp = RightExp RightExp deriving Show

--Left Expressions
data LeftExp = IdRef QId
          | ObjIdRef RightExp QId
          deriving Show

--Right Expressions
data RightExp
        = StrExp QStr 
        | IntExp QInt
        | BinaryExp String RightExp RightExp
        | NotExp RightExp
        | ParExp RightExp
        | ObjFuncCallExp RightExp QId ActualArgs
        | FuncCallExp QId ActualArgs 
        | LeftExp LeftExp
        deriving Show



{-
type QClass = (String,String)

gc :: ClassSig -> QClass
gc cl = case cl of 
                  ClassSigExp cn -> (cn,"")
                  ExtendedClassSigExp cn ec -> (cn, ec)

getClasses :: Program -> [QClass]
getClasses p = let ProgramExp cls stmts = p in
                    map (\cd -> let s b = cd in gc s) cls  


data ClassTree = ClassNode String [ClassTree]-}


data Result a = Ok a | Failed String
type P a =  String -> Int -> Result a



thenP :: P a -> (a -> P b) -> P b
m `thenP` k = \s l -> case m s l of
                Ok a -> k a s l
                Failed e -> Failed e


returnP :: a -> P a
returnP a = \s l -> Ok a


failP :: String -> P a
failP err = \s l -> Failed (err)



catchP :: P a -> (String -> P a) -> P a
catchP m k = \s l -> case m s l of
                        Ok a -> Ok a
                        Failed e -> k e s l


happyLexer :: (Token -> P a) -> P a
happyLexer cont s  = case s of 
                        '\n':s -> \line -> happyLexer cont s (line + 1)
                        _ -> (\l -> case happyScanner s of
                                Left lexErr -> Failed (lexErr ++ " at line " ++ (show l))
                                Right (tok,s',l') -> case tok of 
                                                    Comment _ -> happyLexer cont s' l
                                                    BlockComment _ -> happyLexer cont s' l
                                                    _ -> cont tok s' l)
            

printHappyLexer ::  String -> IO ()
printHappyLexer  s  = do
                        putStrLn $ s                      
                        putStrLn $ show (happyScanner s) 

parseErrorP :: Token -> P a 
parseErrorP t = \s l -> case t of
                        _ -> Failed ("Parse error at line " ++ (show l))





main = do
        p <- getContents
        putStrLn $ (case   (quackParse p) 1 of
                  Ok x -> "Finished parse with no errors"
                  Failed w -> (w) )
}

